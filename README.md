# DC Signs #

### Order of Operations ###
1. Run CheckSupportsSigns.py.  This runs a basic set of checks against the signs in the database.  
2. Run LoadSigns.py.  This loads signs and supports to the local computer and associates the supports to the street centerline.

### Background ###

Throughout its history, the District Department of Transportation (DDOT) has struggled to keep the city's inventory of signs up to date and an accurate representation of our regulatory code.  It's not easy, but we are exploring ways to get smarter about how to identify problems with signs by using GIS to flag and report problems.  

### What is this repository for? ###

This repository provides a GIS linear referenced approach for assessing parking signage within a city context.  The current approach is for the specific needs of the District of Columbia, but our hope is that some of the concepts and script automation might be easily configurable to other localities.  Below is the high-level process for how DC’s sign QC setup and executed.  We will continually update this repository and improve as time permits.  For more information, please contact James Graham or the DDOT GIS team at ddotgis@dc.gov

### Structure of Sign Data ###
DDOT uses a Support -> Signs -> Time Restrictions relationship.  Given one support, one or more signs may be related to it by ESRI based relationship classes.  Given one sign, one or more ‘Time Restrictions’ may be related, again using ESRI relationship classes.  

# High-level QC Workflow #

### Step 1:  Load Signs ###
Now that we have LRS route information for each of the supports, we need to put it in a useful form.  [LoadSigns.py](https://bitbucket.org/ddotgis/dcsigns/src/f60f15a599c02db605d3c51487962bb131c71d5e/linear_checks/LoadSigns.py?at=master) is the main script that does this and it makes it easier to carry out the next step. Essentially it snaps signs to a support based on the common support ID. Then it creates a SignTableDetail that stores all the information we need to create zones of restriction or permission for parking. Each row represents a specific ‘Time Band’ for a given sign.  There may be more than one record for each sign, depending on the number of time bands (information within ‘TOA.TimeRestrictions’ table).  After support, sign and restriction information is collected and stored within the geodatabase, it must be linearly referenced to its associated street centerline.  We do this with a custom ESRI Server Object Extension that was built by DDOT called PointOnSegment.  

By using [GetOnSegPointInfo( )](https://bitbucket.org/ddotgis/dcsigns/src/f60f15a599c02db605d3c51487962bb131c71d5e/linear_checks/SegmentUtilities.py?at=master), we are returned the following information for each support location:  SegmentID of our linear referenced route, Measure along that segment, Side of street, Angle, Offset, and other information.  If more than one route is found within the given tolerance, we keep the closest and store the IDs of the remaining (To be defined in details later).  A modified implementation of the Locate Features Along Routes within the ESRI standard Linear Referencing toolbox can accomplish similar results.  


SignTableDetail  | 
---------------- |
SupportID|
SignID| 
RestrictionID|
SegmentID|
Side |
Measure|
SignType|
SignText|
SignArrowDirection|
StartDay|
EndDay|
StartTime|
EndTime|
Exception|
ZoneID (text) (are we still saving this as text? it is not in the script) |
X/Lon (please check?)|
Y/Lat|



### Step 2: Make Parking Zones ###
After loading the signs into SignTableDetail we begin the process of creating zones.  We will use simple checks to see what matches up as a zone ([MergeZone.py](https://bitbucket.org/ddotgis/dcsigns/src/31d159ebdbad2c6f5f3a88f1e754fbfad87d8360/linear_checks/MergeZone.py?at=master)):

1. Sort by Segment IDs, Side of Street, SignType, TimeID (we created TimeID based on Time Restrictions; signs that have the same time restrictions will have the same TimeIDs), and Measure
2. The essential logic is that we link up signs when they are on the same side of the same street segments, they are of the same type and the same time restrictions, and their arrow directions compose a zone, meaning that there should be a left and a right sign at the end of a zone). Taking time restrictions into considering while forming zones avoids the internal consistency check that we originally do after this step.
3. There are exceptions. For example, there are many cases where a sign points to the end of a street without any other sign in between. That means a zone is formed between the sign and the end of the street.
4. One of the errors we flag throughout the process is when a zone overlaps with a zone of the same type. For example, on the left of a street segment, we have a zone from Measure 5 to Measure 15 of NoStandingNoParking. The next sign of the same type and time restrictions at Measure 20 has an arrow pointing toward Measure 0. That means two zones overlap and we flag that as a zone error. (Currently the error is not stored anywhere yet. The next step is to store that into our zone error table.)


### Check Zone-to-Zone Consistency ###
There are three primary tiers of checks to ensure that there are no Zone-to-Zone conflicts between restrictive and permissive types of parking zones:  Linear, Day, Time.  If a parking permitted zone overlaps/intersects a restrictive zone in all three, then there is a Zone-to-Zone conflict. These Zone-to-Zone conflicts are also stored in our error validation table.

Essential steps:

1. Check whether two zones have linear overlaps. For example, one zone is from Measure 2 to Measure 10 and the other is from Measure 5 to Measure 15.
2. If so, check whether two zones have an overlap in day. For example, one zone is Monday-Saturday and the other is Saturday only.
3. If so, check whether two zones have an overlap in time. For example, one zone is 7:30 am - 9:00 am and the other is ANYTIME.
4. If answers to the previous three questions are YES, continue to check the types of two zones. If they are of different types, meaning one is restrictive and the other is permissive, that is a type of error we need to flag, called Conflicting Zone Error. If they are of the same type, we flag that as well and called it as Overlapping Zone Error (or the name in the error table).

# Validation Error Results #
Currently we have two validation error tables and they contain details about what type of problem exists, what sign(s) are related to the validation error, and geometry (single XY for attribute validation errors; start/end XYs for zone validation errors. Information on where they occur (Ward, ANC, Block) has not been fully incorporated into the error tables yet. 

One is [Point Error Table](https://bitbucket.org/ddotgis/dcsigns/src/d222cede4cc93e2183f5de9c1a1586acb26e3704/linear_checks/PointErrorCheck.py?at=master) and it focuses on errors that occur with each sign or support. 
For example, currently we are checking: 

1. whether there are repetitive signs (of the same type) on the same support; 
2. whether there are multiple street segments assigned to a single support; 
3. whether there are picture attachments for each support.
Other point error checks can be added depending on the needs.

The other is [Zone Error Table](https://bitbucket.org/ddotgis/dcsigns/src/d222cede4cc93e2183f5de9c1a1586acb26e3704/linear_checks/ZoneErrorCheck.py?at=master). Currently we are checking:

1. whether there are conflicting zones
2. whether there are overlapping zones (of the same type and same time/day restrictions)
3. Whether there are missing time/day restrictions or error with those
Other zone-level error checks can be added as needed.


### Brief explanations on other scripts ###

[AddAttachments.py](https://bitbucket.org/ddotgis/dcsigns/src/133050ba22df9e788e504f9efd2f660f6e90a60d/linear_checks/AddAttachment.py?at=master):

It adds a field that stores the information on the number of attachments that are associated with one support.

[SegmentUtilities.py](https://bitbucket.org/ddotgis/dcsigns/src/133050ba22df9e788e504f9efd2f660f6e90a60d/linear_checks/SegmentUtilities.py?at=master):

It contains some simple segment-related functions that are used in other scripts. For example, store Measures of a street segment into a dictionary which can be easily accessed in MergeZone.py.

[SnappingTools.py](https://bitbucket.org/ddotgis/dcsigns/src/133050ba22df9e788e504f9efd2f660f6e90a60d/linear_checks/SnappingTools.py?at=master):

SnapSigns() essentially joins signs to their corresponding support.

RouteSupportsToDict() stores information returned from [GetOnSegPointInfo()](https://bitbucket.org/ddotgis/dcsigns/src/f60f15a599c02db605d3c51487962bb131c71d5e/linear_checks/SegmentUtilities.py?at=master). One thing to note is that we check whether there is an existing manual segment ID in place for a specific support. The reason we are doing so is because in some cases a support will be assigned to an incorrect street segment even though the street segment is the closest one. Since we want to detect issues like this, we store IDs of other street segments that are also close to the support though they are not necessarily the closest one. In this case, we search street segments that are within 100 feet search radius of the support and also less than 25 feet away from the closest street segment.

[TableTools.py](https://bitbucket.org/ddotgis/dcsigns/src/133050ba22df9e788e504f9efd2f660f6e90a60d/linear_checks/TableTools.py?at=master):

Simple tools that convert table to nested dictionary or to join table with other table files.

[Offset.py](https://bitbucket.org/ddotgis/dcsigns/src/133050ba22df9e788e504f9efd2f660f6e90a60d/linear_checks/Offset.py?at=master):

Add offset values to parking zone linear routes so that parking zones of different types are separated and visually clear.

[ZoneSignRelation.py](https://bitbucket.org/ddotgis/dcsigns/src/133050ba22df9e788e504f9efd2f660f6e90a60d/linear_checks/ZoneSignRelation.py?at=master):

Create relationship classes between ParkingZoneTable, SignTableDetail, and sign table from production database. Essentially it allows users to go to composing signs of a zone and find out detail information about the zone as well as locate signs in the map. 

...updating...