import requests
import ast
import sys, os

def GetOnSegPointInfo(inX, inY, segid='', roadwaytype='Street', searchradius=150):
    '''
    Retrieve LRS event data through GetOnSegPoint rest service given an input feature class.
    optionally, a user can provide a search radius (in feet) up to 150 feet.
    optionally, a user can provide a street segment id, if known.
    optionally, a user can provide a street type, to further constrain matches
    '''

    payload = {"X": inX,
               "Y": inY,
               "RoadType": roadwaytype,
               "SearchRadius": searchradius,
               "f":'json'
               }
        
    r = requests.get("http://maps2.dcgis.dc.gov/dcgis/rest/services/DDOT/SSD/MapServer/exts/PointOnSegment/GetPointOnSegment", params=payload)
    thispoint = r.json()
    if 'error' not in thispoint and len(thispoint['PointOnSegments']) > 0:
        #{'PointOnSegments':[{SegID:...,Roadtype:...,},{},{}]}
        closest = min(thispoint['PointOnSegments'], key = lambda x: x['Offset'])
        segid = int(closest['StreetSegID'])
        side = float(closest['Side'])        
        angle = float(closest['Tangent'])
        offset = float(closest['Offset'])
        measure = float(closest['Measure'])
        list = []
        for segment in thispoint['PointOnSegments']:
            if segment['StreetSegID'] != closest['StreetSegID']:
                list.append(int(segment['StreetSegID']))
        other_segid = str(",".join(str(x) for x in sorted(list)))
        return segid, measure, side, offset, angle, other_segid

    else:
        return False


            
