import arcpy
import sys, os
sys.path.append(r'C:\Users\yge\Documents\dcsigns\linear_checks')


arcpy.env.overwriteOutput = True

#create script tool for migration
#the main function: turn a flat table into separate tables with relationship classes in between

#set parameters
gdb_path = arcpy.GetParameterAsText(0)
originTable = arcpy.GetParameterAsText(1)
SignTableName = arcpy.GetParameterAsText(2)
SupportTableName = arcpy.GetParameterAsText(3)
SupportAttachName = arcpy.GetParameterAsText(4)
TimeRestrictionTableName = arcpy.GetParameterAsText(5)
SupportToSignName = arcpy.GetParameterAsText(6)
SignToTimerestrictionsName = arcpy.GetParameterAsText(7)

#output tables 
Sign = os.path.join(gdb_path, SignTableName)
Support = os.path.join(gdb_path, SupportTableName)
Support_ATTACH = os.path.join(gdb_path, SupportAttachName)
TimeRestriction = os.path.join(gdb_path, TimeRestrictionTableName)
#relationship classes tables
Support_to_Sign = os.path.join(gdb_path, SupportToSignName)
Sign_to_TimeRestrictions = os.path.join(gdb_path, SignToTimerestrictionsName)



if not arcpy.Exists(Sign):
    arcpy.CreateFeatureclass_management(gdb, os.path.basename(Sign),'POINT','','','',arcpy.Describe(originTable).spatialReference)
    arcpy.AddField_management(Sign, "SupportID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(Sign, "SignID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(Sign, "MUTCD", "TEXT","", "", 25, "MUTCD Code", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(Sign, "SIGNTEXT", "TEXT","", "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(Sign, "SignArrowDirection", "TEXT","", "", 2, "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(Sign, "Status","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Sign, "CREATED_USER","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Sign, "CREATED_DATE","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Sign, "LAST_EDITED_USER","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Sign, "LAST_EDITED_DATE","TEXT","","","","","NULLABLE","NON_REQUIRED")
else:
    arcpy.TruncateTable_management(Sign)


if not arcpy.Exists(Support):
    arcpy.CreateFeatureclass_management(gdb,os.path.basename(Support),'POINT','','','',arcpy.Describe(originTable).spatialReference)
    arcpy.AddField_management(Support, "SupportID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(Support, "CREATED_USER","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Support, "CREATED_DATE","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Support, "LAST_EDITED_USER","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Support, "LAST_EDITED_DATE","TEXT","","","","","NULLABLE","NON_REQUIRED")
else:
    arcpy.TruncateTable_management(Support)



if not arcpy.Exists(Support_ATTACH):
    arcpy.CreateTable_management(gdb,os.path.basename(Support_ATTACH))
    arcpy.AddField_management(Support_ATTACH, "ATTACHMENTID","TEXT","","","","","NULLABLE","REQUIRED")
    arcpy.AddField_management(Support_ATTACH, "REL_OBJECTID","TEXT","","","","","NULLABLE","REQUIRED")
    arcpy.AddField_management(Support_ATTACH, "CONTENT_TYPE","TEXT","","",150,"","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Support_ATTACH, "ATT_NAME","TEXT","","",250,"","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Support_ATTACH, "DATA_SIZE","LONG",10,"","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(Support_ATTACH, "DATA","TEXT","","","","","NULLABLE","NON_REQUIRED")    
else:
    arcpy.TruncateTable_management(Support_ATTACH)


if not arcpy.Exists(TimeRestriction):
    arcpy.CreateTable_management(gdb,os.path.basename(TimeRestriction))
    arcpy.AddField_management(TimeRestriction, "SIGNID","TEXT",38,"","","","NULLABLE","REQUIRED")
   #is LINKID in TimeRestriction table unique global id for each row? if not, shall we create a timerestrictionID
    arcpy.AddField_management(TimeRestriction, "LINKID","TEXT","","",50,"","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(TimeRestriction, "HOURLIMIT","DOUBLE",38,8,"","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(TimeRestriction, "RESTRICTIONORDER","LONG",10,"","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(TimeRestriction, "STARTDAY","SHORT",5,"","","","NULLABLE","NON_REQUIRED", 'LZ_ServiceDay')
    arcpy.AddField_management(TimeRestriction, "ENDDAY","SHORT",5,"","","","NULLABLE","NON_REQUIRED", 'LZ_ServiceDay')
    arcpy.AddField_management(TimeRestriction, "STARTTIME","SHORT",5,"","","","NULLABLE","NON_REQUIRED", 'LZ_ServiceHours')
    arcpy.AddField_management(TimeRestriction, "ENDTIME","SHORT",5,"","","","NULLABLE","NON_REQUIRED",'LZ_ServiceHours')
    arcpy.AddField_management(TimeRestriction, "EXCEPTION","SHORT",5,"","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(TimeRestriction, "CREATED_USER","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(TimeRestriction, "CREATED_DATE","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(TimeRestriction, "LAST_EDITED_USER","TEXT","","","","","NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(TimeRestriction, "LAST_EDITED_DATE","TEXT","","","","","NULLABLE","NON_REQUIRED")
else:
    arcpy.TruncateTable_management(TimeRestriction)


#In the field list, add fields that you want to be populated, currently it might not be comprehensive
rows = arcpy.da.InsertCursor(Sign, ['SIGNTEXT','MUTCD','SignID','STATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','SignArrowDirection','SupportID'])
arows = arcpy.da.InsertCursor(Support, ['SupportID','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE'])
brows = arcpy.da.InsertCursor(TimeRestriction, ['SIGNID','STARTDAY','ENDDAY','STARTTIME','ENDTIME','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE'])


with arcpy.da.SearchCursor(originTable, ['SIGN_TEXT','PARKING_TYPE','SIGNID','SIGNSTATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','PARKING_ARROW','POLEID','SHAPE@XY']) as cursor:
        for a in cursor:
            if a[2] == 'Active' and 'PoleID':
                rows.insertRow(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10])
                arows.insertRow(a[9], a[4], a[5], a[6], a[7])
                #next we are going to insert time restrictions information. In the origin table, one row is for one sign and can store more than one time restrictions.
                #however, after migration, we want each row to represent one time restriction, therefore we need a restrictionID?
                brows.insertRow(a[2],a[],a[],a[],a[],a[4], a[5], a[6], a[7]) #add the first time restriction
                if a[] or a[] or a[] or a[]: #the second time restriction exists(STARTDAY, ENDDAY, STARTTIME, ENDTIME)
                	brows.insertRow(a[2],a[],a[],a[],a[],a[4], a[5], a[6], a[7]) #add the second restriction
                elif a[] or a[] or a[] or a[]: #the third time restriction exists(STARTDAY, ENDDAY, STARTTIME, ENDTIME)
                	brows.insertRow(a[2],a[],a[],a[],a[],a[4], a[5], a[6], a[7]) #add the third time restriction
                	#does any sign have more than 3 time restrictions?
                	
                	
#Create GUIDs for time restriction table
arcpy.AddGlobalIDs_management(TimeRestriction)

#create relationship class between tables
	#between support and sign
arcpy.CreateRelationshipClass_management (Support, Sign, Support_to_Sign, "SIMPLE", 
										"has these signs", "are on this support", "BOTH", 
										"ONE-TO-MANY", "NONE", "SupportID", "SupportID")
										
	#between sign and time restrictions
arcpy.CreateRelationshipClass_management (Sign, TimeRestrictions, Sign_to_TimeRestrictions, "SIMPLE", 
										"has these time restrictions", "is related to this sign", "BOTH", 
										"ONE-TO-MANY", "NONE", "SIGNID", "SIGNID")







                	
                
                
                
        



























