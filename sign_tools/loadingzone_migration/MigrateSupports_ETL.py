#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    import arcpy
    import sys, os
    sys.path.append(r'C:\Users\jgraham\Documents\asset_checks')
    from AssetUtilities import Table2Dict as Table2Dict

    '''
    Script to migrate Sign Supports from one location to another (maintaining relation ID link)
    '''

    connection_origin = r'Database Connections\Connection to DDOTGIS as PPSA.sde'
    supports_origin = os.path.join(connection_origin,'PPSA.Supports')

    connection_destination = r'Database Connections\Connection to DDOTGIS as TOA.sde'
    supports_destination = os.path.join(connection_destination,'TOA.Supports')
    
    arcpy.AddField_management(supports_destination,'ORIGIN_ID','TEXT')

    support_field_names = ["GLOBALID", "COMMENTS", "SUPPORTTYPE", "SUPPORTSTATUS", "SUPPORTHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "ORIGIN_ID"]

    # Start an edit session. Must provide the worksapce.
    edit = arcpy.da.Editor(connection_destination)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit.startEditing(False, False)

    # Start an edit operation
    edit.startOperation()

    support_icursor = arcpy.da.InsertCursor(supports_destination, support_field_names[1:])

    with arcpy.da.SearchCursor(supports_origin,support_field_names[:-1]) as cursor:
        for row in cursor:
            #insert into new support fc
            print row[0]
            newrow = list(row[1:])
            newrow.append(row[0])
            newrow_t= tuple(newrow)
            support_icursor.insertRow(newrow_t)

    # Stop the edit operation.
    edit.stopOperation()

    # Stop the edit session and save the changes
    edit.stopEditing(True)

if __name__ == '__main__':
    main()
