#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def SnapSigns():
    import arcpy
    import sys, os

    '''
    Function to snap related sign records to supports
    '''
    connection = r'Database Connections\Connection to DDOTGIS as TOA.sde'
    supports = os.path.join(connection,'TOA.Supports')
    sign_types = ['D', 'O', 'P', 'R','S', 'W']
    sign_data = []

    for sign in sign_types:
        sign_data.append(os.path.join(connection,"TOA.Signs_" + sign))

    SupportDict={}
    support_field_names = ['OID@', 'SHAPE@X','SHAPE@Y']
    with arcpy.da.SearchCursor(supports,support_field_names) as cursor:
        for row in cursor:
            SupportDict[row[0]]=dict(zip(support_field_names[1:],row[1:]))

    print 'check'
    sign_field_names = ['SUPPORTID', 'SHAPE@X','SHAPE@Y']

    # Start an edit session. Must provide the worksapce.
    edit = arcpy.da.Editor(connection)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit.startEditing(False, False)

    # Start an edit operation
    edit.startOperation()

    for fc in sign_data:
        with arcpy.da.UpdateCursor(fc,sign_field_names) as ucursor:
            for urow in ucursor:
                if urow[0]:
                    urow[1] = SupportDict[urow[0]][sign_field_names[1]]
                    urow[2] = SupportDict[urow[0]][sign_field_names[2]]
                ucursor.updateRow(urow)

    # Stop the edit operation.
    edit.stopOperation()

    # Stop the edit session and save the changes
    edit.stopEditing(True)

def RouteSupports():

    '''
    Function to snap related sign records to supports
    '''
    from SegmentUtilities import GetOnSegPointInfo

    connection = r'Database Connections\Connection to DDOTGIS as TOA.sde'
    supports = os.path.join(connection,'TOA.Supports')

    with arcpy.da.UpdateCursor(supports,['OID@','SHAPE@X','SHAPE@Y','ROUTEID','MEASURE','SIDE','ROUTEID_ALT']) as cursor:
        for row in cursor:
            closestseg = GetOnSegPointInfo(row[1], row[2])
            if closestseg:
                row[3] = closestseg[0]
                row[4] = closestseg[1]
                row[5] = closestseg[2]
                row[6] = closestseg[5]

                cursor.updateRow(row)
                print "Completed " + str(row[0])

            else:
                print "Error finding segment for " + str(row[0])