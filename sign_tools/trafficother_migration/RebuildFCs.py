#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import arcpy
import sys, os

'''
Script to migrate Sign Supports from one location to another (maintaining relation ID link)
'''

def Table2Dict(inputFeature,key_field,field_names):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

#CONNECTIONS
connection = r'Database Connections\Connection to DDOTGIS as TOA.sde'
##FC_origin = os.path.join(connection,'TOA.TrafficPole3D')
##FC_destination = os.path.join(connection,'TOA.TrafficPole_MIGRATION')
FC_origin = os.path.join(connection,'TOA.Sign_TrafficOther3D')
FC_destination = os.path.join(connection,'TOA.Signs_TrafficOther_MIGRATION')

# Start an edit session. Must provide the worksapce.
edit1 = arcpy.da.Editor(connection)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit1.startEditing(False, False)

# Start an edit operation
edit1.startOperation()

features_loaded = 0
support_origin_field_names = ["POLEID", "POLETYPE", "POLESTATUS", "POLEHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "STREETSEGID"]
#sign_field_names = ["SIGNTEXT", "MUTCD", "ORIGIN_ID","STATUS", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SIGNARROWDIRECTION", "SUPPORTID",'SHAPE@','GLOBALID']
sign_field_names = ["SIGNID","POLEID","COMMENTS","SIGNSTATUS","SHAPE@","CREATED_USER","CREATED_DATE","LAST_EDITED_USER","LAST_EDITED_DATE","SIGNARROWDIRECTION","SERVICEDAYSTART","SERVICEDAYEND","SERVICEHOURSTART","SERVICEHOUREND","SERVICEDAYSTART_ALT","SERVICEDAYEND_ALT","SERVICEHOURSTART_ALT","SERVICEHOUREND_ALT","SIGNTYPE","MUTCD","BASIC_SIGN_CODE","ZONEID","DESTINATION_SIGN_CODE","WARD","NEARBYADDRESS","STREETSEGID","MIGRATION"]


#support_icursor = arcpy.da.InsertCursor(FC_destination, support_origin_field_names)
sign_icursor = arcpy.da.InsertCursor(FC_destination, sign_field_names)

with arcpy.da.SearchCursor(FC_origin,sign_field_names, 'SIGNSTATUS <> 5') as cursor:
    for row in cursor:

        sign_icursor.insertRow(row)
        features_loaded+=1
        print "Total Features Loaded:  " + str(features_loaded)

# Stop the edit operation.
edit1.stopOperation()

# Stop the edit session and save the changes
edit1.stopEditing(True)
