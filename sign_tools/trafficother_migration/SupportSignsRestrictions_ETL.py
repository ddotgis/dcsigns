#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    import arcpy
    import sys, os
    sys.path.append(r'C:\Users\jgraham\Documents\asset_checks')
    from AssetUtilities import Table2Dict as Table2Dict

    '''
    Test script to snap related records
    '''

    connection_origin = r'Database Connections\Connection to DDOTGIS as PPSA.sde'
    supports_origin = os.path.join(connection_origin,'PPSA.Supports')

    connection_destination = r'Database Connections\Connection to DDOTGIS as TOA.sde'
    supports_destination = os.path.join(connection_destination,'TOA.Supports')

    oldsigns_o = os.path.join(connection_origin,'PPSA.Signs_O')
    oldsigns_p = os.path.join(connection_origin,'PPSA.Signs_P')
    oldrestrictions = os.path.join(connection_origin,"PPSA.TimeRestrictions")
    
    arcpy.AddField_management(supports_destination,'ORIGIN_ID','TEXT')

##    sign_types = ['D', 'O', 'P', 'R', 'S', 'W']
##    sign_data_origin = []
##
##    for sign in sign_types:
##        sign_data_origin.append(os.path.join(connection_origin,"PPSA.Signs_" + sign))
##
##    sign_data_origin.append(os.path.join(connection_origin,"PPSA.TimeRestrictions"))


    support_field_names = ["GLOBALID", "COMMENTS", "SUPPORTTYPE", "SUPPORTSTATUS", "SUPPORTHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "ORIGIN_ID"]
    sign_field_names = ["GLOBALID", "MUTCD", "SIGNTEXT", "STATUS", "SIGNNUMBER", "SUPPORTID", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE","SHAPE@", "SIGNARROWDIRECTION"]

    # Start an edit session. Must provide the worksapce.
    edit = arcpy.da.Editor(connection_destination)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit.startEditing(False, False)

    # Start an edit operation
    edit.startOperation()

    support_icursor = arcpy.da.InsertCursor(supports_destination, support_field_names[1:])

    with arcpy.da.SearchCursor(supports_origin,support_field_names[:-1]) as cursor:
        for row in cursor:
            #insert into new support fc
            print row[0]
            newrow = list(row[1:])
            newrow.append(row[0])
            newrow_t= tuple(newrow)
            support_icursor.insertRow(newrow_t)

    # Stop the edit operation.
    edit.stopOperation()

    # Stop the edit session and save the changes
    edit.stopEditing(True)

    # start another edit session and use the newly inserted records from above
    #
    #
    #

    newsupports = Table2Dict(supports_destination,"ORIGIN_ID",support_field_names[:-1])

    # Start an edit session. Must provide the worksapce.
    edit1 = arcpy.da.Editor(connection_destination)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit1.startEditing(False, False)

    # Start an edit operation
    edit1.startOperation()

    connection_origin = r'Database Connections\Connection to DDOTGIS as PPSA.sde'
    supports_origin = os.path.join(connection_origin,'PPSA.Supports')
    newsigns_o = os.path.join(connection_destination,'TOA.Signs_O')
    newsigns_p = os.path.join(connection_destination,'TOA.Signs_P')

##    newrestrictions = os.path.join(connection_destination,'TOA.TimeRestrictions')
    
    newsigns_o_icursor = arcpy.da.InsertCursor(newsigns_o, support_field_names[1:])
    newsigns_p_icursor = arcpy.da.InsertCursor(newsigns_p, support_field_names[1:])    
##    newrestrictions_icursor = arcpy.da.InsertCursor(newsigns_p, support_field_names[1:])    


    with arcpy.da.SearchCursor(oldsigns_o,sign_field_names) as cursor:
        for row in cursor:
            newGUID = newsupports[row[0]]['GLOBALID'] #get the find the new GUID related to the original
            newrow = row[:4] + newGUID + row[5:] #insert the new GUID as the support ID

            #for each sign fc, insert their records into the appropriate destination fc
            newsigns_o_icursor.insertRow(newrow)
            
    with arcpy.da.SearchCursor(oldsigns_p,sign_field_names) as cursor:
        for row in cursor:
            newGUID = newsupports[row[0]]['GLOBALID'] #get the find the new GUID related to the original
            newrow = row[:4] + newGUID + row[5:] #insert the new GUID as the support ID

            #for each sign fc, insert their records into the appropriate destination fc
            newsigns_p_icursor.insertRow(newrow)
            
    # Stop the edit operation.
    edit1.stopOperation()

    # Stop the edit session and save the changes
    edit1.stopEditing(True)


if __name__ == '__main__':
    main()
