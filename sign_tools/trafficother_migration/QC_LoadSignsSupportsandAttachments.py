#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     14/09/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import arcpy
import os, sys

class NoFeatures(Exception):
    pass

class IncorrectNumberofFeatures(Exception):
    pass

def Table2Dict(inputFeature,key_field,field_names):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

def SpatialJoin2Dict(targetFC,keyID, matchFC,thresholdDist):
    """Spatially join two feature classes based on distance and return a join dictionary.

    targetFC -- target feature class to be spatially joined
    keyID -- key ID of target feature class
    matchFC -- matching feature class
    thresholdDist -- threshold searching distance for spatial join

    return a join dictionary in a format as join_Dict = {keyID:{'JOIN_FID':####}}
    """
    joinFC=os.path.join(sys.path[0],"spatialJoin.shp")
    arcpy.SpatialJoin_analysis(targetFC, matchFC, joinFC,join_operation="JOIN_ONE_TO_MANY",match_option="CLOSEST",search_radius=thresholdDist)
    join_fields=['JOIN_FID']
    join_Dict=Table2Dict(joinFC,keyID,join_fields)
    arcpy.Delete_management(joinFC)
    return join_Dict

def PointToPointMatch(targetFC, tgtKID, tmKID, matchFC, matchKID, join_Dict):
    """Spatially match target feature class to another feature class.

    targetFC -- target feature class
    keyID -- key ID of target feature class
    join_Dict -- a dictionary of spatial join between target feature class and matching feature class
                 format: join_Dict = {keyID:{'JOIN_FID':####}}

    return a match dictionary in a format as match_Dict = {"TARGETID":[01,02...],"MATCHID":[001,002..]}
    """
    match_Dict={"TARGETID":[],"mOBJECTID":[], "MATCHID":[]}
    with arcpy.da.SearchCursor(matchFC,['OBJECTID', matchKID], sql_clause=(None, 'ORDER BY OBJECTID')) as cursor:
        MatchFCDic = {}
        for row in cursor:
            MatchFCDic[row[0]]=row[1]

    with arcpy.da.UpdateCursor(targetFC,[tgtKID,tmKID], sql_clause=(None, 'ORDER BY '+ tgtKID)) as cursor:
        for row in cursor:
            match_Dict["TARGETID"].append(row[0])
            if row[0] in join_Dict:
                if join_Dict[row[0]]['JOIN_FID'] != -1:
                    row[1]=MatchFCDic[join_Dict[row[0]]['JOIN_FID']]
                    match_Dict["mOBJECTID"].append(join_Dict[row[0]]['JOIN_FID'])
                    match_Dict["MATCHID"].append(row[1])
                else:
                    match_Dict["mOBJECTID"].append(None)
                    match_Dict["MATCHID"].append(None)
                cursor.updateRow(row)
            else:
                print "This ID not found: ", str(row[0])
    return match_Dict

def PointToPointSnap(targetFC,keyID,matchFC,join_Dict):
    """Move targets to locations of corresponding matching features.

    targetFC -- target feature class to be moved
    keyID -- key ID of target feature class
    matchFC -- matching feature class
    join_Dict -- a dictionary of spatial join between target feature class and matching feature class
                 format: join_Dict = {keyID:{'JOIN_FID':####}}

    return a location dictionary indicates the original locations of changed records,
    in a format as loc_Dict = {"FromX":[...], "FromY":[...]}
    """
    loc_Dict = {"FromX":[], "FromY":[]}
    match_fields=['SHAPE@X','SHAPE@Y']
    matchFC_Dict=Table2Dict(matchFC,'OBJECTID',match_fields)

    with arcpy.da.UpdateCursor(targetFC,[keyID,'SHAPE@X','SHAPE@Y'],sql_clause=(None, 'ORDER BY '+keyID))as cursor:
        for row in cursor:
            if join_Dict[row[0]]['JOIN_FID'] != -1:
                loc_Dict["FromX"].append(row[1])
                loc_Dict["FromY"].append(row[2])
                row[1]=matchFC_Dict[join_Dict[row[0]]['JOIN_FID']]['SHAPE@X']
                row[2]=matchFC_Dict[join_Dict[row[0]]['JOIN_FID']]['SHAPE@Y']

                cursor.updateRow(row)
            else:
                loc_Dict["FromX"].append(None)
                loc_Dict["FromY"].append(None)
    return loc_Dict

#Populate new SIGNID from number
def PopNewIDfromNum(input_FC, IDname):
    startValue = 1
    with arcpy.da.UpdateCursor(input_FC,[IDname],sql_clause=(None, 'ORDER BY OBJECTID'))as cursor:
        for row in cursor:
            row[0]=startValue
            startValue += 1
            cursor.updateRow(row)

#Populate SIGNID = OBJECTID
def PopNewIDfromField(input_FC, IDname, refField):
    with arcpy.da.UpdateCursor(input_FC,[refField,IDname],sql_clause=(None, 'ORDER BY '+refField))as cursor:
        for row in cursor:
            row[1]=row[0]
            cursor.updateRow(row)

# Assign ObjectID to null ID value
def AssignNewIDfromOBJECTID(input_FC,IDname):
    with arcpy.da.UpdateCursor(input_FC,['OBJECTID',IDname],sql_clause=(None, 'ORDER BY OBJECTID'))as cursor:
        for row in cursor:
            if row[1] is None:
                row[1]=row[0]
                cursor.updateRow(row)

# SUPPORT ONLY:  Assign a number value to the null ID value(starting from maximum+1)
def AssignNewIDfromNum(input_FC, IDname, streetSegID):
    with arcpy.da.SearchCursor(input_FC,[IDname],sql_clause=(None, 'ORDER BY '+IDname)) as cursor:
        valueRange =[]
        for row in cursor:
            if row[0]is not None:
                valueRange.append(row[0])
        maxV = max(valueRange)

    with arcpy.da.UpdateCursor(input_FC,[IDname, "StreetSegID"],sql_clause=(None, 'ORDER BY '+IDname)) as cursor:
        for row in cursor:
            if row[0]is None:
                maxV+=1
                row[0]=maxV
                row[1]=streetSegID
                cursor.updateRow(row)
            else:
                row[1]=streetSegID
                cursor.updateRow(row)

def checkSelection(lyr):
    ''' Check whether specified layer has a selection. '''
    import arcpy
    desc = arcpy.Describe(lyr)
    featurecount = len(desc.fidSet.split(";"))
    if featurecount == 0:
        # Layer has no selection
        return 0
    else:
        # Layer has a selection
        return featurecount
'''
Pre-check selected points on this block to determine if they are associated to a support/pole
This script should be run:
    1.  As soon as any additional supports are created
    2.  Whenever signs have been created
    3.  Whenever signs have been snapped to supports
'''

outputFolder=sys.path[0]
arcpy.env.overwriteOutput = True

fc = arcpy.GetParameter(0)

DB_Connection = os.path.join("Database Connections", "Connection to DDOTGIS as TOA.sde")
#CentralDB =  os.path.join("Database Connections", "Connection to DCGISPRD_GRID.sde")
#WardFC = os.path.join(CentralDB, "DCGIS.WardPly")
#my_attachments = os.path.join(DB_Connection, "TOA.Sign_ResidentParking__ATTACH")
#sign_features = os.path.join(DB_Connection, "TOA.Sign_ResidentParking")
my_attachments = os.path.join(DB_Connection, "TOA.Sign_TrafficOther__ATTACH")

sign_features = arcpy.GetParameterAsText(0)
if checkSelection(sign_features) < 1:
    arcpy.AddMessage("Warning!! No Signs Selected.  Ending Snap Process.")
    sys.exit()
else:
    #more than one sign
    arcpy.AddMessage("Total Signs Selected: " + str(checkSelection(sign_features)))

pole_features = arcpy.GetParameterAsText(1)
if checkSelection(pole_features) < 1:
    arcpy.AddMessage("Warning!! No Supports Selected.  Ending Snap Process.")
    sys.exit()
else:
    #more than one sign
    arcpy.AddMessage("Total Supports Selected: " + str(checkSelection(pole_features)))

centerline_feature = arcpy.GetParameterAsText(2)
streetSegID = ""
if checkSelection(centerline_feature) != 1:
    arcpy.AddMessage("Warning!! Only one centerline should be selected at this time.  Ending Snap Process.")
    sys.exit()
else:
    #more than one sign
    with arcpy.da.SearchCursor(centerline_feature,["StreetSegID"]) as cursor:
        for row in cursor:
            streetSegID = row[0]
    arcpy.AddMessage("Centerline Selected: " + str(streetSegID))

sign_ID = "SIGNID"
Dist = 3 #UNITS= meters
pole_ID = "POLEID"

# Start an edit session. Must provide the worksapce.
edit = arcpy.da.Editor(DB_Connection)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit.startEditing(False, False)

# Start an edit operation
edit.startOperation()

#Snap all supports to poles on this block, populate PoleID
# Make sure all the KeyIDs of sign and pole feature class are not null
AssignNewIDfromOBJECTID(sign_features, "SIGNID")
AssignNewIDfromNum(pole_features, "POLEID", streetSegID)

# Operations included below: Spatial join sign and poles; Eventually Count attachments for signs
joinDict = SpatialJoin2Dict(sign_features,sign_ID,pole_features,Dist)
matchDict = PointToPointMatch(sign_features,sign_ID,pole_ID,pole_features,pole_ID,joinDict)
locDict = PointToPointSnap(sign_features,sign_ID, pole_features,joinDict)

# Stop the edit operation.
edit.stopOperation()

# Stop the edit session and save the changes
edit.stopEditing(True)


