#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------


import arcpy
import sys, os
sys.path.append(r'C:\Users\jgraham\Documents\asset_checks')
from AssetUtilities import Table2Dict as Table2Dict

'''
Script to migrate Signs and restrictions maintaining relation link to supports
'''


try:
    sign_features = arcpy.GetParameterAsText(0)
    selectedsigns =checkSelection(sign_features)
    if len(selectedsigns) >= 1:
        #at least one sign
        arcpy.AddMessage("Total Signs Selected: " + ",".join(selectedsigns))
        sign_filter = "OBJECTID in(" + ",".join(selectedsigns) + ")"

except:
    arcpy.AddWarning("Warning!! No Signs Selected.  Ending Sign ETL Process.")
    arcpy.SetParameterAsText(4,"error")
    sys.exit()


connection_destination = r'Database Connections\Connection to DDOTGIS as TOA.sde'
thisfc ='TOA.Signs_TrafficOther'
#thisfc ='TOA.Signs_ResidentParking'
#thisfc ='TOA.Signs_LoadingZones'
oldsigns_p = os.path.join(connection_destination,thisfc)

supports_destination = os.path.join(connection_destination,'TOA.Supports')

newsigns_p = os.path.join(connection_destination,'TOA.Signs_P')
newsigns_p_restrictions = os.path.join(connection_destination,'TOA.TimeRestrictions')
newattach = os.path.join(connection_destination,'TOA.Supports__ATTACH')


support_field_names = ["GLOBALID", "COMMENTS", "SUPPORTTYPE", "SUPPORTSTATUS", "SUPPORTHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "ORIGIN_ID"]
sign_field_names = ["SIGNTEXT", "MUTCD", "ORIGIN_ID","STATUS", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SIGNARROWDIRECTION", "SUPPORTID",'SHAPE@']
attach_field_names = ['GLOBALID','CONTENT_TYPE','ATT_NAME','DATA_SIZE','DATA','REL_GLOBALID','ORIGIN_ID']
restriction_fieldnames = ['HOURLIMIT', 'RESTRICTIONORDER', 'STARTDAY', 'ENDDAY', 'STARTTIME', 'ENDTIME', 'EXCEPTION', 'SIGNID', 'CREATED_USER', 'CREATED_DATE','LAST_EDITED_USER', 'LAST_EDITED_DATE', 'ORIGIN_ID']

newsupports = Table2Dict(supports_destination,"ORIGIN_ID",support_field_names[:-1])


# Start an edit session. Must provide the worksapce.
edit2 = arcpy.da.Editor(connection_destination)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit2.startEditing(False, False)

# Start an edit operation
edit2.startOperation()

#In the field list, add fields that you want to be populated, currently it might not be comprehensive
rows = arcpy.da.InsertCursor(newsigns_p, sign_field_names)

#Signs_ResidentParking
#with arcpy.da.SearchCursor(oldsigns_p, ['SIGN_TEXT','PARKING_TYPE','SIGNID','SIGNSTATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','PARKING_ARROW','POLEID','SHAPE@XY',
                                        #'ServiceDayStart','ServiceDayEnd','ServiceHourStart', 'ServiceHourEnd',
                                        #'ServiceDayStart_ALT','ServiceDayEnd_ALT','ServiceHourStart_ALT', 'ServiceHourEnd_ALT']],
                                        #where_clause=sign_filter) as cursor:

#Signs_TrafficOther
with arcpy.da.SearchCursor(oldsigns_p, ['COMMENTS','MUTCD','SIGNID','SIGNSTATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','SIGNARROWDIRECTION','POLEID','SHAPE@',
                                        'ServiceDayStart','ServiceDayEnd','ServiceHourStart', 'ServiceHourEnd',
                                        'ServiceDayStart_ALT','ServiceDayEnd_ALT','ServiceHourStart_ALT', 'ServiceHourEnd_ALT'],
                                        where_clause=sign_filter) as cursor:

    for a in cursor:
        rows.insertRow(a[0], a[1], thisfc + str(a[2]), a[3], a[4], a[5], a[6], a[7], a[8], newsupports[a[9]]['GLOBALID'], newsupports[a[9]]['SHAPE@']) #Insert sign

# Stop the edit operation.
edit2.stopOperation()

# Stop the edit session and save the changes
edit2.stopEditing(True)

newsigns = Table2Dict(newsigns_p,"ORIGIN_ID",sign_field_names)

# Start an edit session. Must provide the worksapce.
edit3 = arcpy.da.Editor(connection_destination)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit3.startEditing(False, False)

# Start an edit operation
edit3.startOperation()

restrictionrows = arcpy.da.InsertCursor(newsigns_p_restrictions, ['SIGNID','STARTDAY','ENDDAY','STARTTIME','ENDTIME','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE'])

#Signs_TrafficOther
with arcpy.da.SearchCursor(oldsigns_p, ['COMMENTS','MUTCD','SIGNID','SIGNSTATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','SIGNARROWDIRECTION','POLEID','SHAPE@',
                                        'ServiceDayStart','ServiceDayEnd','ServiceHourStart', 'ServiceHourEnd',
                                        'ServiceDayStart_ALT','ServiceDayEnd_ALT','ServiceHourStart_ALT', 'ServiceHourEnd_ALT'],
                                        where_clause=sign_filter) as cursor:

    for a in cursor:
        restrictionrows.insertRow(newsigns[thisfc + str(a[2])]['GLOBALID'],a[11],a[12],a[13],a[14],a[4], a[5], a[6], a[7]) #add the first time restriction
        if a[15] or a[16] or a[17] or a[18]: #some form of second time restriction exists in the 'alt' secondary restriction info (STARTDAY, ENDDAY, STARTTIME, ENDTIME)
        	restrictionrows.insertRow(newsigns[thisfc + str(a[2])]['GLOBALID'],a[15],a[16],a[17],a[18],a[4],a[5],a[6],a[7]) #add the second restriction


