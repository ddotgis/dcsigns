#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    import arcpy
    import sys, os
    sys.path.append(r'C:\Users\jgraham\Documents\asset_checks')
    from AssetUtilities import Table2Dict as Table2Dict

    '''
    Script to migrate restrictions, maintaining relation link to supports
    '''

    connection_origin = r'Database Connections\Connection to DDOTGIS as PPSA.sde'
    oldrestrictions = os.path.join(connection_origin,'PPSA.TimeRestrictions')
    oldsupports = os.path.join(connection_origin,'PPSA.Supports')

    connection_destination = r'Database Connections\Connection to DDOTGIS as TOA.sde'
    newrestrictions = os.path.join(connection_destination,'TOA.TimeRestrictions')
    newsupports = os.path.join(connection_destination,'TOA.Supports')
    newsigns_o = os.path.join(connection_destination,'TOA.Signs_O')
    newsigns_p = os.path.join(connection_destination,'TOA.Signs_P')


    restriction_fieldnames = ['GLOBALID', 'HOURLIMIT', 'RESTRICTIONORDER', 'STARTDAY', 'ENDDAY', 'STARTTIME', 'ENDTIME', 'EXCEPTION', 'SIGNID', 'CREATED_USER', 'CREATED_DATE','LAST_EDITED_USER', 'LAST_EDITED_DATE', 'ORIGIN_ID']
    support_field_names = ["GLOBALID", "COMMENTS", "SUPPORTTYPE", "SUPPORTSTATUS", "SUPPORTHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "ORIGIN_ID"]
    sign_field_names = ["GLOBALID", "MUTCD", "SIGNTEXT", "STATUS", "SIGNNUMBER", "SUPPORTID", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE","SHAPE@", "SIGNARROWDIRECTION", "ORIGIN_ID"]
    newsignage_p = Table2Dict(newsigns_p,"ORIGIN_ID",["GLOBALID"])
    newsupports = Table2Dict(newsigns_o,"ORIGIN_ID",["GLOBALID"])

    arcpy.AddField_management(newrestrictions,'ORIGIN_ID','TEXT')

    #make one big dict to look up any original sign id, rgardless of type
    #newsignage = newsignage_p.copy()
    #newsignage.update(newsignage_o)

    # Start an edit session. Must provide the worksapce.
    edit1 = arcpy.da.Editor(connection_destination)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit1.startEditing(False, False)

    # Start an edit operation
    edit1.startOperation()

    newrestrictions_fields = list(restriction_fieldnames)
    newrestrictions_fields.remove('GLOBALID') #On New records, omit the global id field because this will be generated automatically
    restriction_fieldnames.remove('ORIGIN_ID') #On original/old records, omit the origin id field because this field doesn't exist

    restrictions_icursor = arcpy.da.InsertCursor(newrestrictions, newrestrictions_fields)   #Exclude GlobalID and SignArrowDirection

    with arcpy.da.SearchCursor(oldrestrictions,restriction_fieldnames) as cursor:
        for row in cursor:
            newGUID = newsignage_p[row[8]]['GLOBALID'] #the new GUID related to the original
            print newGUID
            newrow1 = list(row[1:8])
            newrow1.append(newGUID) #new GUID
            newrow2 = list(row[9:])
            newrow2.append(row[0]) #Original ID
            newrowlist = newrow1 + newrow2

            #insert the new GUID as the SIGN ID
            newrow = tuple(newrowlist)
            #for each sign fc, insert their records into the appropriate destination fc
            restrictions_icursor.insertRow(newrow)

    # Stop the edit operation.
    edit1.stopOperation()

    # Stop the edit session and save the changes
    edit1.stopEditing(True)


if __name__ == '__main__':
    main()
