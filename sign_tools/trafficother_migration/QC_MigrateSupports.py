#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import arcpy
import sys, os

'''
Script to migrate Sign Supports from one location to another (maintaining relation ID link)
'''

def Table2Dict(inputFeature,key_field,field_names):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

def checkSelection(lyr):
    ''' Check whether specified layer has a selection. '''
    import arcpy
    desc = arcpy.Describe(lyr)
    if len(desc.fidSet) == 0:
        return 0
    else:
        # Layer has a selection
        features = desc.fidSet.split(";")
        return features

#:::: SIGN FEATURES TO MIGRATE :::::
thisfc ='TOA.Signs_TrafficOther_MIGRATION'
#thisfc ='TOA.Sign_ResidentParking'
#thisfc ='TOA.Sign_LoadingZones'

#PROD
connection_origin = r'Database Connections\Connection to DDOTGIS as TOA.sde'
connection_destination = r'Database Connections\Connection to DDOTGIS as TOA.sde'
supports_origin = os.path.join(connection_origin,'TOA.TrafficPole_MIGRATION')
supports_destination = os.path.join(connection_destination,'TOA.Supports')
oldsigns_p = os.path.join(connection_destination,thisfc)
newsigns_p = os.path.join(connection_destination,'TOA.Signs_P')
newsigns_p_restrictions = os.path.join(connection_destination,'TOA.TimeRestrictions')
newattach = os.path.join(connection_destination,'TOA.Supports__ATTACH')


#FIELD DEFINITIONS

sign_field_names = ["SIGNTEXT", "MUTCD", "ORIGIN_ID","STATUS", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SIGNARROWDIRECTION", "SUPPORTID",'SHAPE@','GLOBALID']
attach_field_names = ['GLOBALID','CONTENT_TYPE','ATT_NAME','DATA_SIZE','DATA','REL_GLOBALID','ORIGIN_ID']
restriction_fieldnames = ['HOURLIMIT', 'RESTRICTIONORDER', 'STARTDAY', 'ENDDAY', 'STARTTIME', 'ENDTIME', 'EXCEPTION', 'SIGNID', 'CREATED_USER', 'CREATED_DATE','LAST_EDITED_USER', 'LAST_EDITED_DATE', 'ORIGIN_ID']

pole_features = arcpy.GetParameterAsText(1)
selected_poles = ""

if checkSelection(pole_features) < 1:
    arcpy.AddError("Warning!! No Supports Selected.  Ending ETL Process.")
    arcpy.SetParameterAsText(3,"error")
    sys.exit()
else:
    #more at least 1 support seelected
    selectedresult = checkSelection(pole_features)
    arcpy.AddMessage("Total Supports Selected: " + str(len(selectedresult)))
    selected_poles = "OBJECTID in(" + ",".join(selectedresult) + ")"
    arcpy.AddMessage("Supports Selected for ETL: " + selected_poles)

#selected_poles = "OBJECTID in(54744)"

support_crosswalk = {1:6,
                    2:1,
                    3:10,
                    4:14,
                    5:14,
                    6:8,
                    7:14,
                    8:11,
                    9:11}


# Start an edit session. Must provide the worksapce.
edit1 = arcpy.da.Editor(connection_destination)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit1.startEditing(False, False)

# Start an edit operation
edit1.startOperation()

support_field_names = ["GLOBALID", "COMMENTS", "SUPPORTTYPE", "SUPPORTSTATUS", "SUPPORTHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "ORIGIN_ID", "ROUTEID"]
support_origin_field_names = ["POLEID", "COMMENTS", "POLETYPE", "POLESTATUS", "POLEHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "STREETSEGID","migration"]

support_icursor = arcpy.da.InsertCursor(supports_destination, support_field_names[1:])
supportcount = 0
currentsupports = Table2Dict(supports_destination,"ORIGIN_ID",support_field_names[:-1])

with arcpy.da.UpdateCursor(supports_origin,support_origin_field_names, where_clause=selected_poles) as cursor:
    for row in cursor:
        arcpy.AddMessage("attempting to migrate the following Support OIDs: " + selected_poles)
        #first check if the support has all the needed info and is valid
        if not row[0] > 0:
            arcpy.AddError("Warning!! Support " + str(row[0]) + " does not have valid PoleID. Please rerun the snapping process for this support.")
            sys.exit()

        if not row[10] > 0:
            arcpy.AddError("Warning!! Support " + str(row[0]) + " does not have valid Centerline ID. Please resnap/associate to centerline.  Ending ETL.")
            sys.exit()

        if str(row[0]) in currentsupports:
            #this support has already been migrated
            arcpy.AddError("Warning!! Support " + str(row[0]) + " has previously been migrated and is already present in new schema.")
            arcpy.AddError("Using Existing Support and Continuing ETL")
            #sys.exit()

        #passed cursory checks.  insert into new support fc
        newrow = [row[1],#comments
                support_crosswalk[row[2]],#type
                row[3],#status
                row[4],#height
                row[5],#created-user
                row[6],#created-date
                row[7],#edited-user
                row[8],#edited-date
                row[9],#shape
                row[0],#origin-id (from pole-id)
                row[10]]#routeid (from streetsegid)

        support_icursor.insertRow(newrow)
        arcpy.AddMessage("Passed 'insertRow' on Support OIDs: " + selected_poles)
        supportcount+=1
        row[11] = "COMPLETE" #indicate that this support has sucessfully been migrated
        cursor.updateRow(row)
        arcpy.AddMessage("Passed 'updateRow' on Support OIDs: " + selected_poles)

# Stop the edit operation.
edit1.stopOperation()
arcpy.AddMessage("Passed 'stopOperation' on Support OIDs: " + selected_poles)
# Stop the edit session and save the changes
edit1.stopEditing(True)

try:
    sign_features = arcpy.GetParameterAsText(0)
    selectedsigns =checkSelection(sign_features)
    if len(selectedsigns) >= 1:
        #at least one sign
        arcpy.AddMessage("Total Signs Selected: " + ",".join(selectedsigns))
        sign_filter = "OBJECTID in(" + ",".join(selectedsigns) + ")"

except:
    arcpy.AddWarning("Warning!! No Signs Selected.  Ending Sign ETL Process.")
    sys.exit()

#sign_filter = "OBJECTID in(27944)"




newsupports = Table2Dict(supports_destination,"ORIGIN_ID",support_field_names[:-1])

# Start an edit session. Must provide the worksapce.
edit2 = arcpy.da.Editor(connection_destination)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit2.startEditing(False, False)

# Start an edit operation
edit2.startOperation()

irows = arcpy.da.InsertCursor(newsigns_p, sign_field_names[:-1])
signcount = 0
currentsigns = Table2Dict(newsigns_p,"ORIGIN_ID",sign_field_names)
#Signs_ResidentParking
#with arcpy.da.SearchCursor(oldsigns_p, ['SIGN_TEXT','PARKING_TYPE','SIGNID','SIGNSTATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','PARKING_ARROW','POLEID','SHAPE@XY',
                                        #'ServiceDayStart','ServiceDayEnd','ServiceHourStart', 'ServiceHourEnd',
                                        #'ServiceDayStart_ALT','ServiceDayEnd_ALT','ServiceHourStart_ALT', 'ServiceHourEnd_ALT'],
                                        #where_clause=sign_filter) as cursor:

#INSERT NEW SIGN FEATURES
#Signs_TrafficOther
with arcpy.da.UpdateCursor(oldsigns_p, ['COMMENTS','MUTCD','SIGNID','SIGNSTATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','SIGNARROWDIRECTION','POLEID','SHAPE@','OID@','migration',
                                        'ServiceDayStart','ServiceDayEnd','ServiceHourStart', 'ServiceHourEnd',
                                        'ServiceDayStart_ALT','ServiceDayEnd_ALT','ServiceHourStart_ALT', 'ServiceHourEnd_ALT'],
                                        where_clause=sign_filter) as cursor:

    for a in cursor:

        if thisfc + str(a[2]) in currentsigns:
            #this sign has already been migrated
            arcpy.AddError("Warning!! Sign ID " + str(row[2]) + " has previously been migrated and is already present in new schema.  Ending ETL.")
            sys.exit()

        signcount +=1
        irows.insertRow((a[0], a[1], thisfc + str(a[2]), a[3], a[4], a[5], a[6], a[7], a[8], newsupports[str(a[9])]['GLOBALID'], newsupports[str(a[9])]['SHAPE@'])) #Insert sign
        a[12] = 'Signs:COMPLETE'
        cursor.updateRow(a)

# Stop the edit operation.
edit2.stopOperation()
arcpy.AddMessage("Passed 'stopOperation' on Support OIDs: " + selected_poles)
# Stop the edit session and save the changes
edit2.stopEditing(True)

# Start an edit session. Must provide the worksapce.
edit3 = arcpy.da.Editor(connection_destination)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit3.startEditing(False, False)

# Start an edit operation
edit3.startOperation()

newsigns = Table2Dict(newsigns_p,"ORIGIN_ID",sign_field_names)

restrictionrows = arcpy.da.InsertCursor(newsigns_p_restrictions, ['SIGNID','STARTDAY','ENDDAY','STARTTIME','ENDTIME','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE', 'ORIGIN_ID'])
restrictioncount = 0

#INSERT NEW RESTRICTIONS, update original fc to mark as complete
#Signs_TrafficOther
with arcpy.da.UpdateCursor(oldsigns_p, ['COMMENTS','MUTCD','SIGNID','SIGNSTATUS','CREATED_USER','CREATED_DATE','LAST_EDITED_USER','LAST_EDITED_DATE','SIGNARROWDIRECTION','POLEID','SHAPE@',
                                        'ServiceDayStart','ServiceDayEnd','ServiceHourStart', 'ServiceHourEnd',
                                        'ServiceDayStart_ALT','ServiceDayEnd_ALT','ServiceHourStart_ALT', 'ServiceHourEnd_ALT','migration'],
                                        where_clause=sign_filter) as cursor:

    for a in cursor:
        if a[11] or a[12] or a[13] or a[14]:
            restrictionrows.insertRow((newsigns[thisfc + str(a[2])]['GLOBALID'],a[11],a[12],a[13],a[14],a[4], a[5], a[6], a[7], thisfc + str(a[2]))) #add the first time restriction
            restrictioncount+=1
        #some form of second time restriction exists in the 'alt' secondary restriction info (STARTDAY, ENDDAY, STARTTIME, ENDTIME)
        if a[15] or a[16] or a[17] or a[18]:
            restrictionrows.insertRow((newsigns[thisfc + str(a[2])]['GLOBALID'],a[15],a[16],a[17],a[18],a[4],a[5],a[6],a[7], thisfc + str(a[2])))
            restrictioncount+=1

        a[19] += ", Restrictions:COMPLETE" #indicate that the sign restrictions have sucessfully been migrated
        cursor.updateRow(a)


# Stop the edit operation.
edit3.stopOperation()
arcpy.AddMessage("Passed 'stopOperation' on Support OIDs: " + selected_poles)
# Stop the edit session and save the changes
edit3.stopEditing(True)
arcpy.AddMessage("Total Supports Loaded to new Schema: " + str(supportcount))
arcpy.AddMessage("Total Signs Loaded to new Schema: " + str(signcount))
arcpy.AddMessage("Total Restrictions Loaded to new Schema: " + str(restrictioncount))


