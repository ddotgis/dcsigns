#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import arcpy
import sys, os

'''
Script to migrate Sign Supports from one location to another (maintaining relation ID link)
'''

def Table2Dict(inputFeature,key_field,field_names):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

def checkSelection(lyr):
    ''' Check whether specified layer has a selection. '''
    import arcpy
    desc = arcpy.Describe(lyr)
    if len(desc.fidSet) == 0:
        return 0
    else:
        # Layer has a selection
        features = desc.fidSet.split(";")
        return features


pole_features = arcpy.GetParameterAsText(1)
selected_poles = ""
if checkSelection(pole_features) < 1:
    arcpy.AddError("Warning!! No Supports Selected.  Ending ETL Process.")
    sys.exit()
else:
    #more at least 1 support seelected
    selectedresult = checkSelection(pole_features)
    arcpy.AddMessage("Total Supports Selected: " + str(len(selectedresult)))
    selected_poles = "OBJECTID in(" + ",".join(selectedresult) + ")"
    arcpy.AddMessage("Supports Selected for ETL: " + selected_poles)


connection_origin = r'Database Connections\Connection to DDOTGIS as TOA.sde'
supports_origin = os.path.join(connection_origin,'TOA.TrafficPole3D')

connection_destination = r'Database Connections\Connection to DDOTGIS as TOA.sde'
supports_destination = os.path.join(connection_destination,'TOA.Supports')

arcpy.AddField_management(supports_destination,'ORIGIN_ID','TEXT')

support_origin_field_names = ["POLEID", "COMMENTS", "POLETYPE", "POLESTATUS", "POLEHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "STREETSEGID","migration"]
support_field_names = ["COMMENTS", "SUPPORTTYPE", "SUPPORTSTATUS", "SUPPORTHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "ORIGIN_ID", "ROUTEID"]

support_crosswalk = {1:6,
                    2:1,
                    3:10,
                    4:14,
                    5:14,
                    6:8,
                    7:14,
                    8:11,
                    9:11}

# Start an edit session. Must provide the worksapce.
edit = arcpy.da.Editor(connection_destination)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit.startEditing(False, False)

# Start an edit operation
edit.startOperation()

support_icursor = arcpy.da.InsertCursor(supports_destination, support_field_names)

with arcpy.da.UpdateCursor(supports_origin,support_origin_field_names, where_clause=selected_poles) as cursor:
    for row in cursor:
        arcpy.AddMessage("attempting to migrate the following Support OIDs: " + selected_poles)
        #first check if the support has all the needed info and is valid
        if not row[0] > 0:
            arcpy.AddError("Warning!! Support " + str(row[0]) + " does not have valid PoleID. Please rerun the snapping process for this support.")
            sys.exit()

        if not row[10] > 0:
            arcpy.AddError("Warning!! Support " + str(row[0]) + " does not have valid Centerline ID. Please resnap/associate to centerline.  Ending ETL.")
            sys.exit()

        #passed cursory checks.  insert into new support fc
        newrow = [row[1],support_crosswalk[row[2]],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[0],row[10]]
        support_icursor.insertRow(newrow)
        row[11] = "COMPLETE" #indicate that this support has sucessfully been migrated
        cursor.updateRow(row)

# Stop the edit operation.
edit.stopOperation()

# Stop the edit session and save the changes
edit.stopEditing(True)

