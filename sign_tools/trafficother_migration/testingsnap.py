#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     13/10/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------


import arcpy
import os, sys
import pprint
def Table2Dict(inputFeature,key_field,field_names):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

def SpatialJoin2Dict(snaptothisFC,movingFC,snaptothisFC_ID, movingFC_ID, thresholdDist):
    """Spatially join two feature classes based on distance and return a join dictionary.
    return a join dictionary in a format as join_Dict = {keyID:{'JOIN_FID':####}}
    """

    joinFC=os.path.join(sys.path[0],"working.gdb","spatialJoin")
    arcpy.SpatialJoin_analysis(snaptothisFC, movingFC, joinFC,join_operation="JOIN_ONE_TO_ONE",match_option="CLOSEST",search_radius=thresholdDist)
    join_fields=[snaptothisFC_ID,'SHAPE@']
    join_Dict=Table2Dict(joinFC,movingFC_ID,join_fields)
    arcpy.Delete_management(joinFC)

    return join_Dict

def PointSnapperWithID(snaptothisFC_Dict, snaptothisFC_ID, movingFC_ID, movingFC):
    """Spatially match target feature class to another feature class.
    This is a sister function to SpatialJoin2Dict.  Use this function to join up two point FCs.
    The result is the Shape and ID of the point being snapped to. (such as a sign support)
    k = movingFC_ID
    v = {snaptothisID:... , SHAPE@:<shape>}
    """

    movingFC_ID_list = snaptothisFC_Dict.keys()
    ID_string = movingFC_ID_list.split(",")
    where_clause = movingFC_ID + " in(" + ID_string + ")"
    print where_clause

    with arcpy.da.UpdateCursor(movingFC,[snaptothisFC_ID, movingFC_ID, 'SHAPE@'], where_clause=where_clause) as cursor:
        for row in cursor:
            row[0] = snaptothisFC_Dict[row[1]][snaptothisFC_ID]
            row[2] = snaptothisFC_Dict[row[1]]['SHAPE@']
            cursor.updateRow(row)

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
DB_Connection = os.path.join(scriptloc,"connections","Connection to DDOTGIS as DDOTEDITOR.sde")

mysupports = os.path.join("Database Connections","Connection to DDOTGIS as DDOTEditor.sde", "TOA.TrafficPole_MIGRATION")
support_filter = "OBJECTID in(80326)"
pole_features = "pole_features"
arcpy.MakeFeatureLayer_management(mysupports,pole_features,support_filter)

mysigns = os.path.join("Database Connections","Connection to DDOTGIS as DDOTEditor.sde","TOA.Signs_TrafficOther_MIGRATION")
sign_filter = "OBJECTID in(195,516,517)"
sign_features = "sign_features"
arcpy.MakeFeatureLayer_management(mysigns,sign_features,sign_filter)


# Start an edit session. Must provide the worksapce.
edit = arcpy.da.Editor(DB_Connection)

# Edit session is started without an undo/redo stack for versioned data
#  (for second argument, use False for unversioned data)
edit.startEditing(False, False)

# Start an edit operation
edit.startOperation()

myresult = SpatialJoin2Dict(pole_features,sign_features,'POLEID','SIGNID',5)
PointSnapperWithID(myresult,'POLEID','SIGNID',sign_features)

# Stop the edit operation.
edit.stopOperation()

# Stop the edit session and save the changes
edit.stopEditing(True)
print 'whoa'