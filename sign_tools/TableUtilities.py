import arcpy
def Table2Dict(inputFeature,key_field,field_names):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

def JoinTable(inFeature, inField, joinTable, joinField):
    """Join table with other table files.

    inFeature -- input feature
    inField -- key field in input feature
    joinTable -- join table to be appended to input feature
    joinField -- key field in join table
    outFeature -- output feature
    """
    import arcpy, traceback, sys
    arcpy.env.workspace = sys.path[0]
    arcpy.env.overwriteOutput = True

    if not joinTable.endswith('.dbf'):
        joinDBF = joinTable.split('.',1)[0]+".dbf"
        outputFolder = sys.path[0]
        arcpy.TableToTable_conversion(joinTable, outputFolder, joinDBF)
    arcpy.JoinField_management (inFeature, inField, joinDBF, joinField)
    arcpy.Delete_management (joinDBF)

