import arcpy
import sys, os
import time

def Table2Dict(inputFeature,key_field,field_names,filter=None):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names,where_clause=filter) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
checkfolder = raw_input("Enter the Folder to Check")

gdb = os.path.join(scriptloc, "errorchecks.gdb")

if not arcpy.Exists(gdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(gdb))

signs_P =os.path.join(scriptloc, checkfolder, 'trafficsigns_join.shp')
supports_P =os.path.join(scriptloc, checkfolder,'supports_routid.shp')
errortable =os.path.join(gdb, 'errortable_' + checkfolder)

if not arcpy.Exists(errortable):
    arcpy.CreateTable_management(gdb,os.path.basename(errortable))
    arcpy.AddField_management(errortable, "SupportID", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(errortable, "SignID", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(errortable, "ErrorText", "TEXT", 255, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(errortable, "ErrorType", "TEXT", 255, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(errortable, "X", "FLOAT", 12, 2, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(errortable, "Y", "FLOAT", 12, 2, "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(errortable, "Z", "FLOAT", 12, 8, "", "", "NULLABLE", "REQUIRED")

else:
    arcpy.TruncateTable_management(errortable)


#load all supports
supports_dict = Table2Dict(supports_P, 'SupportID',['GlobalID','objecttype'])
#insert errors

requirestime = ['R-NS-067','R-NS-006','R-NS-012','R-NS-013','R-NS-022','R-NS-026','R-NS-052','R-NS-053','R-NS-064','R-NS-075','R-NS-122','R-NS-174','R-NS-180',
                   'R-NS-185','R-NS-210','R-NS-213','R-NS-214','R-NS-LZSHARED','R-NS_ROP','R-NS-RPP','R-DC-2HROLD','R-DC-2HROLD','R-DC-2HR','R-DC-NSNP',
                   'R-DC-NSNP_EXCEPTION','R-DC-PTPCOIN','R-DC-No_Parking_Generic_w_Time','R-DC-Diplomat_Eq_Guinea','R-DC-Embassy _Angola',
                   'R-DC-15_Min_Parking1','R-DC-One_Hour_Parking_Eastern_Market','R-DC-School_Parking_Zone_15_Min','R-DC-School_Loading_Zones2','R-DC-School_Loading_Zones3',
                   'R-DC-15_Min_Parking2','R-NS-002','R-NS-153','R-NS-020','W-NS-027','R-NS-032','R-NS-033',
                   'R-NS-171','R-NS-073','R-NS-077','R-DC-One_Way_Yellow','R-DC-Reserved_DCGov','R-NS-047','R-NS-030','D-NS-007','R-DC-Commercial_Vehicles_Loading']

optionaltime = ['R-DC-NO_PARK_ENTRANCE_TIMES','R-NS-173','R-NS-LZPTL']

requiresday = ['R-NS-006','R-NS-012','R-NS-013','R-NS-022','R-NS-026','R-NS-052','R-NS-053','R-NS-075','R-NS-122','R-NS-174','R-NS-180',
                   'R-NS-185','R-NS-210','R-NS-213','R-NS-214','R-NS-LZSHARED','R-NS_ROP','R-NS-RPP','R-DC-2HROLD','R-DC-2HROLD','R-DC-2HR','R-DC-NSNP',
                   'R-DC-NSNP_EXCEPTION','R-DC-PTPCOIN','R-DC-No_Parking_Generic_w_Time','R-DC-Diplomat_Eq_Guinea','R-DC-Embassy _Angola',
                   'R-DC-15_Min_Parking1','R-DC-15_Min_Parking2','R-NS-002','R-NS-153','R-NS-020','W-NS-027','R-NS-032','R-NS-033',
                   'R-NS-171','R-NS-073','R-NS-077','R-DC-One_Way_Yellow','R-DC-Reserved_DCGov','R-NS-047','R-NS-030','D-NS-007','R-DC-Commercial_Vehicles_Loading']

optionalday = ['R-DC-NO_PARK_ENTRANCE_TIMES','R-NS-064','R-DC-School_Parking_Zone_15_Min','R-DC-School_Loading_Zones2','R-DC-School_Loading_Zones3','R-NS-173','R-NS-LZPTL']

requiresMPH = ['R-NS-173','R-NS-071','W-NS-008','R-NS-037','R-NS-110','R-NS-103']

requiresarrow = ['R-NS-006','R-NS-011','R-NS-012','R-NS-013','R-NS-015','R-NS-017','R-NS-019','R-NS-022','R-NS-026','R-NS-038','R-NS-052','R-NS-053','R-NS-059',
                 'R-NS-064','R-NS-075','R-NS-080','R-NS-121','R-NS-122','R-NS-131','R-NS-133','R-NS-141','R-NS-148','R-NS-172','R-NS-174','R-NS-180',
                 'R-NS-185','R-NS-210','R-NS-215','R-NS-213','R-NS-214','R-NS-056','R-NS-LZSHARED','R-NS-OLD','R-NS_ROP','R-NS-RPP','R-DC-2HROLD','R-DC-2HROLD',
                 'R-DC-2HR','R-DC-NSNP','R-DC-NSNP_EXCEPTION','R-DC-PTPCOIN','R-DC-Taxi_2','R-DC-NO_PARK_ENTRANCE_TIMES','R-DC-Diplomat','R-DC-No_Park_Russia_Embassy',
                 'R-DC-No_Parking_Generic_w_Time','R-DC-Diplomat_Kazakhstan','R-DC-No_Stand_Bus','R-DC-Diplomat_Eq_Guinea','R-DC-Embassy _Angola','R-DC-Embassy _Diplomat_Mexico',
                 'R-DC-15_Min_Parking1','R-DC-One_Hour_Parking_Eastern_Market','R-DC-School_Parking_Zone_15_Min',
                 'R-DC-School_Loading_Zones2','R-DC-School_Loading_Zones3','R-DC-15_Min_Parking2','R-DC-Back_in_Parking',
                 'R-NS-157','R-NS-120','R-DC-One_Way_Yellow','R-DC-Reserved_DCGov','D-NS-011','D-NS-038','D-NS-039','D-NS-055','R-NS-046','R-NS-134','R-DC-Hotel_Load','R-NS-LZPTL',
                 'R-NS-LZSPECIAL','O-NS-024']

optionalarrow = ['R-DC-No_Parking_Alley','R-DC-No_Park_Stadium_Event']

requireszone = ['R-NS_ROP','R-NS-RPP','R-DC-PMPBP','R-DC-PBC_PLAQUE','R-DC-ROP_Exception_Plaque','R-DC-RPP_Exception_Plaque','R-NS-LZPTL']

requireshourlimit = ['R-NS-RPP','R-DC-2HROLD','R-DC-2HR','R-DC-PTPCOIN','R-NS-LZPTL']

dayindex = {'Monday':1,
            'Tuesday':2,
            'Wednesday':3,
            'Thursday':4,
            'Friday':5,
            'Saturday':6,
            'Sunday':7,
            'Anytime':8}


icursor = arcpy.da.InsertCursor(errortable, ["SupportID","SignID","ErrorText","ErrorType", "X", "Y", "Z"])

signs = dict()
supports = dict()
errorsigns = dict()
errorsupports = dict()

with arcpy.da.SearchCursor(signs_P,['SupportID','GlobalID','signtype', 'signtext','startday','endday','starttime','endtime','hourlimit','zoneid',
                                    'startday2','endday2','starttime2','endtime2','arrow', 'SHAPE@XY', 'SHAPE@Z']) as cursor:
    for row in cursor:
        signs[row[1]] = 0
        supports[row[0]] = 0
        if row[0] not in supports_dict:
            #SupportID in SignFC should be present in SupportFC
            errorsigns[row[1]] = 0
            errorsupports[row[0]] = 0
            print 'unmatched support on: ' + row[0]
            icursor.insertRow([row[0],row[1],
                               "SupportID " + row[0] + " is not in the feature class:" + os.path.basename(supports_P), "ERROR", row[15][0],row[15][1] ,row[16]])

        if 'unreadable' in row[3].lower():
            #sign said to be obscured/unreadable            
            continue
        if row[2] in requirestime:
            #Check to make sure sign has start time            
            if not row[6] or row[6] == '' or row[6] == ' ':
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                
                print 'missing or incomplete start time on sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'missing or incomplete start time on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])
            #Check to make sure sign has end time
            if not row[7] or row[7] == '' or row[7] == ' ':
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                
                print 'missing or incomplete end time on sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'missing or incomplete end time on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])
            #Check if starttime occurs on or after endtime
            if row[6] and row[6] != '' and row[6] != ' ' and row[7] and row[7] != '' and row[7] != ' ':
                #both start and end days are good for evaluation
                if row[4] in dayindex and row[5] in dayindex:
                    startday = dayindex[row[4]]
                    endday = dayindex[row[5]]
                else:
                    #assume blanks indicate 'Anytime'
                    startday = 8
                    endday = 8
                starttime = row[6] + "M" #need to append this for Python to eval time
                endtime = row[7] + "M" #need to append this for Python to eval time
                try:

                    starttime_py = time.strptime(starttime, "%I%M%p")
                    endtime_py = time.strptime(endtime, "%I%M%p")
                    if starttime_py >= endtime_py:
                        if endtime != '1200AM':
                            #avoiding midnight-related issues on this error type.
                            if (endday - startday) != 0: #accounting for possiblities where we have overnight street sweeping. When the start day end day are identical, then should not be considered an error
                                errorsigns[row[1]] = 0
                                errorsupports[row[0]] = 0
                                print 'starttime is larger or equal to end time: ' + row[2]
                                icursor.insertRow([row[0],row[1],'starttime is larger or equal to end time on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])
                except:
                    errorsigns[row[1]] = 0
                    errorsupports[row[0]] = 0                                    
                    icursor.insertRow([row[0],row[1],'start or endtime in unxpected format on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])

    
        if row[2] in requiresday:
            #Check to make sure sign has start day
            if not row[4] or row[4] == '' or row[4] == ' ':
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                                
                print 'missing or incomplete start day on sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'missing or incomplete start day on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])
            #Check to make sure sign has end day
            if not row[5] or row[5] == '' or row[5] == ' ':
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                                
                print 'missing or incomplete end day on sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'missing or incomplete end day on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])
            #Check if startday is 'larger' than end day
            if row[4] in dayindex and row[5] in dayindex:
                #both start and end days are good for evaluation
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                                
                startday = dayindex[row[4]]
                endday = dayindex[row[5]]
                if startday > endday:                
                    print 'startday is larger than end day: ' + row[2]
                    icursor.insertRow([row[0],row[1],'startday is larger than end day on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])
                    
        if row[2] in requiresarrow:
            #Check to make sure sign has an arrow
            if not row[14] or row[14] == '' or row[14] == ' ':
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                                
                print 'missing arrow on sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'missing arrow on signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])

        if row[2] in optionalarrow:
            #Check to make sure sign has an arrow
            if not row[14] or row[14] == '' or row[14] == ' ':
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                                
                print 'No arrow on sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'missing arrow on signtype: ' + row[2], "WARNING", row[15][0],row[15][1] ,row[16]])
                
        if row[2] in requiresMPH:
            #Check to make sure speed limit signs have an integer value in the signtext field
            if not row[3]:
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                            
                print 'MPH value is Null on Speed Limit type sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'MPH value is Null on Speed Limit type signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])

            elif not row[3].isdigit():
                errorsigns[row[1]] = 0
                errorsupports[row[0]] = 0                                
                print 'Non-numeric value on Speed Limit type sign: ' + row[2]
                icursor.insertRow([row[0],row[1],'missing or invalid MPH value on Speed Limit type signtype: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])

            else:
                #we have a numeric value, check to see if it's divisible by 5
                if not int(row[3]) % 5 == 0:
                    errorsigns[row[1]] = 0
                    errorsupports[row[0]] = 0                
                    print 'Value on Speed Limit type sign is not divisible by 5: ' + row[2]
                    icursor.insertRow([row[0],row[1],'Value on Speed Limit type sign is not divisible by 5: ' + row[2], "ERROR", row[15][0],row[15][1] ,row[16]])

del icursor

print 'error checks complete for ' + checkfolder + '\n'


print checkfolder + " sign error percentage: {percent:.2%}".format(percent=float(len(errorsigns)) / float(len(signs)))

arcpy.TableToExcel_conversion(errortable, os.path.join(scriptloc,checkfolder + "_errorchecks.xls"))

