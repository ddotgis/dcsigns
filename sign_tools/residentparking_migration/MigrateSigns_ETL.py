#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    import arcpy
    import sys, os
    sys.path.append(r'C:\Users\jgraham\Documents\asset_checks')
    from AssetUtilities import Table2Dict as Table2Dict

    '''
    Script to migrate Signs, maintaining relation link to supports
    '''

    connection_origin = r'Database Connections\Connection to DDOTGIS as PPSA.sde'
    oldsigns_o = os.path.join(connection_origin,'PPSA.Signs_O')
    oldsigns_p = os.path.join(connection_origin,'PPSA.Signs_P')
    oldattach = os.path.join(connection_origin,'PPSA.Supports__ATTACH')

    connection_destination = r'Database Connections\Connection to DDOTGIS as TOA.sde'
    supports_destination = os.path.join(connection_destination,'TOA.Supports')
    newsigns_o = os.path.join(connection_destination,'TOA.Signs_O')
    newsigns_p = os.path.join(connection_destination,'TOA.Signs_P')
    newattach = os.path.join(connection_destination,'TOA.Supports__ATTACH')

    arcpy.AddField_management(newsigns_o,'ORIGIN_ID','TEXT')
    arcpy.AddField_management(newsigns_p,'ORIGIN_ID','TEXT')
    arcpy.AddField_management(newattach,'ORIGIN_ID','TEXT')

##    support_fieldnames = [f.name for f in arcpy.ListFields(fclass)]
##    sign_fieldnames = [f.name for f in arcpy.ListFields(fclass)]
##    restriction_fieldnames = [f.name for f in arcpy.ListFields(fclass)]
    support_field_names = ["GLOBALID", "COMMENTS", "SUPPORTTYPE", "SUPPORTSTATUS", "SUPPORTHEIGHT", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE", "SHAPE@", "ORIGIN_ID"]
    sign_field_names = ["GLOBALID", "MUTCD", "SIGNTEXT", "STATUS", "SIGNNUMBER", "SUPPORTID", "CREATED_USER", "CREATED_DATE", "LAST_EDITED_USER", "LAST_EDITED_DATE","SHAPE@", "SIGNARROWDIRECTION", "ORIGIN_ID"]
    attach_field_names = ['GLOBALID','CONTENT_TYPE','ATT_NAME','DATA_SIZE','DATA','REL_GLOBALID','ORIGIN_ID']
    newsupports = Table2Dict(supports_destination,"ORIGIN_ID",support_field_names[:-1])

    # Start an edit session. Must provide the worksapce.
    edit1 = arcpy.da.Editor(connection_destination)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit1.startEditing(False, False)

    # Start an edit operation
    edit1.startOperation()

    newsigns_o_fields = list(sign_field_names)
    newsigns_o_fields.remove('GLOBALID')
    newsigns_o_fields.remove('SIGNARROWDIRECTION')

    newattach_fields = list(attach_field_names)
    newattach_fields.remove('GLOBALID')

    newsigns_p_fields = list(sign_field_names)
    newsigns_p_fields.remove('GLOBALID')

    newsigns_o_icursor = arcpy.da.InsertCursor(newsigns_o, newsigns_o_fields)   #Exclude GlobalID and SignArrowDirection
    newsigns_p_icursor = arcpy.da.InsertCursor(newsigns_p, newsigns_p_fields)   #Exclude GlobalID
    newattach_icursor = arcpy.da.InsertCursor(newattach, newattach_fields)   #Exclude GlobalID

    with arcpy.da.SearchCursor(oldsigns_o,sign_field_names[:-2]) as cursor:
        for row in cursor:
            newGUID = newsupports[row[5]]['GLOBALID'] #the new GUID related to the original
            newrow1 = list(row[1:5])
            newrow1.append(newGUID) #new GUID
            newrow2 = list(row[6:])
            newrow2.append(row[0]) #Original ID
            newrowlist = newrow1 + newrow2

            #insert the new GUID as the support ID
            newrow = tuple(newrowlist)
            #for each sign fc, insert their records into the appropriate destination fc
            newsigns_o_icursor.insertRow(newrow)
            print "moved GUID other sign for... " + newGUID


    with arcpy.da.SearchCursor(oldsigns_p,sign_field_names[:-1]) as cursor:
        for row in cursor:
            newGUID = newsupports[row[5]]['GLOBALID'] #the new GUID related to the original
            newrow1 = list(row[1:5])
            newrow1.append(newGUID) #new GUID
            newrow2 = list(row[6:])
            newrow2.append(row[0]) #Original ID
            newrowlist = newrow1 + newrow2

            #insert the new GUID as the support ID
            newrow = tuple(newrowlist)

            #for each sign fc, insert their records into the appropriate destination fc
            newsigns_p_icursor.insertRow(newrow)
            print "moved GUID parking sign for... " + newGUID

    with arcpy.da.SearchCursor(oldattach,attach_field_names[:-1]) as cursor:
        for row in cursor:
            newGUID = newsupports[row[5]]['GLOBALID'] #the new GUID related to the original
            newrow1 = list(row[1:5])
            newrow1.append(newGUID) #new GUID for the support that this attachment should be related to
            newrow1.append(row[0]) #Original ID

            #insert the new GUID as the support ID
            newrow = tuple(newrow1)

            #for each sign fc, insert their records into the appropriate destination fc
            newattach_icursor.insertRow(newrow)
            print "moved GUID attachment for... " + newGUID

    # Stop the edit operation.
    edit1.stopOperation()

    # Stop the edit session and save the changes
    edit1.stopEditing(True)


if __name__ == '__main__':
    main()
