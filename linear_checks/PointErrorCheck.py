## Point Error including sign-level error and support-level error
import sys, os
import arcpy
from TableTools import Table2Dict
from SegmentUtilities import getHundredBlockString

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
checkzone = os.path.join(scriptloc, 'checkzones.gdb')
PointError = os.path.join(checkzone,
                          'PointError')

connection = r"Database Connections\Connection to DDOTGIS as TOA.sde"
supports = os.path.join(connection,'TOA.Supports')
signs_P =os.path.join(connection, 'TOA.Signs_P')

  
def BuildPointErrorFeature():
    
    if not arcpy.Exists(PointError):
        arcpy.CreateFeatureclass_management(checkzone,os.path.basename(PointError),'POINT','','','',arcpy.Describe(signs_P).spatialReference)
        arcpy.AddField_management(PointError, "ErrorCode", "TEXT", 25, "", "", "", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "Description", "TEXT", 255, "", "", "", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "SupportID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
        arcpy.AddField_management(PointError, "SignID", "TEXT", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "ROUTEID", "LONG", 10, "", "", "", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "MUTCD", "TEXT","", "", 25, "MUTCD Code", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "COUNT", "LONG", 2, "", "", "COUNT", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "ANGLE", "FLOAT", 8, "", "", "", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "OTHER_SEGID", "TEXT", 50, "", "", "", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "HBS", "TEXT", 50, "", "", "HUNDRED BLOCK ST.", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "X", "FLOAT", 12, "3", "", "", "NULLABLE", "NON_REQUIRED")
        arcpy.AddField_management(PointError, "Y", "FLOAT", 12, "3", "", "", "NULLABLE", "NON_REQUIRED")
    else:
        arcpy.TruncateTable_management(PointError)

    print 'Empty error table is ready to be populated'
    return PointError


def WriteToTable(dict, CheckTable, ErrorCode, Description, field):
    '''
    dict: the returned dictionary of errors from previous searchcursor
    CheckTable: the error table to be populated
    ErrorCode: the errorcode for the specific error
    Description: description for the error
    field: populated field names
    '''
    import arcpy
    
    cursor = arcpy.da.InsertCursor(CheckTable, field)
    for key, value in dict.iteritems():
        cursor.insertRow(tuple([ErrorCode, Description, key]+ [value[a] for a in field if a != 'ErrorCode' and a != 'Description' and a != 'SupportID']))
    del cursor

    
def RepetitiveSign(inputValue, index, dict):

    '''
    inputValue: the row from the sign table
    index: used to distinguish the first row from the rest
    dict: output dictionary
    '''
    
    global prevID, previousSign, previousSupport, field
    #field of the Point Error Check Table

    #{SupportID:{MUTCD: , COUNT: ,  SHAPE: (x,y) }}
    thisSign = inputValue[field.index('MUTCD')]
    thisSupport = inputValue[field.index('SupportID')]
    if index != 1:
        if thisSign == previousSign and thisSupport == previousSupport:
            if thisSupport in dict:
                dict[inputValue[field.index('SupportID')]]['COUNT'] += 1
                dict[inputValue[field.index('SupportID')]]['SignID'] += (',' + (inputValue[field.index('SignID')])) 
            else:
                dict[inputValue[field.index('SupportID')]] = {'SignID': ','.join([inputValue[field.index('SignID')], prevID]),
                                                              'MUTCD': inputValue[field.index('MUTCD')],
                                                              'ROUTEID': inputValue[field.index('ROUTEID')],
                                                              'COUNT': 2,
                                                              'X': inputValue[field.index('X')],
                                                              'Y':inputValue[field.index('Y')],
                                                              'SHAPE@XY': (inputValue[field.index('X')],inputValue[field.index('Y')])}
                
    prevID = inputValue[field.index('SignID')]
    previousSign = inputValue[field.index('MUTCD')]
    previousSupport = inputValue[field.index('SupportID')]
    return previousSign, previousSupport, prevID, dict


def MultipleSeg(inputValue, dict):

    global field
    #field of the Point Error Check Table
    
    #check for supports with potential errors (having more than one segmentIDs)
    if inputValue[field.index('OTHER_SEGID')] != '':
        dict[inputValue[field.index('SupportID')]] = {'ROUTEID': inputValue[field.index('ROUTEID')],
                               'OTHER_SEGID': inputValue[field.index('OTHER_SEGID')],
                               'ANGLE':inputValue[field.index('ANGLE')],
                               'X': inputValue[field.index('X')],
                               'Y': inputValue[field.index('Y')],
                               'SHAPE@XY': (inputValue[field.index('X')],inputValue[field.index('Y')])}

    return dict

def AttachDict():

    '''
    create a dictionary including support ID and the number of attachments associated with the support, based on production data
    '''
    
    connection = r"Database Connections\Connection to DDOTGIS as TOA.sde"
    attachments = os.path.join(connection, "TOA.Supports__ATTACH")
    attach_dict = {}
    with arcpy.da.SearchCursor(attachments, ['REL_GLOBALID']) as cursor:
        for row in cursor:
            if row[0] in attach_dict:
                 attach_dict[row[0]] += 1
            else:
                attach_dict[row[0]] = 1

    return attach_dict

def NoAttachmentSupport(inputValue, attach_dict, dict):

    '''
    inputValue: one row from the sign table
    attach_dict: dictionary with supportID and its attachments, supports with no attachment are not in the dictionary
    dict: output dictionary including supports with no attachemnt and their information
    '''
    
    if inputValue[field.index('SupportID')] not in attach_dict:
        dict[inputValue[field.index('SupportID')]] = {'X': inputValue[field.index('X')],
                                                      'Y': inputValue[field.index('Y')],
                                                      'ROUTEID': inputValue[field.index('ROUTEID')],
                                                      'SHAPE@XY': (inputValue[field.index('X')],inputValue[field.index('Y')])}
    return dict

gdb = os.path.join(scriptloc, "scratch.gdb")
LocalCopyofSigns = os.path.join(gdb,'Signs_P_local')

index = 0
CheckDict_Sign = {}
CheckDict_Support = {}

CheckDict_Attach = {}
attach_dict = AttachDict()

field = ['SupportID','SignID','MUTCD','SIGNTEXT','SignArrowDirection','X','Y','ROUTEID','MEASURE','SIDE','ANGLE','OTHER_SEGID']
with arcpy.da.SearchCursor(LocalCopyofSigns, field) as rows:
    for row in rows:
        index += 1
        RepetitiveSign(row, index, CheckDict_Sign)
        MultipleSeg(row, CheckDict_Support)
        NoAttachmentSupport(row, attach_dict, CheckDict_Attach)
        
Table = BuildPointErrorFeature()

#Populate checktable (repetitive sign on the same support)
field = ['ErrorCode','Description','SupportID','SignID','MUTCD','ROUTEID','COUNT','X','Y','SHAPE@XY']
WriteToTable(CheckDict_Sign, Table, 'RS', 'Repetitive signs on the same support', field)

#Populate checktable (multiple segment IDs)
field = ['ErrorCode','Description','SupportID','ROUTEID','ANGLE','OTHER_SEGID','X','Y','SHAPE@XY']
WriteToTable(CheckDict_Support, Table, 'MS', 'Multiple identified segments for one support', field)

#Populate checktable (missing attachments)
field = ['ErrorCode','Description','SupportID','ROUTEID','X','Y','SHAPE@XY']
WriteToTable(CheckDict_Attach, Table, 'MA', 'Missing picture attachments for signs', field)

#roadway segment ID lookup dictionary
inputFeature = os.path.join(r"Database Connections\Connection to DDOTGIS as SSDDBA.sde", "SSDDBA.COMP_ROADWAYSEG_VIEW")
field_names = ['REGISTEREDNAME','STREETTYPE','QUADRANT','FROMLEFTTHEORETICRANGE']
key_field = 'STREETSEGID'
addr={}
field_names.insert(0,key_field)
with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
    for row in cursor:
        if row[0]:
            if row[0] not in addr: 
                addr[row[0]]=dict(zip(field_names[1:],row[1:]))
            else:
                if not addr[row[0]]['FROMLEFTTHEORETICRANGE']:
                    addr[row[0]]=dict(zip(field_names[1:],row[1:]))


#populate the table with 100 blcok street info
with arcpy.da.UpdateCursor(PointError, ['ROUTEID','HBS','OBJECTID']) as cursor:
    for row in cursor:
        value = addr[row[0]]
        row[1] = getHundredBlockString(value['FROMLEFTTHEORETICRANGE'], value['REGISTEREDNAME'], value['STREETTYPE'], value['QUADRANT'])
        cursor.updateRow(row)
        if not value['FROMLEFTTHEORETICRANGE']:
            print 'check ' + str(row[2])





        













