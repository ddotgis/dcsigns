import requests
import os,sys
import arcpy
sys.path.append(r"C:\Users\jgraham\Documents\asset_checks")
from SegmentUtilities import GetOnSegPointInfo

scriptloc = sys.path[0]
gdb = os.path.join(scriptloc, "scratch.gdb")
connection = r"Database Connections\Connection to DDOTGIS as TOA.sde"
data =os.path.join(connection, "TOA.Sign_TrafficOther")

table = os.path.join(gdb, "signevents")#table with point sign events.

if not arcpy.Exists(table):
    arcpy.CreateTable_management(os.path.join(scriptloc,"scratch.gdb"),os.path.basename(table))
    arcpy.AddField_management(table, "SourceID", "LONG", 9, "", "", "SourceID", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(table, "StreetSegID", "LONG", 9, "", "", "Street Segment ID", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "MEAS", "FLOAT", 8, 3, "", "Measure Value", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "SIDE", "FLOAT", 8, 3, "", "Measure Value", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "OFFSET", "FLOAT", 8, 3, "", "Measure Value", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "TANGENT", "FLOAT", 8, 3, "", "Measure Value", "NULLABLE", "NON_REQUIRED") 

else:
    arcpy.TruncateTable_management(table)

fields = ['SourceID', 'StreetSegID', 'MEAS', 'SIDE', 'OFFSET', 'TANGENT']
icursor = arcpy.da.InsertCursor(table, fields)

with arcpy.da.SearchCursor(data,['OID@', 'SHAPE@X', 'SHAPE@Y']) as cursor:
    for row in cursor:

        closestseg = GetOnSegPointInfo(row[1], row[2])

        if closestseg:
            
            icursor.insertRow([row[0], closestseg[0], closestseg[1], closestseg[2], closestseg[3], closestseg[4]])
            print "Completed " + str(row[0])

        else:            
            print "Error finding segment for " + str(row[0])
            
            
del cursor, icursor



