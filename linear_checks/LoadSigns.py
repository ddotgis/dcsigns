import arcpy
import sys, os

from SegmentUtilities import getLRSRouteIDandMsFromSegID,routeAndUpdateSupportsBySeg,getFeatureOIDsNearSeg
from TableTools import Table2Dict
from SnappingTools import RouteSupportsToDict, SnapSigns

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]

blockID = int(arcpy.GetParameterAsText(0))
refresh = arcpy.GetParameterAsText(1) #user can request that supports be rerouted against the LRS.  This should be done if a user has recently added supports or manually associated to another street
units = 'meters'

gdb = os.path.join(scriptloc, "basetables.gdb")
parking_gdb = os.path.join(scriptloc, "parking_signs_reference.gdb")

if not arcpy.Exists(gdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(gdb))

connection = r"Database Connections\Connection to DDOTGIS as DDOTEDITOR.sde"
signs_P =os.path.join(connection, 'TOA.Signs_P')
supports =os.path.join(connection, 'TOA.Supports')

time_restrictions = os.path.join(connection, 'TOA.TimeRestrictions')
SignTableDetail = os.path.join(gdb,'SignTableDetail')
conflict_signs_reference = os.path.join(parking_gdb, 'conflict_signs_reference')

conflict_signs_ref = Table2Dict(conflict_signs_reference,
                        "MUTCD",
                        ["TYPE"])
 
if not arcpy.Exists(SignTableDetail):
    arcpy.CreateTable_management(gdb,os.path.basename(SignTableDetail))
    arcpy.AddField_management(SignTableDetail, "SupportID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(SignTableDetail, "SignID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(SignTableDetail, "RestrictionID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(SignTableDetail, "MUTCD", "TEXT","", "", 25, "MUTCD Code", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "SIGNTEXT", "TEXT","", "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "SignArrowDirection", "TEXT","", "", 2, "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "SegID", "LONG", 10, "", "", "", "NULLABLE", "NON_REQUIRED")  
    arcpy.AddField_management(SignTableDetail, "ROUTEID", "LONG", 10, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "MEASURE", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "SIDE", "TEXT", 20, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "ZoneID", "LONG", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "STARTDAY", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "ENDDAY", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "STARTTIME", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "ENDTIME", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "TIMEID", "TEXT", 25, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "EXCEPTION", "TEXT", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "X", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "Y", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "LAT", "FLOAT", '12', "8", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "LON", "FLOAT", '12', "8", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "SIGNNUMBER", "LONG", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(SignTableDetail, "OriginAngle", "FLOAT", '12', "8", "", "", "NULLABLE", "NON_REQUIRED")

else:
    arcpy.TruncateTable_management(SignTableDetail)


print 'Tables ready'

if units == 'feet':
    #keep in place until Markos changes SOE to return meters.
    fromM *= 3.2808
    toM *= 3.2808


if refresh:
    #update supports with new route, measure and side information.
    #get route and  measure vals from this segment
    #routeid, fromM, toM = getLRSRouteIDandMsFromSegID(blockID)
    OID_list, OID_str = getFeatureOIDsNearSeg(blockID, fc='TOA.Supports', distance=150)
    routeAndUpdateSupportsBySeg(blockID, OID_str, fc=os.path.basename(supports))

#get routeid and from/to for this specific segment; use that in the where_clause on the searchcursor for supports below
routeid, fromM, toM = getLRSRouteIDandMsFromSegID(blockID)
segSupports = dict()
with arcpy.da.SearchCursor(supports,["GLOBALID","ROUTEID", "MEASURE", "SIDE", "SHAPE@X","SHAPE@Y","ANGLE"], where_clause="SUPPORTSTATUS<>5 AND ROUTEID = '" + str(routeid) + "' AND ("  + str(fromM) + "<= MEASURE AND " + str(toM) + ">= MEASURE)") as cursor:
    for row in cursor:
        segSupports[row[0]] = {"ROUTEID":row[1],"MEASURE":row[2],"SIDE":row[3], "X":row[4], "Y":row[5], "ANGLE":row[6]}

supportIDs = segSupports.keys()
supports2query = ','.join(["'" + str(i) + "'" for i in supportIDs]) #capture all the IDs for the selected supports

#query signs on these supports and store in dict
segSigns= dict()
with arcpy.da.SearchCursor(signs_P,["SupportID","GlobalID",'MUTCD','SignArrowDirection','STATUS', 'SIGNTEXT', 'SIGNNUMBER'], where_clause="STATUS<>5 AND SupportID in (" + supports2query + ")") as cursor:
    for row in cursor:
        segSigns[row[1]] = {"SupportID":row[0],'MUTCD':row[2],'SignArrowDirection':row[3],'SIGNTEXT':row[5], 'SIGNNUMBER':row[6]}

signIDs = segSigns.keys()
signs2query = ','.join(["'" + str(i) + "'" for i in signIDs]) #capture all the IDs for the selected supports

#query restrictions on these signs and store in dict
segRestrictions= dict()
segRestrictionList = []
with arcpy.da.SearchCursor(time_restrictions,["SignID","GlobalID",'STARTDAY','ENDDAY','STARTTIME','ENDTIME','EXCEPTION'], where_clause="STATUS<>5 AND SignID in (" + signs2query + ")") as cursor:
    for row in cursor:
        segRestrictions[row[1]] = {"SignID":row[0],'STARTDAY':row[2],'ENDDAY':row[3],'STARTTIME':row[4],'ENDTIME':row[5],'EXCEPTION':row[6]}
        segRestrictionList.append(row[0]) #list of sign ids which have restrictions on this seg

##****** now take care of below.  We have supports, signs, restrictions all queried up above.  Carry forward changes below****####
#load all restrictions for signs
#restrictions_dict = Table2Dict(time_restrictions, 'GLOBALID',['SignID','STARTDAY','ENDDAY','STARTTIME','ENDTIME','EXCEPTION'])
SRbyWKID = arcpy.SpatialReference (3857)
with arcpy.da.SearchCursor(signs_P, ['SupportID','GLOBALID','MUTCD','SignArrowDirection','STATUS', 'SIGNTEXT', 'SIGNNUMBER'],
                                   where_clause="GLOBALID in (" + signs2query + ")",
                                   spatial_reference=SRbyWKID) as rows:
    
     with arcpy.da.InsertCursor(SignTableDetail, ['SignID','RestrictionID','STARTDAY','ENDDAY','STARTTIME','ENDTIME','TIMEID','EXCEPTION',
                                                  'ROUTEID','MEASURE','SIDE','MUTCD','SIGNTEXT','SignArrowDirection','SIGNNUMBER','X','Y', 'SupportID', 'OriginAngle']) as cursor:
        for sign in rows:
            if sign[2] in conflict_signs_ref:
                #only taking into consideration those signs which form parking zones
                if sign[0] in segSupports:
                    #working block-by-block; sign must be on support for this block
                    if sign[1] in segRestrictionList:
                        print "loading sign data with restrictions for support " + sign[0]
                        #we have a sign with one or more restrictions. load them all here:
                        for restriction_id, restriction_data in segRestrictions.items():
                            if restriction_data["SignID"] == sign[1]:
                                cursor.insertRow([sign[1],#SignID
                                            restriction_id,
                                            restriction_data['STARTDAY'],
                                            restriction_data['ENDDAY'],
                                            restriction_data['STARTTIME'],
                                            restriction_data['ENDTIME'],
                                            str(restriction_data['STARTDAY'])+'_'+str(restriction_data['ENDDAY'])+'_'+str(restriction_data['STARTTIME'])+'_'+str(restriction_data['ENDTIME']),
                                            restriction_data['EXCEPTION'],
                                            segSupports[sign[0]]['ROUTEID'],
                                            segSupports[sign[0]]['MEASURE'],
                                            segSupports[sign[0]]['SIDE'],
                                            sign[2],#MUTCD
                                            sign[5],#SIGNTEXT
                                            sign[3],#SIGNARROWDIRECTION
                                            sign[6],#SIGNNUMBER
                                            segSupports[sign[0]]['X'],#X
                                            segSupports[sign[0]]['Y'],#Y
                                            sign[0],#SupportID
                                            segSupports[sign[0]]['ANGLE']]) #support angle from/to centerline
                    else:
                        
                        #this sign does not have restrictions, so just plugging default 'anytime' values in
                        print "loading standard sign data for support " + sign[0]
                        cursor.insertRow([sign[1],#SignID                  
                                        "N/A",#no restriction id
                                        8,#default
                                        8,#default
                                        49,#default
                                        49,#default
                                        '8_8_49_49',#default
                                        "N/A",#default
                                        segSupports[sign[0]]['ROUTEID'],
                                        segSupports[sign[0]]['MEASURE'],
                                        segSupports[sign[0]]['SIDE'],
                                        sign[2],#MUTCD
                                        sign[5],#SIGNTEXT
                                        sign[3],#SIGNARROWDIRECTION
                                        sign[6],#SIGNNUMBER
                                        segSupports[sign[0]]['X'],#X
                                        segSupports[sign[0]]['Y'],#Y
                                        sign[0],#SupportID
                                        segSupports[sign[0]]['ANGLE']]) #support angle from/to centerline

                else:
                    #sign has been retired, so this restriction is essentially orphaned
                    #probably need to come up with a way to clean these orphaned ones from the db.
                    continue

print 'sign and restrictions... loading complete'
