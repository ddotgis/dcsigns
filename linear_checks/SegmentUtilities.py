import requests
import ast
import sys, os


def getFeatureOIDsNearSeg(segid, fc='TOA.Supports', distance=50):
    '''
    Function to spatially query for features within a certain distance of a street segment.
    user has option to provide distance
    '''
    import sys, os
    import arcpy

    fc_connection = r'Database Connections\Connection to DDOTGIS as DDOTEDITOR.sde'
    seg_connection = r'Database Connections\Connection to DDOTLRS as LRSViewer.sde'
    features = os.path.join(fc_connection,fc)
    streetsegs = os.path.join(seg_connection,'Ddotlrs.RH.LRSE_StreetSegment')

    if arcpy.Exists("streetsegs_lyr"):
        arcpy.Delete_management("streetsegs_lyr")  
    streetsegs_lyr = "streetsegs_lyr"
    
    if arcpy.Exists("selectthesefeatures_lyr"):
        arcpy.Delete_management("selectthesefeatures_lyr")        
    selectthesefeatures_lyr = "selectthesefeatures_lyr"

    arcpy.MakeFeatureLayer_management(features, selectthesefeatures_lyr)

    try:
        arcpy.MakeFeatureLayer_management(streetsegs, streetsegs_lyr, "STREETSEGID = " + str(segid)) #only selecting this block street seg ID
    except:
        raise Exception, "Error.  Cannot find this StreetSeg ID.  Check ID or source (currently set to {0})".format(segid)

    try:
        arcpy.SelectLayerByLocation_management(in_layer=selectthesefeatures_lyr,
                                               overlap_type="WITHIN_A_DISTANCE",
                                               select_features=streetsegs_lyr,
                                               search_distance=str(distance) + " FEET",
                                               selection_type="NEW_SELECTION")
        
        #need to find out what was selected...
        selectthesefeatures_lyr_sel_desc = arcpy.Describe(selectthesefeatures_lyr)
        FID_List = selectthesefeatures_lyr_sel_desc.FIDset.split(";")
        FID_Joined = ','.join(map(str, FID_List)) #capture all the IDs for the selected supports
        return FID_List, FID_Joined
    
    except:
        raise Exception, "Error.  Problem selecting supports along this segment ({0}.".format(segid)
    return OIDs

def routeAndUpdateSupportsBySeg(segid, OIDs, fc='TOA.Supports'):
    '''
    Use LRSTools to route features using a list of Object IDs.
    Based upon the returned values from the LRS, update the relevant features\
    OIDs:  A single string, containing a comma-delimited list of FID/OIDs
    '''
    import sys, os
    import arcpy

    whereclause = "OBJECTID in ( " + OIDs + ")"

    connection = r'Database Connections\Connection to DDOTGIS as DDOTEDITOR.sde'
    features = os.path.join(connection,fc)

    # Start an edit session. Must provide the worksapce.
    edit = arcpy.da.Editor(connection)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit.startEditing(False, False)

    # Start an edit operation
    edit.startOperation()
    
    with arcpy.da.UpdateCursor(features,['GLOBALID','SHAPE@X','SHAPE@Y','ROUTEID','MEASURE','SIDE','ROUTEID_ALT','MANUAL_SEGID',"OID@","ANGLE"], where_clause=whereclause) as cursor:
        for row in cursor:
            if row[7]:
                if int(row[7]) != segid:
                    #if the manual segment ID for this feature doesn't match the current segid, pass this support; it belongs to another segment.
                    continue
                else:
                    segdata = GetOnSegPointInfo2(row[1], row[2], searchradius = 50, manualsegID = int(row[7]))
            else:
                segdata = GetOnSegPointInfo2(row[1], row[2], searchradius = 50)
                    
                if segdata[0] != segid:
                    #if the closest segment doesn't match the current segid,
                    #pass this support; it belongs to another segment.
                    continue
                
                
            print segdata #segid, measure, side, offset, angle, routeid
            
            if 'error' in segdata:
                #document that we had an error locating this support
                row[3] = 'ERROR' #route id
                row[4] = 0.0 #measure
                row[5] = 0 #side
                row[9] = 0 #angle                
                print "Problem locating " + str(row[0])
                cursor.updateRow(row)

            else:
                #fill support row with route info
                row[3] = segdata[5] #route id
                row[4] = segdata[1] #measure
                if 'right' in segdata[2].lower(): #side  
                    row[5] = 1
                elif 'left' in segdata[2].lower():
                    row[5] = 3
                row[9] = float(segdata[4]) #angle                

                print "Completed " + str(row[0])
                cursor.updateRow(row)

    # Stop the edit operation.
    edit.stopOperation()

    # Stop the edit session and save the changes
    edit.stopEditing(True)

    print "Segment Update for ID " + str(segid) + "is complete."

def getLRSRouteIDandMsFromSegID(segid):
    '''
    Retrieve Roads and Highways LRS route id data through GetPointOnRoute Roads and Highways LRS rest service given an input DDOT Street Segment ID.
    '''

    payload = {"where": "STREETSEGID = " + str(segid),
               "outFields": "*",
               "f":'json'}

    r1 = requests.get("https://rh.dcgis.dc.gov/dcgis/rest/services/DDOT/LRSSupport/MapServer/3/query", params=payload)
    try:
        thispoint = r1.json()
    except:
        raise Exception,  "Error.  Cannot parse json.  Possibly invalid StreetSegID"

    try:
            
        routeid = thispoint['features'][0]['attributes']['ROUTEID']
        frommeas = thispoint['features'][0]['attributes']['FROMMEASURE']
        tomeas = thispoint['features'][0]['attributes']['TOMEASURE']

    except:
        raise Exception,  "Error.  Unexpected json format returned"

    return routeid, frommeas, tomeas

def GetOnSegPointInfo2(inX, inY, segid='', roadwaytype='Street', searchradius=50, manualsegID='', inSR=26985,outSR=26985):
    '''
    Retrieve LRS event data through GetPointOnRoute Roads and Highways LRS rest service given an input feature class X, Y, type, and radius.
    The closest point returned from the SOE (getPointOnRoute) will be the closest
    optionally, a user can provide a search radius (in feet) up to 50 meters.
    optionally, a user can provide a street segment id, if known.
    optionally, a user can provide a street type, to further constrain matches
    '''

    payload = {"x": inX,
               "y": inY,
               "inSR":inSR,
               "outSR":outSR,
               "routeType": roadwaytype,
               "searchRadius": searchradius,
               "f":'json'
               }

    r = requests.get("https://rh.dcgis.dc.gov/dcgis/rest/services/DDOT/LRSSupport/MapServer/exts/DDOTLRSTools/getPointOnRoute", params=payload)


    try:
        thispoint = r.json()
    except:
        raise Exception,  "Error.  Cannot parse json.  Check URL for issues"

    try:
        #closest = thispoint['pointOnRoutes'][next(index for (index, d) in enumerate(thispoint['PointOnSegments']) if d['StreetSegID']== str(manualsegID))]
        if manualsegID != '':
            #let's find the segment that matches what the user defined (manualsegID):
            for point in thispoint['pointOnRoutes']:
                if int(point['streetSegID']) == int(manualsegID):
                    #found the feature that matches the manual seg id.
                    segid = int(point['streetSegID'])
                    side = point['side']
                    angle = float(point['tangentOnRoute']['tangentHorizontalAngle'])
                    offset = float(point['offsetInMeters'])
                    measure = float(point['measureInMeters'])
                    routeid = float(point['routeID'])
                    return segid, measure, side, offset, angle, routeid
        else:
            #let's get the first (closest) feature
            segid = int(thispoint['pointOnRoutes'][0]['streetSegment']['streetSegID'])
            side = thispoint['pointOnRoutes'][0]['side']
            angle = float(thispoint['pointOnRoutes'][0]['tangentOnRoute']['tangentHorizontalAngle'])
            offset = float(thispoint['pointOnRoutes'][0]['offsetInMeters'])
            measure = float(thispoint['pointOnRoutes'][0]['measureInMeters'])
            routeid = int(thispoint['pointOnRoutes'][0]['routeID'])

        return segid, measure, side, offset, angle, routeid

    except:
        return "Error locating the support"
            
def GetOnSegPointInfo(inX, inY, segid='', roadwaytype='Street', searchradius=50, manualsegID=''):
    '''
    Retrieve LRS event data through the SSD GetOnSegPoint rest service given an input feature class.
    optionally, a user can provide a search radius (in feet) up to 50 meters.
    optionally, a user can provide a street segment id, if known.
    optionally, a user can provide a street type, to further constrain matches
    '''

    payload = {"X": inX,
               "Y": inY,
               "RoadType": roadwaytype,
               "SearchRadius": searchradius,
               "f":'json'
               }

    r = requests.get("http://maps2.dcgis.dc.gov/dcgis/rest/services/DDOT/SSD/MapServer/exts/PointOnSegment/GetPointOnSegment", params=payload)
    thispoint = r.json()
    if 'error' not in thispoint and len(thispoint['PointOnSegments']) > 0:
        if manualsegID == '':
            #{'PointOnSegments':[{SegID:...,Roadtype:...,},{},{}]}
            closest = min(thispoint['PointOnSegments'], key = lambda x: int(x['Offset']))
        else:
            closest = thispoint['PointOnSegments'][next(index for (index, d) in enumerate(thispoint['PointOnSegments']) if d['StreetSegID']== str(manualsegID))]
        segid = int(closest['StreetSegID'])
        side = float(closest['Side'])
        angle = float(closest['Tangent'])
        offset = float(closest['Offset'])
        measure = float(closest['Measure'])
        list = []
        for segment in thispoint['PointOnSegments']:
            if segment['StreetSegID'] != closest['StreetSegID']:
                list.append((segment['StreetSegID'],int(segment['Offset'])))
        other_segid = sorted(list, key=lambda x: x[1])
        return segid, measure, side, offset, angle, other_segid

    else:
        return False

def RouteMToDict():
    '''
    Retrieve LRS segment M values:  M_From and M_To.
    Returns a nested dictionary containing the M from and to values by ID:
    For example:  {1234: {M_From: 0, M_To: 144}, 2345: {M_From: 55, M_To: 98}, ...}
    '''

    import arcpy
    import sys, os

    connection = r'Database Connections\Connection to DDOTGIS as DDOTEDITOR.sde'
    streetsegments = os.path.join(connection,'SSDDBA.ReferenceFeatures', 'SSDDBA.StreetSegment')

    dict2return = dict()

    with arcpy.da.SearchCursor(streetsegments, ['StreetSegID', 'Shape@']) as cursor:
        for segment in cursor:
            startpt, endpt = segment[1].firstPoint, segment[1].lastPoint
            start = startpt.M
            end = endpt.M
            dict2return[segment[0]] = {'M_From':start, 'M_To':end}

    return dict2return


##def ReverseGeocode(x, y, streetSegID, dict, Key):
##
##    '''
##    RoadStreetID_dict: key is roadway ID and value is corresponding street segment ID
##    '''
##
##    payload = {"X": x,
##                "Y": y,
##                "f":'json'
##                }
##    
##    r = requests.get("http://citizenatlas.dc.gov/newwebservices/locationverifier.asmx/reverseGeocoding2", params=payload)
##    thispoint = r.json()
##
##    if 'error' not in thispoint and len(thispoint['Table1']) > 0:
##
##        list = [point for point in thispoint['Table1'] if point['FULLADDRESS'] is not None and int(point['ROADWAYSEGID']) in dict and dict[int(point['ROADWAYSEGID'])]== streetSegID]
##        if list != []:
##            closest = min(list,key = lambda x: float(x['DISTANCE']))
##            addressnum = int(closest['ADDRNUM'])
##            streettype = str(closest['STREET_TYPE'])
##            quadrant = str(closest['QUADRANT'])
##            streetname = str(closest['STNAME'])
##            print 'Okay!'
##            print '---------'
##        else:
##            print "No match is found for " + str(Key)
##            print set([int(point['ROADWAYSEGID']) for point in thispoint['Table1'] if point['FULLADDRESS'] is not None])
##            print set([dict[int(point['ROADWAYSEGID'])] for point in thispoint['Table1'] if point['FULLADDRESS'] is not None])
####            print set([dict[int(point['ROADWAYSEGID'])] for point in thispoint['Table1']])
##            print streetSegID
##            print '---------'
##            addressnum = ''
##            streettype = ''
##            quadrant = ''
##            streetname = ''
##
##        return addressnum, streetname, streettype, quadrant

def getHundredBlockString(addressnum, streetname, streettype, quadrant):

    if addressnum:
        BLOCKNUM = addressnum // 100 * 100 #drops whatever this number is to the lowest one hundr
        if BLOCKNUM == 0:
            BLOCKNUM = "UNIT"
        HBS = str(BLOCKNUM) + " Block of " + streetname + " " + streettype+ " " + quadrant
    else:
        HBS = "Unknown"
    
    return HBS
        


if __name__ == '__main__':
    #def GetOnSegPointInfo2(inX, inY, segid='', roadwaytype='Street', searchradius=50, manualsegID='', inSR=,outSR=):
    #testdict = GetOnSegPointInfo2(392885,139914,inSR=26985,outSR=26985)

    OID_list, OID_str = getFeatureOIDsNearSeg(8835, fc='TOA.Supports', distance=50)
    routeAndUpdateSupportsBySeg(8835, OID_str, fc='TOA.Supports')
    
##    items = GetOnSegPointInfo2(397747,136725,inSR=26985,outSR=26985,manualsegID=8835)
##    rid, measfrom, measto = getLRSRouteIDandMsFromSegID(10010)
##    print rid, measfrom, measto
##    print 'whoa'
