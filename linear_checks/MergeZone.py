import arcpy
import sys, os
import json

from SegmentUtilities import getLRSRouteIDandMsFromSegID
from ValidationUtilities import writeZoneError, getZoneErrorInfo
from TableTools import Table2Dict
from ALRSpy import getLRSGeometry, getLRSRouteID

##def allZoneTimesMatch(zonesigns):
##    timeset = []
##    for sign,val in zonesigns.items():
##        for time in val:
##            if time not in timeset:
##                timeset.append(time)
##
##    if len(timeset) => 2:
##        #more than one 
            
    
def writeNewZoneEvent(zone, SignType, SignText, RouteID, Side, begin, end, startday,endday,starttime, endtime, signs):
    global ParkingZones_dict, mutcddesc, offset
    if Side == 'right':
        offset -= 1
    else:
        offset += 1

    print str(offset)
    cursor = arcpy.da.InsertCursor(ParkingZones, ['ZoneID', 'MUTCD', 'SIGNTEXT', 'ROUTEID', 'SIDE', 'MEAS_FROM', 'MEAS_TO','STARTDAY','ENDDAY','STARTTIME','ENDTIME','SIGNS', 'OFFSET'])
    cursor.insertRow((zone, SignType,SignText, RouteID,Side,begin,end,startday, endday, starttime, endtime, signs, offset))
    thisGeometry = getLRSGeometry(RouteID,fromM=begin, toM=end)
    
    ParkingZone_dict[zone] = {"MUTCD":SignType,
                              "ZoneDescription":mutcddesc[SignType],
                                "SignIDs":[signs],
                                "StartTime":starttime,
                                "EndTime":endtime,
                                "StartDay":startday,
                                "EndDay":endday,
                                "SupportIDs":[thisSupportID],
                                "Polyline":thisGeometry}   

    del cursor
    print '@_@'


def resetPreviousValues():
    #reset all 'previous' event variables.
    global prevRouteID, prevSide, prevSignID, prevSignType, prevRouteEnd, prevTime, prevStartDay, prevEndDay, prevStartTime, prevEndTime, prevText
    #prevRouteID = ''
    #prevSide = ''
    previousSign = ''
    prevSignID = ''
    prevTime = ''
    prevStartDay = None
    prevEndDay = None
    prevStartTime = None
    prevEndTime = None
    prevRouteEnd = 0
    prevText = ''
    
    currentEvent.clear()
    currentEvent['ParticiSigns'] = []

def pastePreviousValues():
    #set all previous values to be this values
    global prevRouteID, thisRouteID, prevSide, thisSide, prevSignID, thisSignID, prevSignType, thisSignType, prevRouteEnd, thisMeasure, prevTime, thisTime
    global prevStartDay, prevStarTime, prevEndDay, prevEndTime, thisStartDay, thisEndDay, thisStartTime, thisEndTime, prevText, thisText, currentEvent
    
    prevRouteID = thisRouteID
    prevSide = thisSide
    prevSignID = thisSignID
    prevSignType = thisSignType
    prevTime = thisTime
    prevStartDay = thisStartDay
    prevEndDay = thisEndDay
    prevStartTime = thisStartTime
    prevEndTime = thisEndTime
    prevRouteEnd = thisRouteEnd
    prevText = thisText

def getLeadingGUIDVals(GUID):
    '''
    This inspects a Global ID and parses the string, returning
    only the first piece of the GUID as a string.
    '''
    return GUID.split('-')[0].lstrip('{')

def converttoFeet(measure):
     return round(measure *3.28084, 3)

def converttoMeters(measure):
     return round(measure *.3048, 3)

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
blockID = int(arcpy.GetParameterAsText(0))

connection = r'Database Connections\Connection to DDOTGIS as SSDDBA.sde'

gdb = os.path.join(scriptloc, "basetables.gdb")
error_gdb = os.path.join(scriptloc, "errorchecks.gdb")
zones_gdb = os.path.join(scriptloc, "zones.gdb")
parking_gdb = os.path.join(scriptloc, "parking_signs_reference.gdb")

SignTableDetail = os.path.join(gdb,'SignTableDetail')
ErrorCheckTable = os.path.join(error_gdb,'ErrorCheckTable')
conflict_signs_reference = os.path.join(parking_gdb, 'conflict_signs_reference')
ParkingZones = os.path.join(zones_gdb,'ParkingZones')

if not arcpy.Exists(zones_gdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(zones_gdb))
    
if not arcpy.Exists(ParkingZones):
    arcpy.CreateTable_management(zones_gdb,os.path.basename(ParkingZones))
    arcpy.AddField_management(ParkingZones, "ZoneID", "LONG", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "MUTCD", "TEXT","", "", 25, "SignType", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "SIGNTEXT", "TEXT","", "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "ROUTEID", "LONG", 10, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "SIDE", "TEXT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "MEAS_FROM", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "MEAS_TO", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "STARTDAY", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "ENDDAY", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "STARTTIME", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "ENDTIME", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "EXCEPTION", "TEXT","", "", 2, "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "SIGNS", "TEXT", "", "", "","PARTICIPATING SIGN", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZones, "OFFSET", "FLOAT", 3, 1, "","OFFSET", "NULLABLE", "NON_REQUIRED")
else:
    arcpy.TruncateTable_management(ParkingZones)

#Make Zones
#sign zone event data
print "creating conflict signs dict"
conflict_signs_ref = Table2Dict(conflict_signs_reference,
                        "MUTCD",
                        ["TYPE"])
all_signs_ref = Table2Dict(SignTableDetail,
                        "SignID",
                        ["TIMEID","MEASURE", "SupportID"])
ZoneBuildErrors = dict() #storing any issues as zones are being built
ParkingZone_dict = dict() #dict to store all successfully created parking zones

#this will give us a key id lookup for street segment ids, with M_From and M_To values.
streetsegment_ref = getLRSRouteIDandMsFromSegID(blockID)
#other lookups
arrowdirection =[d.codedValues for d in arcpy.da.ListDomains(connection) if d.name == 'SignArrowDirection'][0]
mutcddesc =[d.codedValues for d in arcpy.da.ListDomains(connection) if d.name == 'MUTCD'][0]
streetside = {'1':'right', '3':'left'}

prevRouteID = ''
prevSide = ''
prevSignType = '' #this will be the MUTCD code far the previous sign.
prevText = ''
prevSignID = ''
prevRouteEnd = 0
prevTime = ''
prevStartDay = None
prevEndDay = None
prevStartTime = None
prevEndDay = None
zoneID = 300000 #this will be an incremental number.
offset = 0

currentEvent = dict() #this will be the start measure of the current event and also keep track of participating signs
currentEvent['ParticiSigns'] = []
currentEvent['geoms'] = []
print "Evaluating sign detail table"


    
with arcpy.da.UpdateCursor(SignTableDetail,['SupportID', 'SignID','MUTCD','SignArrowDirection','ROUTEID','MEASURE','SIDE','STARTDAY','ENDDAY','STARTTIME','ENDTIME','TIMEID','X','Y', 'ZoneID','SIGNTEXT'],
                           sql_clause=(None, 'ORDER BY ROUTEID, SIDE, MUTCD, TIMEID, MEASURE ASC')) as rows:

    for row in rows:
        
        if row[3]:
            thisArrow = arrowdirection[int(row[3])].lower()
        else:
            #No arrow to evaluate
            continue
        if row[4] == 'No Support Found':
            #Support retired?
            continue

        #print row
        thisSupportID = row[0]
        thisRouteID = row[4]
        thisSide = streetside[row[6]]
        thisSignID = row[1]
        thisSignType = row[2]
        thisText = row[15]
        thisTime = row[11] #TimeID
        thisStartDay = row[7]
        thisEndDay = row[8]
        thisStartTime = row[9]
        thisEndTime = row[10]
        thisMeasure = row[5]
        thisRouteStart = streetsegment_ref[1]
        thisRouteEnd = streetsegment_ref[2]
        
        if thisRouteID == '10004':
            print 'whoa'
        if thisSide != prevSide or thisRouteID != prevRouteID or thisSignType != prevSignType or thisTime != prevTime:
            #END CAP CLOSE OUT ROUTINE
            #close out previous sign line event with the segment 'M_To' value, if it is still open (encap situations)
            if thisSide != prevSide:
                #reset offset
                offset = 0
            if len(currentEvent['ParticiSigns']) > 0:
                #there is an open event on the previous type.  Before continuing, close it to the segment end.
                writeNewZoneEvent(zoneID, prevSignType, prevText, prevRouteID, prevSide, currentEvent['START'], prevRouteEnd, prevStartDay, prevEndDay, prevStartTime, prevEndTime, ','.join(currentEvent['ParticiSigns']))
                row[14] = zoneID
                rows.updateRow(row)
           
                zoneID +=1
                #reset all 'previous' variables (everything start fresh, no need to compare to the previous sign).
                resetPreviousValues()

            #because we're working with a sorted cursor(if we get a new type of sign/start on the new side or route), we know it is the first on this side or it should be a starting event.
            #STARTING UP NEW SIGN TYPE ON THIS SIDE
            if thisSide == 'right':
                if thisArrow == 'right':
                    #end at begincap
                    writeNewZoneEvent(zoneID, thisSignType, thisText,thisRouteID, thisSide, thisRouteStart, thisMeasure, thisStartDay, thisEndDay, thisStartTime, thisEndTime, getLeadingGUIDVals(thisSignID))
                    row[14] = zoneID
                    rows.updateRow(row)
                    zoneID +=1
                    resetPreviousValues()

                elif thisArrow == 'bi-directional':
                    #begincap and continue
                    row[14] = zoneID
                    currentEvent['START'] = thisRouteStart
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    pastePreviousValues()

                elif thisArrow == 'left':
                    #begin at this measure
                    row[14] = zoneID
                    currentEvent['START'] = thisMeasure
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    pastePreviousValues()

            else: #thisSide == 'left'
                if thisArrow == 'left':
                    #end at begincap
                    writeNewZoneEvent(zoneID, thisSignType, thisText, thisRouteID, thisSide, thisRouteStart, thisMeasure, thisStartDay, thisEndDay, thisStartTime, thisEndTime, getLeadingGUIDVals(thisSignID))
                    row[14] = zoneID
                    rows.updateRow(row)
                    zoneID += 1
                    resetPreviousValues()

                elif thisArrow == 'bi-directional':
                    #begincap and continue
                    row[14] = zoneID
                    currentEvent['START'] = thisRouteStart
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    pastePreviousValues()

                elif thisArrow == 'right':
                    #begin at this measure
                    row[14] = zoneID
                    currentEvent['START'] = thisMeasure
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    #[-12:-1]
                    pastePreviousValues()

        else:
            #CONTINUING THE SAME SIGN TYPE ON THIS SIDE, same type, same time restriction
            if thisSide == 'right':
                if thisArrow == 'right':
                    #end event?
                    if len(currentEvent['ParticiSigns']) > 0:
                        #we have a preexisting event of this same type open already.
                        #Good, this is the way it should be.  Close event
                        currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))                                  
                        writeNewZoneEvent(zoneID, thisSignType, thisText, thisRouteID, thisSide, currentEvent['START'], thisMeasure, thisStartDay, thisEndDay, thisStartTime, thisEndTime, ','.join(currentEvent['ParticiSigns']))

                    else:
                        #there is not an event started, but this is a closing arrow on this side.
                        #no option other than to make it a begin cap and flagging as warning
                        #it overlaps the previous event (of the same type)
                        writeNewZoneEvent(zoneID, thisSignType,thisText, thisRouteID, thisSide, thisRouteStart, thisMeasure, thisStartDay, thisEndDay, thisStartTime, thisEndTime, thisSignID[-12:-1])
                        print 'Possible Zone Error: Unexpected closing arrow, not at block corner'
                        thisLRSRouteID = getLRSRouteID(thisRouteID)
                        thisGeometry = getLRSGeometry(thisLRSRouteID,fromM=thisRouteStart, toM=thisMeasure)
                        
                        ZoneBuildErrors[zoneID] = {"MUTCD":thisSignType,
                                                   "SignIDs":[getLeadingGUIDVals(thisSignID)],
                                                   "ErrorLevel":"Warning",
                                                   "ErrorTYpe":"ZoneBuildErrors",
                                                   "CheckName":"Unexpected closing arrow, not at block corner",
                                                   "SupportIDs":[thisSupportID],
                                                   "Polyline":thisGeometry}                                                            
                        
                    row[14] = zoneID
                    rows.updateRow(row)
                    resetPreviousValues() #everything is closed off, no need to compare to the previous sign
                    zoneID +=1

                elif thisArrow == 'bi-directional':
                    #log id and continue
                    row[14] = zoneID
                    currentEvent['MIDDLE'] = thisMeasure
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    pastePreviousValues()
                    
                elif thisArrow == 'left':
                    #begin at this measure
                    row[14] = zoneID
                    currentEvent['MIDDLE'] = thisMeasure
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    pastePreviousValues()


            elif thisSide == 'left':
                if thisArrow == 'left':
                    #end event?
                    if len(currentEvent['ParticiSigns']) > 0:
                        #we have a preexisting event of this same type open already.
                        #Good, this is the way it should be.  Close event
                        currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                        writeNewZoneEvent(zoneID, thisSignType,thisText,thisRouteID, thisSide, currentEvent['START'], thisMeasure, thisStartDay, thisEndDay, thisStartTime, thisEndTime, ','.join(currentEvent['ParticiSigns']))

                    else:
                        #there is not an event started, but this is a closing arrow on this side.
                        #no option other than to make it a begin cap and flagging as warning
                        #it overlaps the previous event (of the same type)
                        
                        writeNewZoneEvent(zoneID, thisSignType, thisText,thisRouteID, thisSide, thisRouteStart, thisMeasure, thisStartDay, thisEndDay, thisStartTime, thisEndTime, getLeadingGUIDVals(thisSignID))
                        print 'Possible Zone Error: Unexpected closing arrow, not at block corner'
                        thisLRSRouteID = getLRSRouteID(thisRouteID)
                        thisGeometry = getLRSGeometry(thisLRSRouteID,fromM=thisRouteStart, toM=thisMeasure)
                        
                        ZoneBuildErrors[zoneID] = {"MUTCD":thisSignType,
                                                   "SignIDs":[getLeadingGUIDVals(thisSignID)],
                                                   "ErrorLevel":"Warning",
                                                   "ErrorTYpe":"ZoneBuildErrors",
                                                   "CheckName":"Unexpected closing arrow, not at block corner",
                                                   "SupportIDs":[thisSupportID],
                                                   "Polyline":thisGeometry}
                        
                    row[14] = zoneID
                    rows.updateRow(row)
                    resetPreviousValues()
                    zoneID +=1

                elif thisArrow == 'bi-directional':
                    #log id and continue
                    row[14] = zoneID
                    currentEvent['MIDDLE'] = thisMeasure
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    pastePreviousValues()
                    
                elif thisArrow == 'right':
                    #begin at this measure
                    row[14] = zoneID
                    currentEvent['MIDDLE'] = thisMeasure
                    currentEvent['ParticiSigns'].append(getLeadingGUIDVals(thisSignID))
                    pastePreviousValues()
                    
        rows.updateRow(row)

signerrors = Table2Dict(ErrorCheckTable, "ErrorID", ["SupportIDs","SignIDs","MUTCD","X","Y","CheckName","ErrorLevel","ErrorType"])
signerrors.update(ZoneBuildErrors)

with open(os.path.join(scriptloc, 'result.json'), 'w') as fp:
    json.dump(signerrors, fp)
    
print 'YAY'