import requests
import os,sys
import arcpy
sys.path.append(r"C:\Users\jgraham\Documents\asset_checks")
from SegmentUtilities_updated import GetOnSegPointInfo

scriptloc = sys.path[0]
arcpy.env.overwriteOutput = True
gdb = os.path.join(scriptloc, "scratch.gdb")
connection = r"Database Connections\Connection to DDOTGIS as TOA.sde"
data =os.path.join(connection, "TOA.Sign_TrafficOther")

table = os.path.join(gdb, "signevents")#table with point sign events.

# Assign a number value to the null ID value(starting from maximum+1)
def AssignNewIDfromNum(input_FC, db_connection, IDname):
    with arcpy.da.SearchCursor(input_FC,[IDname],sql_clause=(None, 'ORDER BY '+IDname)) as cursor:
        valueRange =[]
        for row in cursor:
            if row[0]is not None:
                valueRange.append(row[0])
        maxV = max(valueRange)

    edit = arcpy.da.Editor(db_connection)
    edit.startEditing(False, False)
    with arcpy.da.UpdateCursor(input_FC,[IDname],sql_clause=(None, 'ORDER BY '+IDname)) as cursor:
        for row in cursor:
            if row[0]is None or row[0] == 0 or row[0] == '':
                maxV+=1
                row[0]=maxV
                cursor.updateRow(row)

    edit.stopOperation()
    edit.stopEditing(True)

def nuller(input):
    if input:
        return input
    else:
        return 0


if not arcpy.Exists(table):
    arcpy.CreateTable_management(os.path.join(scriptloc,"scratch.gdb"),os.path.basename(table))
    arcpy.AddField_management(table, "SignID", "LONG", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(table, "PoleID", "LONG", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(table, "StreetSegID", "LONG", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "MEAS", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "SIDE", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "OFFSET", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "TANGENT", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(table, "Other_SegID", "TEXT", "", "", "", "", "NULLABLE", "NON_REQUIRED")

else:
    arcpy.TruncateTable_management(table)

#signtype = [d.codedValues for d in arcpy.da.ListDomains(gdb) if d.name == 'TrafficSignType'][0]
#arrowdirtype = [d.codedValues for d in arcpy.da.ListDomains(gdb) if d.name == 'SignArrowDirection'][0]

fields = ['SignID', 'PoleID', 'StreetSegID', 'MEAS', 'SIDE', 'OFFSET', 'TANGENT', 'Other_SegID']
icursor = arcpy.da.InsertCursor(table, fields)

with arcpy.da.SearchCursor(data,['OID@', 'SHAPE@X', 'SHAPE@Y']) as cursor:
    for row in cursor:

        closestseg = GetOnSegPointInfo(row[1], row[2])

        if closestseg:
            
            icursor.insertRow([row[0], 0, closestseg[0], closestseg[1], closestseg[2], closestseg[3], closestseg[4], closestseg[5]])
            print "Completed " + str(row[0])

        else:            
            print "Error finding segment for " + str(row[0])
            
            
del cursor, icursor



