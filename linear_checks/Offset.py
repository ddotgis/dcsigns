import arcpy
import sys, os
from TableTools import Table2Dict

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
gdb = os.path.join(scriptloc, "scratch.gdb")
ref_gdb = os.path.join(scriptloc, "parking_signs_reference.gdb")
ParkingZone_copy = os.path.join(gdb, 'ParkingZoneCopy')
conflict_signs = os.path.join(ref_gdb, 'conflict_signs_reference')

conflict_dict = Table2Dict(conflict_signs, 'MUTCD',['TYPE'])

#add offset value
with arcpy.da.UpdateCursor(ParkingZone_copy,['SIDE', 'MUTCD','OFFSET']) as cursor:
    for row in cursor:
        if row[0] == "right":
            if row[1] in conflict_dict:
                if conflict_dict[row[1]]['TYPE'] == 'permissive':
                    row[2] = -15
                elif conflict_dict[row[1]]['TYPE'] == 'restrictive':
                    row[2] = -10
            else:
                row[2] = -5
        else:
            if row[1] in conflict_dict:
                if conflict_dict[row[1]]['TYPE'] == 'permissive':
                    row[2] = 15
                elif conflict_dict[row[1]]['TYPE'] == 'restrictive':
                    row[2] = 10
            else:
                row[2] = 5
        cursor.updateRow(row)

