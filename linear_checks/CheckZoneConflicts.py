import arcpy
import sys, os
import csv
sys.path.append(r"C:\Users\jgraham\Documents\asset_checks")
from AssetUtilities import Table2Dict
from operator import itemgetter
from pprint import pprint

def getArrowTypeList(connection):
    arrowdirtype = [d.codedValues for d in arcpy.da.ListDomains(connection) if d.name == 'SignArrowDirection'][0]
    return arrowdirtype

def checkTimeDayBand():
    #this function examines each sign within the current zone for matching day,time.
    #if matched, nothing happens
    #if a mismatch is found, a validation error is logged on the current zone event.
    #any zone found to have internal time/day mismatches, it should not be part of block analysis
    pass


scriptloc = sys.path[0]

##Assumes a point events table has recently been created and is now available at (source directory)/scratch.gdb/signevents.
##Also assumes a linear events table has been recently created and available (linearsignevents)
##local work gdb...
gdb = os.path.join(scriptloc, "scratch.gdb")
signeventtable = os.path.join(gdb,"signevents")
#sign zone event data
linearsigneventtable =os.path.join(gdb,"linearsignevents")
linearsigneventtable_errors =os.path.join(gdb,"linearsignevents_errors")

errorfieldnames = ["RouteID",
              "MUTCD",
              "ParticipatingSigns",
              "Side",
              "begin",
              "end",
              "ErrorType"]

#production sign data..
connection_signs = r"Database Connections\Connection to DDOTGIS as TOA.sde"
data_signs =os.path.join(connection_signs, "TOA.Sign_TrafficOther")
data_signs_keyfield ="OBJECTID"
#these fields should come in ordered in sets of 4, with each set having a start/end day in positions 0,1 then start/end time in positions 2,3.

zone_sign_key = {"ParticipatingSigns":"",
                  "daystart1":"ServiceDayStart",
                  "dayend1":"ServiceDayEnd",
                  "hourstart1":"ServiceHourStart",
                  "hourend1":"ServiceHourEnd",
                  "daystart2":"ServiceDayStart_ALT",
                  "dayend2":"ServiceDayEnd_ALT",
                  "hourstart2":"ServiceHourStart_ALT" ,
                  "hourend2":"ServiceHourEnd_ALT",
                  "daystart3":"",
                  "dayend3":"",
                  "hourstart3":"",
                  "hourend3":"",
                  "daystart4":"",
                  "dayend4":"",
                  "hourstart4":"",
                  "hourend4":"",
                  "InZoneValidation":"InZoneValidation"}

sign_zone_key = {y:x for x,y in zone_sign_key.iteritems() if y}

data_signs_fields = [x for x in zone_sign_key.values() if x and x != 'InZoneValidation']
zone_fields =sorted(zone_sign_key.keys())

serviceday =[d.codedValues for d in arcpy.da.ListDomains(connection_signs) if d.name == 'LZ_ServiceDay'][0]
servicehour =[d.codedValues for d in arcpy.da.ListDomains(connection_signs) if d.name == 'LZ_ServiceHours'][0]

signs_dict = Table2Dict(data_signs,
                        data_signs_keyfield,
                        data_signs_fields)

icursor = arcpy.da.UpdateCursor(linearsigneventtable_errors, zone_fields)

with arcpy.da.UpdateCursor(linearsigneventtable, zone_fields) as cursor:
    for zone in cursor:
        #comparison dictionary
        zonesigns_compare = dict()

        #identify signs involved in this zone by sign ids.
        zonesigns = zone[1].split(',')
        if len(zonesigns) <= 1:
            #this is a one-sign zone.
            continue

        #collect time/day info for each sign; add to time/day dict
        for sign in zonesigns:
            thissign = int(sign)
            if thissign in signs_dict:
                #sign is found in signs dictionary, initialize our comparision dictionary.
                zonesigns_compare[thissign] = {}

                for sign_field in data_signs_fields[1:]:
                    zonesigns_compare[thissign][sign_field] = signs_dict[thissign][sign_field]
            else:
                print "Error finding sign"

        print "checking comparison zone dict"
        zone_validation_dict = {"InZoneValidation":[]}
        has_validation_errors = False
        for k,v in zonesigns_compare.items():

            for sign_field in data_signs_fields[1:]:
                if sign_field not in zone_validation_dict:
                    zone_validation_dict[sign_field] = v[sign_field]
                else:
                    #field already stored, compare previous to this value
                    if zone_validation_dict[sign_field] == v[sign_field]:
                        #this is a matching value, we can move on.
                        continue
                    else:
                        #conflicting values!  Flag and send to validation field
                        if 'hour' in sign_field.lower():
                            #time
                            if zone_validation_dict[sign_field] in servicehour:
                                previousvalue = servicehour[zone_validation_dict[sign_field]]
                            else:
                                if zone_validation_dict[sign_field] is None:
                                    previousvalue = "Empty"
                                else:
                                    previousvalue = zone_validation_dict[sign_field]

                            if v[sign_field] in servicehour:
                                thisvalue = servicehour[v[sign_field]]
                            else:
                                if v[sign_field] is None:
                                    thisvalue = "Empty"
                                else:
                                    thisvalue = str(v[sign_field])

                        else:
                            #day
                            if zone_validation_dict[sign_field] in serviceday:
                                previousvalue = serviceday[zone_validation_dict[sign_field]]
                            else:
                                if zone_validation_dict[sign_field] is None:
                                    previousvalue ="Empty"
                                else:
                                    previousvalue = zone_validation_dict[sign_field]

                            if v[sign_field] in serviceday:
                                thisvalue = serviceday[v[sign_field]]
                            else:
                                if v[sign_field] is None:
                                    thisvalue = "Empty"
                                else:
                                    thisvalue = str(v[sign_field])

                        #store validation error
                        if not sign_field in zone_validation_dict["InZoneValidation"]:
                            validation_entry = sign_field + " mismatch: %s<>%s)" % (previousvalue,thisvalue)
                            zone_validation_dict["InZoneValidation"].append(validation_entry)
                            has_validation_errors = True

            print "check before proceeding"

        if '27058' in zonesigns:
            print 'whoa'

        if has_validation_errors:
            #Houston, we have a problem with this zone.  Only write the validation errors here.
            #update record with validation error info if any mismatch is found.
            for k,v in zone_validation_dict.items():
                if k == 'InZoneValidation':
                    info = ",".join(v)
                    info_trim = (info[:250] + '...') if len(info) > 250 else info
                    zone[zone_fields.index(sign_zone_key[k])] = info_trim

        else:
            #found no validation errors.  Continue to write to row.
            #update record with time/day info (if all time/day bands are identical)
            for k,v in zone_validation_dict.items():
                if k != 'InZoneValidation':
                    zone[zone_fields.index(sign_zone_key[k])] = v

        cursor.updateRow(zone)
