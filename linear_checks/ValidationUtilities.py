#-------------------------------------------------------------------------------
# Name:        Validation Utilities
# Purpose:
#
# Author:      jgraham
#
# Created:     20/07/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import sys, os
import arcpy

def writeZoneError(errorcode, zone, SignType, RouteID, Side, begin,end):

    '''
    Function that writes a new error/warning to our zone checks table
    'ENDARROWATBEGIN',zoneID, thisSignType,thisRouteID, thisSide, 0, thisMeasure
    '''
    scriptloc = sys.path[0]
    gdb = os.path.join(scriptloc, "checkzones.gdb")

    #error table
    checkzones = os.path.join(gdb,'CheckTable_ErrorZone')

    errordescription = getZoneErrorInfo(errorcode)

    cursor = arcpy.da.InsertCursor(checkzones, ['ERROR_TYPE', 'ZoneID', 'MUTCD', 'ROUTEID', 'SIDE', 'MEAS_FROM', 'MEAS_TO','ERROR_DESC'])
    cursor.insertRow([errorcode,zone,SignType,RouteID,Side,begin,end,errordescription])
    del cursor


def getZoneErrorInfo(errorcode="error"):
    '''
    Function that returns appropriate error description for all known
    zone error types
    '''
    errortypes = {"ENDARROWATBEGIN":"This zone is a single-signed zone that overlaps another zone of the same type",
                    "TIMEBANDLOW":"this time band does not appear in all signs in the zone",
                    "TIMEBANDHIGH":"this time band appears more times than expected"}

    if errorcode in errortypes:
        return errortypes[errorcode]
    else:
        return 'unknown error type'


def checkZoneDayTimeIntersect(starta, enda, startb, endb):
    '''
    If a intersects b (time or day), then this function returns a list of what number ranges intersect
    If nothing intersects, an empty list is returned.
    '''
    x = range(starta, enda)
    y = range(startb, endb)
    return list(set(x) & set(y))