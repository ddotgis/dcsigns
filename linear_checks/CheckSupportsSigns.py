import arcpy
import sys, os
import time

def Table2Dict(inputFeature,key_field,field_names,filter=None):
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names,where_clause=filter) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
connection = r"Database Connections\Connection to DDOTGIS as DDOTEDITOR.sde"
gdb = os.path.join(scriptloc, "basetables.gdb")
error_gdb = os.path.join(scriptloc, "errorchecks.gdb")

supports =os.path.join(connection, 'TOA.Supports')
signs_P =os.path.join(connection, 'TOA.Signs_P')
time_restrictions =os.path.join(connection, 'TOA.TimeRestrictions')
ErrorCheckTable = os.path.join(error_gdb,'ErrorCheckTable')

if not arcpy.Exists(error_gdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(error_gdb))

if not arcpy.Exists(ErrorCheckTable):
    arcpy.CreateTable_management(error_gdb,os.path.basename(ErrorCheckTable))
    arcpy.AddField_management(ErrorCheckTable, "SupportIDs", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "SignIDs", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "RestrictionID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "MUTCD", "TEXT","", "", 25, "MUTCD Code", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "X", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "Y", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "MEAS", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "MEAS_TO", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "LAT", "FLOAT", '12', "8", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "LON", "FLOAT", '12', "8", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "CheckName", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ErrorLevel", "TEXT", 20, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ErrorType", "TEXT", 20, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ErrorID", "LONG", 8, "", "", "", "NULLABLE", "NON_REQUIRED")
    
else:
    arcpy.TruncateTable_management(ErrorCheckTable)


#insert errors

requirestime = ['R-NS-067','R-NS-006','R-NS-012','R-NS-013','R-NS-022','R-NS-026','R-NS-052','R-NS-053','R-NS-064','R-NS-075','R-NS-122','R-NS-174','R-NS-180',
                   'R-NS-185','R-NS-210','R-NS-213','R-NS-214','R-NS-LZSHARED','R-NS-OLD','R-NS_ROP','R-NS-RPP','R-DC-2HROLD','R-DC-2HROLD','R-DC-2HR','R-DC-NSNP',
                   'R-DC-NSNP_EXCEPTION','R-DC-NO_PARK_ENTRANCE_TIMES','R-DC-No_Parking_Generic_w_Time','R-DC-Diplomat_Eq_Guinea','R-DC-Embassy _Angola',
                   'R-DC-15_Min_Parking1','R-DC-One_Hour_Parking_Eastern_Market','R-DC-School_Parking_Zone_15_Min','R-DC-School_Loading_Zones2','R-DC-School_Loading_Zones3',
                   'R-DC-ROP_Exception_Plaque','R-DC-RPP_Exception_Plaque','R-DC-15_Min_Parking2','R-NS-002','R-NS-153','R-NS-020','W-NS-027','R-NS-032','R-NS-033',
                   'R-NS-171','R-NS-073','R-NS-077','R-DC-One_Way_Yellow','R-DC-Reserved_DCGov','R-NS-047','R-NS-030','R-NS-173','D-NS-007','R-DC-Commercial_Vehicles_Loading',
                   'R-NS-LZPTL']

requiresday = ['R-NS-006','R-NS-012','R-NS-013','R-NS-022','R-NS-026','R-NS-052','R-NS-053','R-NS-064','R-NS-075','R-NS-122','R-NS-174','R-NS-180',
                   'R-NS-185','R-NS-210','R-NS-213','R-NS-214','R-NS-LZSHARED','R-NS-OLD','R-NS_ROP','R-NS-RPP','R-DC-2HROLD','R-DC-2HROLD','R-DC-2HR','R-DC-NSNP',
                   'R-DC-NSNP_EXCEPTION','R-DC-NO_PARK_ENTRANCE_TIMES','R-DC-No_Parking_Generic_w_Time','R-DC-Diplomat_Eq_Guinea','R-DC-Embassy _Angola',
                   'R-DC-15_Min_Parking1','R-DC-School_Parking_Zone_15_Min','R-DC-School_Loading_Zones2','R-DC-School_Loading_Zones3',
                   'R-DC-ROP_Exception_Plaque','R-DC-RPP_Exception_Plaque','R-DC-15_Min_Parking2','R-NS-002','R-NS-153','R-NS-020','W-NS-027','R-NS-032','R-NS-033',
                   'R-NS-171','R-NS-073','R-NS-077','R-DC-One_Way_Yellow','R-DC-Reserved_DCGov','R-NS-047','R-NS-030','R-NS-173','D-NS-007','R-DC-Commercial_Vehicles_Loading',
                   'R-NS-LZPTL']

requiresMPH = ['R-NS-173','R-NS-071','W-NS-008','R-NS-037','R-NS-110','R-NS-103']

requiresarrow = ['R-NS-006','R-NS-011','R-NS-012','R-NS-013','R-NS-015','R-NS-017','R-NS-019','R-NS-022','R-NS-026','R-NS-038','R-NS-052','R-NS-053','R-NS-059',
                 'R-NS-064','R-NS-075','R-NS-080','R-NS-121','R-NS-122','R-NS-131','R-NS-133','R-NS-141','R-NS-148','R-NS-172','R-NS-174','R-NS-180',
                 'R-NS-185','R-NS-210','R-NS-215','R-NS-213','R-NS-214','R-NS-056','R-NS-LZSHARED','R-NS-OLD','R-NS_ROP','R-NS-RPP','R-DC-2HROLD','R-DC-2HROLD',
                 'R-DC-2HR','R-DC-NSNP','R-DC-NSNP_EXCEPTION','R-DC-PTPCOIN','R-DC-Taxi_2','R-DC-NO_PARK_ENTRANCE_TIMES','R-DC-Diplomat','R-DC-No_Park_Russia_Embassy',
                 'R-DC-No_Parking_Generic_w_Time','R-DC-Diplomat_Kazakhstan','R-DC-No_Stand_Bus','R-DC-Diplomat_Eq_Guinea','R-DC-Embassy _Angola','R-DC-Embassy _Diplomat_Mexico',
                 'R-DC-15_Min_Parking1','R-DC-No_Park_Stadium_Event','R-DC-One_Hour_Parking_Eastern_Market','R-DC-No_Parking_Alley','R-DC-School_Parking_Zone_15_Min',
                 'R-DC-School_Loading_Zones2','R-DC-School_Loading_Zones3','R-DC-ROP_Exception_Plaque','R-DC-RPP_Exception_Plaque','R-DC-15_Min_Parking2','R-DC-Back_in_Parking',
                 'R-NS-157','R-NS-120','R-DC-One_Way_Yellow','R-DC-Reserved_DCGov','D-NS-011','D-NS-038','D-NS-039','D-NS-055','R-NS-046','R-NS-134','R-DC-Hotel_Load','R-NS-LZPTL',
                 'R-NS-LZSPECIAL','O-NS-024']

requireszone = ['R-NS_ROP','R-NS-RPP','R-DC-PMPBP','R-DC-PBC_PLAQUE','R-DC-ROP_Exception_Plaque','R-DC-RPP_Exception_Plaque','R-NS-LZPTL']

requireshourlimit = ['R-NS-RPP','R-DC-2HROLD','R-DC-2HR','R-DC-PTPCOIN','R-NS-LZPTL']

supportlocerrors = []

#load all supports
supports_dict = Table2Dict(supports, 'GlobalID',['SUPPORTSTATUS'])
signswithrestrictions_dict = dict()
with arcpy.da.SearchCursor(time_restrictions, ['GLOBALID', 'SIGNID', 'STARTDAY', 'ENDDAY','STARTTIME', 'ENDTIME', 'HOURLIMIT']) as cursor:
    for row in cursor:
        if row[1] not in signswithrestrictions_dict:
            signswithrestrictions_dict[row[1]] = [(row[2],row[3],row[4],row[5], row[6], row[0])]
        else:
            signswithrestrictions_dict[row[1]].append((row[2],row[3],row[4],row[5], row[6], row[0]))

with arcpy.da.InsertCursor(ErrorCheckTable, ["SupportIDs","SignIDs","CheckName", "ErrorLevel", "ErrorType", "RestrictionID", "X", "Y", "MUTCD"]) as icursor:
                               
    with arcpy.da.SearchCursor(signs_P,['SupportID','GlobalID','MUTCD', 'SIGNTEXT','SIGNARROWDIRECTION', 'SHAPE@XY']) as cursor:
        for row in cursor:
            thisSupportID = row[0]
            thisSignID = row[1]
            thisSignType = row[2]

            #SUPPORT CHECKS
            if thisSupportID not in supports_dict and thisSupportID not in supportlocerrors:
                #SupportID in SignFC should be present in SupportFC
                supportlocerrors.append(thisSupportID)
                print 'unmatched support on: ' + thisSupportID
                icursor.insertRow([thisSupportID,thisSignID,"SupportID is not an active support", "Error", "SignToSupportErrors", "", row[5][0], row[5[1], thisSignType]])
                
            if (not thisSupportID or thisSupportID == '' or thisSupportID == ' ') and thisSupportID not in supportlocerrors:
                #Null/blank SupportID.  Not good
                supportlocerrors.append(thisSupportID)
                print 'null support ID on: ' + thisSupportID
                icursor.insertRow([thisSupportID,thisSignID,"SupportID is null or blank", "Error", "SignToSupportErrors", "", row[5][0], row[5][1], thisSignType])

            #ARROW CHECKS    
            if thisSignType in requiresarrow:
                #Check to make sure sign has an arrow
                if not row[4] or row[4] == '' or row[4] == ' ':
                    print 'missing arrow on sign: ' + thisSignID
                    icursor.insertRow([thisSupportID,thisSignID,'missing arrow on sign', "Error", "SignErrors", "", row[5][0], row[5][1], thisSignType])

            #HOUR LIMIT CHECKS
            if thisSignType in requireshourlimit:
                #Check to make sure sign has an arrow
                if not row[3]:        
                    print 'Hour limit value is Null on sign that requires an hour limit: ' + thisSignID
                    icursor.insertRow([thisSupportID,thisSignID,'Hour limit required for this sight type', "Error", "SignErrors", "", row[5][0], row[5][1], thisSignType])

                elif not row[3].strip().isdigit():        
                    print 'Hour limit value is Null on sign that requires an hour limit' + thisSignID
                    icursor.insertRow([thisSupportID,thisSignID,'missing or invalid MPH value on Speed Limit type sign', "Error", "SignErrors", "", row[5][0], row[5][1], thisSignType])

            #MPH/SPEED LIMIT CHECKS
            if thisSignType in requiresMPH:
                #Check to make sure speed limit signs have an integer value in the signtext field
                if not row[3]:        
                    print 'MPH value is Null on Speed Limit type sign: ' + thisSignID
                    icursor.insertRow([thisSupportID,thisSignID,'MPH value is Null on Speed Limit type sign', "Error", "SignErrors", "", row[5][0], row[5][1], thisSignType])

                elif not row[3].strip().isdigit():        
                    print 'missing or invalid MPH value on Speed Limit type sign: ' + thisSignID
                    icursor.insertRow([thisSupportID,thisSignID,'missing or invalid MPH value on Speed Limit type sign', "Error", "SignErrors", "", row[5][0], row[5][1], thisSignType])

                else:
                    if not int(row[3])%5==0:
                        print 'MPH value on Speed Limit type sign must be divisible by 5: ' + thisSignID
                        icursor.insertRow([thisSupportID,thisSignID,'MPH value on Speed Limit type sign must be divisible by 5', "Error", "SignErrors", "", row[5][0], row[5][1], thisSignType])

            #CHECK RESTRICTIONS 
            if thisSignType in requirestime or thisSignType in requiresday:
                #sign should have restriction
                if thisSignID not in signswithrestrictions_dict:
                    #this particular SignID is missing it, however
                    print 'missing restriction record ' + thisSignID
                    icursor.insertRow([thisSupportID,thisSignID,'missing restriction record', "Warning", "SignErrors", "", row[5][0], row[5][1], thisSignType])

                else:
                    #RESTRICTION PRESENT
                    for restriction in signswithrestrictions_dict[thisSignID]:
                        
                        if thisSignType in requiresday:

                            #Check to make sure sign has start day
                            if (not restriction[0] or restriction[0] == ' ') and restriction[0] !=8:
                                print 'missing or incomplete start day on sign: ' + thisSignID
                                icursor.insertRow([thisSupportID,thisSignID,'missing start day on sign restriction', "Warning", "SignRestrictionErrors", restriction[5], row[5][0], row[5][1], thisSignType])

                            #Check to make sure sign has end day
                            if (not restriction[1] or restriction[1] == ' ') and restriction[1] !=8:
                                print 'missing or incomplete end day on sign: ' + thisSignID
                                icursor.insertRow([thisSupportID,thisSignID,'missing end day on sign restriction', "Warning", "SignRestrictionErrors", restriction[5], row[5][0], row[5][1], thisSignType])
                                
                            #Check if startday is 'larger' than end day
                            if restriction[0] > restriction[1]:                
                                print 'startday is larger than end day: ' + thisSignID
                                icursor.insertRow([thisSupportID,thisSignID,'start day is larger than end day', "Info", "SignRestrictionErrors", restriction[5], row[5][0], row[5][1], thisSignType])

                        if thisSignType in requirestime:
                            
                            #Check to make sure sign has start time
                            if (not restriction[2] or restriction[2] == ' ') and restriction[2] !=49:
                                print 'missing or incomplete start time on sign: ' + thisSignID
                                icursor.insertRow([thisSupportID,thisSignID,'missing or incomplete start time on sign restriction', "Warning", "SignRestrictionErrors", restriction[5], row[5][0], row[5][1], thisSignType])
                                
                            #Check to make sure sign has end time
                            if (not restriction[3] or restriction[3] == ' ') and restriction[3] !=49:
                                print 'missing or incomplete end time on sign: ' + thisSignID
                                icursor.insertRow([thisSupportID,thisSignID,'missing or incomplete end time on sign', "Warning", "SignRestrictionErrors", restriction[5], row[5][0], row[5][1], thisSignType])
                                
                            #Check if starttime is 'larger' than end day
                            if restriction[2] >= restriction[3]:                
                                print 'start time is larger than end time: ' + thisSignID
                                icursor.insertRow([thisSupportID,thisSignID,'start time is larger than end time', "Info", "SignRestrictionErrors", restriction[5], row[5][0], row[5][1], thisSignType])
                                            
print 'sign and support error checks complete'
