#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      jgraham
#
# Created:     31/07/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-----------------------------------------------------------------------------

import arcpy
import os, sys

from ValidationUtilities import writeZoneError, getZoneErrorInfo, checkZoneDayTimeIntersect
from TableTools import Table2Dict

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
connection = r'Database Connections\Connection to DDOTGIS as TOA.sde'

gdb = os.path.join(scriptloc, "scratch.gdb")
checkzone_gdb = os.path.join(scriptloc, 'checkzones.gdb')
parking_gdb = os.path.join(scriptloc, "parking_signs_reference.gdb")
parking_zones_gdb = os.path.join(scriptloc, "parking_zones.gdb")
#Zone table
ParkingZone = os.path.join(parking_zones_gdb,'ParkingZone')
ParkingZoneDetail = os.path.join(parking_zones_gdb,'ParkingZoneDetail')
if not arcpy.Exists(ParkingZoneDetail):
    arcpy.CreateTable_management(out_path=parking_zones_gdb,
                                out_name="ParkingZoneDetail",
                                template=ParkingZone,config_keyword="#")

    arcpy.AddField_management(ParkingZoneDetail, "STARTDAY", "LONG", 3, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZoneDetail, "ENDDAY", "LONG", 3, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZoneDetail, "STARTTIME", "LONG", 3, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ParkingZoneDetail, "ENDTIME", "LONG", 3, "", "", "", "NULLABLE", "NON_REQUIRED")

#Zone error table
CheckTable_ErrorZone = os.path.join(checkzone_gdb, 'CheckTable_ErrorZone')
#CheckTable_ErrorSign for repetitive sign types and other types of sign-specific errors.
if not arcpy.Exists(CheckTable_ErrorZone):
    arcpy.CreateTable_management(checkzone_gdb,os.path.basename(CheckTable_ErrorZone))
    arcpy.AddField_management(CheckTable_ErrorZone, "SupportID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "ZoneID", "LONG",10, "", "", "MUTCD Code", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "ROUTEID", "TEXT","", "", 25, "MUTCD Code", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "SIDE", "TEXT", 8, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "MUTCD", "TEXT","", "", 25, "MUTCD Code", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "COUNT", "LONG",2, "", "", "COUNT", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "MEAS_FROM", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "MEAS_TO", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "ERROR_TYPE", "TEXT", 255, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(CheckTable_ErrorZone, "ERROR_DESC", "TEXT", 255, "", "", "", "NULLABLE", "NON_REQUIRED")
else:
    arcpy.TruncateTable_management(CheckTable_ErrorZone)


#reference to conflicting signs table
conflict_signs_reference = os.path.join(parking_gdb, 'conflict_signs_reference')
#main working detail table
SignTableDetail = os.path.join(gdb,'SignTableDetail')
#Sign table
LocalCopyofSigns = os.path.join(gdb,'Signs_P_local')
LocalCopyofSigns_summary = LocalCopyofSigns + "_summary"

#Summarize the total number of signs per zone:
arcpy.Statistics_analysis(in_table=LocalCopyofSigns,
                            out_table=LocalCopyofSigns_summary,
                            statistics_fields="OBJECTID COUNT",
                            case_field="ZoneID;MUTCD")

SignTableDetail_summary = SignTableDetail + "_summary"
arcpy.Statistics_analysis(in_table=SignTableDetail,
                            out_table=SignTableDetail + "_summary",
                            statistics_fields="OBJECTID COUNT",
                            case_field="ZoneID;MUTCD;ROUTEID;STARTDAY;ENDDAY;STARTTIME;ENDTIME")

#getting a count of each zone's sign count.  This gives us the ability to
zonecount = Table2Dict(LocalCopyofSigns_summary, "ZoneID", ['MUTCD', 'FREQUENCY'])
zonemeas = Table2Dict(ParkingZone, "ZoneID", ['MEAS_FROM', 'MEAS_TO'])

print 'loading into zonedetail table...'
icursor = arcpy.da.InsertCursor(ParkingZoneDetail, ['ZoneID','MUTCD','ROUTEID','MEAS_FROM','MEAS_TO','SIDE','STARTDAY', 'ENDDAY', 'STARTTIME', 'ENDTIME'])

currentzone = 0
previouszone = 0

with arcpy.da.SearchCursor(SignTableDetail_summary, ['ZoneID','MUTCD','ROUTEID','STARTDAY', 'ENDDAY', 'STARTTIME', 'ENDTIME', 'FREQUENCY'],
                             sql_clause=(None, 'ORDER BY ZoneID ASC, FREQUENCY DESC')) as cursor:

    for row in cursor:
        startday=row[3]
        endday=row[4]
        starttime=row[5]
        endtime=row[6]
        bandcountis = row[7]
        bandcountshouldbe = zonecount[row[0]]['FREQUENCY']

        if row[0] in zonemeas:
            meas_from, meas_to = zonemeas[row[0]]['MEAS_FROM'],zonemeas[row[0]]['MEAS_TO']

            if bandcountis == bandcountshouldbe:
                #this time band is accounted for across all signs in this zone.
                icursor.insertRow([row[0],row[1],row[2],meas_from,meas_to,row[5],row[6],row[7],row[8],row[9]])
            elif bandcountis > bandcountshouldbe:
                #this time band does not appear in all signs in the zone.
                writeZoneError("TIMEBANDLOW", row[0], row[1], row[2], row[6], row[4], row[5])
                icursor.insertRow([row[0],row[1],row[2],meas_from,meas_to,row[5],row[6],row[7],row[8],row[9]])
            else:
                #this time band somehow appears in more than the expected number (somehow).
                writeZoneError("TIMEBANDHIGH", row[0], row[1], row[2], row[6], row[4], row[5])
                icursor.insertRow([row[0],row[1],row[2],meas_from,meas_to,row[5],row[6],row[7],row[8],row[9]])

print 'yay'