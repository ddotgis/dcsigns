import sys, os
import arcpy


def CheckAttachments(my_features, feature_ID, my_attachments, attach_ID, add_field):
    """Count the number of attachments.
    
    my_features -- feature class of which attachments are counted
    feature_ID -- key ID of my_features to be based on join
    my_attachments -- attachment table of feature class
    attach_ID -- key ID of my_attachments to match my_features
    add_field -- new field added to the sign table counting number of picture attachments

    
    """
    attach_list = []

    with arcpy.da.SearchCursor(my_attachments,[attach_ID])as rows:
        for row in rows:
            attach_list.append(row[0])

    with arcpy.da.UpdateCursor(my_features,[feature_ID, add_field]) as frows:
        for frow in frows:
             frow[1] = attach_list.count(frow[0])
             frows.updateRow(frow)