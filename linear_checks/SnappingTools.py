#-------------------------------------------------------------------------------
# Name:        Snapping Tools
# Purpose:      To provide functions that allow for the movement of Supports to a nearby centerline feature, and maintenance of signs related to those same supports
#
# Author:      jgraham
#
# Created:     22/05/2015
# Copyright:   (c) jgraham 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------


def SnapSigns(bySegment=None, DBconnection=None):
    import arcpy
    import sys, os

    '''
    Function to snap related sign records to supports
    If bySegment == None, then all signs will be snapped to related support XYs
    If bySegment is passed, then snapping will be performed on only records that are associated with a specific StreetSegID
    '''
    def rreplace(s, old, new, occurrence):
        li = s.rsplit(old, occurrence)
        return new.join(li)

    connection = DBconnection
    supports = os.path.join(connection,'TOA.Supports')
    sign_types = ['P']
    sign_data = []

    for sign in sign_types:
        sign_data.append(os.path.join(connection,"TOA.Signs_" + sign))

    SupportDict={}
    support_field_names = ['GLOBALID', 'SHAPE@X','SHAPE@Y','ROUTEID']

    if bySegment:
        #we have been passed a route ID
        filter_supports = "ROUTEID = '" + bySegment + "'"
    else:
        filter_supports = None

    with arcpy.da.SearchCursor(supports,support_field_names,where_clause=filter_supports) as cursor:
        for row in cursor:
            SupportDict[row[0]]=dict(zip(support_field_names[1:],row[1:]))

    if bySegment:
        #we have been passed a route ID
        supports = SupportDict.keys()
        supports_fromlist = "','".join(supports)
        filter_signs ="SUPPORTID in(" + supports_fromlist + ")"
        filter_signs = filter_signs.replace("{","'{", 1)
        filter_signs = rreplace(filter_signs,"}","}'",1)
    else:
        filter_signs = None

    print 'check'
    sign_field_names = ['SUPPORTID', 'SHAPE@X','SHAPE@Y']

    # Start an edit session. Must provide the worksapce.
    edit = arcpy.da.Editor(connection)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit.startEditing(False, False)

    # Start an edit operation
    edit.startOperation()
    counter =1
    for fc in sign_data:
        print "working on " + fc
        with arcpy.da.UpdateCursor(fc,sign_field_names, where_clause=filter_signs) as ucursor:
            for urow in ucursor:
                counter +=1
                print 'updated sign on support ' + str(urow[0])
                if urow[0]:
                    urow[1] = SupportDict[urow[0]][sign_field_names[1]]
                    urow[2] = SupportDict[urow[0]][sign_field_names[2]]
                ucursor.updateRow(urow)

    # Stop the edit operation.
    edit.stopOperation()

    # Stop the edit session and save the changes
    edit.stopEditing(True)

def RouteSupportsToDict(singleseg=None):

    '''
    Function to OnSeg supports to centerline and provide a dict in return with the closest.
    If a seg id value is provided, then it is used to perform a spatial query for only those supports nearby
    '''
    import sys, os
    import arcpy
    from SegmentUtilities import GetOnSegPointInfo2

    connection = r'Database Connections\Connection to DDOTGIS as DDOTEDITOR.sde'
    supports = os.path.join(connection,'TOA.Supports')
    supports_lyr = "supports_lyr"
    arcpy.MakeFeatureLayer_management(supports, supports_lyr)

    dict2return = dict()

    if singleseg:
        #do a spatial query to grab within 150 feet of the selected segment
        streetsegs = os.path.join(connection,'SSDDBA.ReferenceFeatures\SSDDBA.StreetSegment')
        streetsegs_lyr = "streetsegs_lyr"
        try:
            arcpy.MakeFeatureLayer_management(streetsegs, streetsegs_lyr, "STREETSEGID = " + str(singleseg)) #only selecting this block street seg ID
        except:
            raise Exception, "Error.  Cannot find this StreetSeg ID.  Check ID or source (currently set to {0})".format(singleseg)

        try:
            arcpy.SelectLayerByLocation_management(supports_lyr, "WITHIN_A_DISTANCE", streetsegs_lyr, "150 FEET", "NEW_SELECTION")
            #need to find out what was selected...
            supports_lyr_sel_desc = arcpy.Describe(supports_lyr)
            supports_lyr_sel_FIDS = supports_lyr_sel_desc.FIDset.split(";")
            supports_selected = ','.join(map(str, supports_lyr_sel_FIDS)) #capture all the IDs for the selected supports
            whereclause = "OBJECTID in ( " + supports_selected + ")"
        except:
            raise Exception, "Error.  Problem selecting supports along this segment ({0}.".format(singleseg)

    else:
        #run all recs
        whereclause = "GLOBALID IS NOT NULL"

    SupportLocationErrors = dict()
    with arcpy.da.SearchCursor(supports_lyr,['GLOBALID','SHAPE@X','SHAPE@Y','ROUTEID','MEASURE','SIDE','ROUTEID_ALT','MANUAL_SEGID',"OID@"], where_clause=whereclause) as cursor:
        for row in cursor:
            #if we are locating only a single single street seg
            manualsegID = row[6]

            #validate
            if row[7] is None:
                closestseg = GetOnSegPointInfo2(row[1], row[2], searchradius = 100)
            else:
                closestseg = GetOnSegPointInfo2(row[1], row[2], searchradius = 100, manualsegID = int(row[7]))
                
            if singleseg:   
                if closestseg[0] != int(singleseg):
                    continue

            print closestseg
            if 'error' in closestseg:
                #document that we had an error locating this support
                dict2return[row[0]] = {"ErrorLevel":"Warning",
                           "ErrorType":"SupportLocationErrors",
                           "CheckName":"Network Location for this Support cannot be found",
                           "SupportIDs":row[0],
                           "Polyline":thisGeometry}

            dict2return[row[0]] = {"ROUTEID":closestseg[0],
                                    "MEASURE":closestseg[1],
                                    "SIDE":closestseg[2],
                                    "ANGLE":closestseg[4],
                                    "ROUTEID_ALT":"---"} #eventually include logic to switch route based on manual override.
            
            print "Completed " + str(row[0])

        else:
            print "Error finding segment for " + str(row[0])

    return dict2return

def RouteAndUpdateSupports():

    '''
    Function to OnSeg supports to centerline and provide a dict in return with the closest
    '''
    import sys, os
    import arcpy
    from SegmentUtilities import GetOnSegPointInfo

    connection = r'Database Connections\Connection to DDOTLRS as LRSViewer.sde'
    supports = os.path.join(connection,'Ddotlrs.RH.LRSE_StreetSegment')
    dict2return = dict()

    # Start an edit session. Must provide the worksapce.
    edit = arcpy.da.Editor(connection)

    # Edit session is started without an undo/redo stack for versioned data
    #  (for second argument, use False for unversioned data)
    edit.startEditing(False, False)

    # Start an edit operation
    edit.startOperation()

    with arcpy.da.UpdateCursor(supports,['GLOBALID','SHAPE@X','SHAPE@Y','ROUTEID','MEASURE','SIDE','ROUTEID_ALT']) as cursor:
        for row in cursor:
            closestseg = GetOnSegPointInfo(row[1], row[2])
            if closestseg:
                row[3] = closestseg[0]
                row[4] = closestseg[1]
                row[5] = closestseg[2]
                row[6] = closestseg[5]

                cursor.updateRow(row)
                print "Completed " + str(row[0])

            else:
                print "Error finding segment for " + str(row[0])

    # Stop the edit operation.
    edit.stopOperation()

    # Stop the edit session and save the changes
    edit.stopEditing(True)

if __name__ == '__main__':
    #SnapSigns('10495')
    testdict = RouteSupportsToDict('32')
    print 'whoa'

