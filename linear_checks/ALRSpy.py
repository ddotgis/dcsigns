def getLRSRouteID(streetSegID):
    '''
    get the route id for a given streetsegid.  Uses DDOT's Roads and Highways LRS Map Services
    '''
    import requests
    r = requests.post('https://rh.dcgis.dc.gov/dcgis/rest/services/DDOT/DDOTLRS/FeatureServer/4/query',
                  data = {'where':'STREETSEGID = ' + str(streetSegID),
                          'outFields': '*',
                          'f':'json'})

    try:
        response = r.json()
        routeid = response['features'][0]['attributes']['ROUTEID']
    except:
        raise Exception,  "Error.  Cannot parse json.  Check URL or response for issues"
    
    return routeid

def getLRSGeometry(routeID, fromM, toM=0, outSR=3857):
    '''
    get the geometry for a given centerline, given a routeID and a from and to measure for line events, single measure (fromM) for single point events.
    Uses DDOT's Roads and Highways LRS Map Services
    '''
    import requests

    if toM>0:
        #from and to measures submitted (line)
        locations_text = "[{'routeId':" + str(routeID) + ",'fromMeasure':" + str(fromM) + ",'toMeasure':" + str(toM) + "}]"
    else:
        #single measure (point)
        locations_text = "[{'routeId':" + str(routeID) + ",'measure':" + str(fromM) + "}]"
        
    r = requests.post('https://rh.dcgis.dc.gov/dcgis/rest/services/DDOT/DDOTLRS/MapServer/exts/LRSServer/networkLayers/76/measureToGeometry',
                  data = {'locations':locations_text,
                          'outFields': '*',
                          'outSR':outSR,
                          'f':'json'})

    try:
        response = r.json()
        if toM>0:
            #from and to measures submitted (line)
            geoms = response['locations'][0]['geometry']['paths'][0]
        else:
            #measure point location
            geoms = response['locations'][0]['geometry']['x'], response['locations'][0]['geometry']['y']
        
    except:
        raise Exception,  "Error.  Cannot parse json.  Check URL or response for issues"
    
    return geoms

if __name__ == '__main__':

    routeid = getLRSRouteID(32)
    geom = getLRSGeometry(routeid,0,50)