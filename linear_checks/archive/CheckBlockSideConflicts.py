#-------------------------------------------------------------------------------
# Name:        CheckBlockSideConflicts
# Purpose:
#
# Author:      jgraham
#
# Created:     31/12/2014
# Copyright:   (c) jgraham 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import arcpy
import sys, os
import csv
sys.path.append(r"C:\Users\jgraham\Documents\asset_checks")
from AssetUtilities import Table2Dict
from operator import itemgetter
from pprint import pprint

def getOverlap(starta, enda, startb, endb):
    x = range(starta, enda)
    y = range(startb, endb)
    return list(set(x) & set(y))

def main():

    scriptloc = sys.path[0]

    ##Assumes a point events table has recently been created and is now available at (source directory)/scratch.gdb/signevents.
    ##Also assumes a linear events table has been recently created and available (linearsignevents)
    ##Also assumes the above-referenced linear events table has been updated with day and time for all 'valid' zones.  Any zones that do not pass validation are not processed in this module.
    #Assumptions:
    #1. Any zone which lacks day/time begin/end information is assumed to be 'Anytime'.
    #2. Any zone that has internal conflicts (I'm calling them 'Zone-level Conflicts') are assumed to be 'Anytime' until the conflict is resolved and zone-level validations are rerun.
    ##local work gdb...
    gdb = os.path.join(scriptloc, "scratch.gdb")
    linearsigneventtable =os.path.join(gdb,"linearsignevents")
    linearsignevents_permissive = os.path.join(gdb,"linearsignevents_permissive")
    linearsignevents_restrictive = os.path.join(gdb,"linearsignevents_restrictive")
    conflict_signs_reference =os.path.join(gdb,"conflict_signs_reference")
    connection_signs = r"Database Connections\Connection to DDOTGIS as TOA.sde"

    zone_fields_key = "ParticipatingSigns"
    zone_fields = ["MUTCD",
                "RouteID",
                "Side",
                "Begin",
                "End",
                "daystart1",
                "dayend1",
                "hourstart1",
                "hourend1",
                "daystart2",
                "dayend2",
                "hourstart2",
                "hourend2",
                "daystart3",
                "dayend3",
                "hourstart3",
                "hourend3",
                "daystart4",
                "dayend4",
                "hourstart4",
                "hourend4",
                "BlockValidation",
                "InZoneValidation"]

    serviceday =[d.codedValues for d in arcpy.da.ListDomains(connection_signs) if d.name == 'LZ_ServiceDay'][0]
    servicehour =[d.codedValues for d in arcpy.da.ListDomains(connection_signs) if d.name == 'LZ_ServiceHours'][0]

    #sign zone event data
    zones_dict = Table2Dict(linearsigneventtable,
                            zone_fields_key,
                            zone_fields)

    #sign zone event data
    conflict_signs_ref = Table2Dict(conflict_signs_reference,
                            "MUTCD",
                            ["TYPE"])

    #BLOCK/SIDE LOADING DATA FOR COMPARISON
    #Structure is like this: Segid_side { participatingsigns_timeday_quad{}}
    #For example:                 8885_1{'26738,26736_1'{MUTCD:'R-NS-011', begin:0, end:54, daystart:1, dayend:5}...}
    blockzones_compare = dict()

    for k,v in zones_dict.items():

        this_route_side = str(v["RouteID"]) + "_" + str(v["Side"])

        for x in range(1,5):
            print 'working on...' + k + "_" + str(x)
            if k + "_" + str(x) == '26646,26648,26654,26658,26626,26663_2':
                print 'whoa'
            #inspect available day/time info.  Iterate through day/time quads
            daystart = v["daystart" + str(x)]
            dayend = v["dayend" + str(x)]
            hourstart = v["hourstart" + str(x)]
            hourend = v["hourend" + str(x)]
            inzonevalidation = [v["InZoneValidation"]]

            #standard entry.  If we need to, we'll change this prior to committing to dict.
            new_entry = {'MUTCD':v['MUTCD'],
                        'Begin':v['Begin'],
                        'End':v['End'],
                        'daystart':daystart,
                        'dayend':dayend,
                        'hourstart':hourstart,
                        'hourend':hourend,
                        'BlockValidation': [],
                        'InZoneValidation':inzonevalidation}

            #anytime entry.
            new_entry_anytime = {'MUTCD':v['MUTCD'],
                        'Begin':v['Begin'],
                        'End':v['End'],
                        'daystart':1,
                        'dayend':7,
                        'hourstart':1,
                        'hourend':50,
                        'BlockValidation': [],
                        'InZoneValidation':inzonevalidation}

            if not daystart and not dayend and not hourstart and not hourend:
                #all nulls or blanks or zeros
                if x == 1:
                    #if we have all nulls on the first quad of day/times, these are assumed to be 'ANYTIME',
                    if this_route_side not in blockzones_compare:
                        blockzones_compare[this_route_side] = {k + "_" + str(x) : new_entry_anytime}
                    else:
                        #add to existing entry...
                        blockzones_compare[this_route_side][k + "_" + str(x)] = new_entry_anytime
                else:
                    continue

            elif daystart == 8 or (daystart and dayend and hourstart and hourend):
                #we have at least a beginning day of 'ANYTIME' or all other fields are filled (minimum requirement).

                if this_route_side not in blockzones_compare:
                    if daystart == 8:
                        blockzones_compare[this_route_side] = {k + "_" + str(x) : new_entry_anytime}
                    else:
                        blockzones_compare[this_route_side] = {k + "_" + str(x) : new_entry}

                else:
                    #add to existing entry...
                    if daystart == 8:
                        blockzones_compare[this_route_side][k + "_" + str(x)] = new_entry_anytime
                    else:
                        blockzones_compare[this_route_side][k + "_" + str(x)] = new_entry
            else:
                #this zone has some unexpected/incomplete day/time entries that we can't reconcile.  Need to flag.

                if this_route_side not in blockzones_compare:
                    blockzones_compare[this_route_side] = {k + "_" + str(x) : new_entry}
                    blockzones_compare[this_route_side][k + "_" + str(x)]['InZoneValidation'].append('TimeDayIncomplete: One or more signs has incomplete day and/or hour data')

                else:
                    #add to existing entry...
                    blockzones_compare[this_route_side][k + "_" + str(x)] = new_entry
                    blockzones_compare[this_route_side][k + "_" + str(x)]['InZoneValidation'].append('TimeDayIncomplete: One or more signs has incomplete day and/or hour data')


    #BLOCK/SIDE COMPARISON
    #Store Validation Conflicts

    for streetside,zones in blockzones_compare.items():
        print "Zone Checks"
        for check_id, check_zone in zones.items():
            if len("; ".join(check_zone['InZoneValidation'])) > 0:
                #there is an in-zone validation issue with this zone.  Do not consider until it is resolved.
                continue

            check_zone_MUTCD = check_zone['MUTCD']
            check_zone_signtype = conflict_signs_ref[check_zone_MUTCD]['TYPE'] #returns whether sign is permissive or restrictive.
            check_zone_begin = int(check_zone['Begin'])
            check_zone_end = int(check_zone['End'])
            check_zone_daystart = check_zone['daystart']
            check_zone_dayend = check_zone['dayend']
            check_zone_hourstart = check_zone['hourstart']
            check_zone_hourend = check_zone['hourend']


            for against_id, against_zone in zones.items():
                if len("; ".join(against_zone['InZoneValidation'])) > 0:
                    #there is an in-zone validation issue with this zone.  Do not consider until it is resolved.
                    continue
                if check_id == against_id:
                    #self to self.  skip it.
                    continue

                print "Checking " + check_id + " against " + against_id
                against_zone_MUTCD = against_zone['MUTCD']
                against_zone_signtype = conflict_signs_ref[against_zone_MUTCD]['TYPE'] #returns whether sign is permissive or restrictive.
                against_zone_begin = int(against_zone['Begin'])
                against_zone_end = int(against_zone['End'])
                against_zone_daystart = against_zone['daystart']
                against_zone_dayend = against_zone['dayend']
                against_zone_hourstart = against_zone['hourstart']
                against_zone_hourend = against_zone['hourend']

                #check if checking permissive vs. restrictive/ restrictive vs. permissive.
                if check_zone_signtype != against_zone_signtype:
                    #check linear overlap
                    linearoverlap = sorted(getOverlap(check_zone_begin, check_zone_end, against_zone_begin, against_zone_end))
                    if linearoverlap:
                        #check day overlap
                        dayoverlap = getOverlap(check_zone_daystart, check_zone_dayend, against_zone_daystart, against_zone_dayend)
                        if dayoverlap:
                            #finally, check if times overlap
                            timeoverlap = getOverlap(check_zone_hourstart, check_zone_hourend, against_zone_hourstart, against_zone_hourend)
                            if timeoverlap:
                                #if we get here, then we should have two conflicting zones.  Let's log as a BlockValidation error
                                errormessage = 'SignConflictError: Signtypes {0} and {1} overlap from {2} to {3}'.format(check_zone_MUTCD, against_zone_MUTCD, linearoverlap[0], linearoverlap[-1])
                                blockzones_compare[streetside][check_id]['BlockValidation'].append(errormessage)

    #Update event table with all zone conflict detail.
    with arcpy.da.UpdateCursor(linearsigneventtable, ['RouteID', 'Side', 'ParticipatingSigns', 'InZoneValidation', 'BlockValidation']) as cursor:
        for row in cursor:
            blockvalidations = []
            inzonevalidations = []
            for x in xrange(1,5):
                streetside = str(row[0]) + "_" + str(row[1])
                check_id = row[2] + "_" + str(x)
                if check_id in blockzones_compare[streetside]:
                    #this day/time quad is available
                    inzonevalidations.append(", ".join(blockzones_compare[streetside][check_id]['InZoneValidation']))
                    blockvalidations.append(", ".join(blockzones_compare[streetside][check_id]['BlockValidation']))

            row[3] = "; ".join(inzonevalidations)
            row[4] = "; ".join(blockvalidations)
            cursor.updateRow(row)
if __name__ == '__main__':
    main()








