import arcpy
import sys, os

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
gdb = os.path.join(scriptloc, "scratch.gdb")
zonegdb = os.path.join(scriptloc, 'parking_zones.gdb')

connection = r'Database Connections\Connection to DDOTGIS as TOA.sde'
signs_P =os.path.join(connection, 'TOA.Signs_P')

#Create relationship class (local)
ParkingZone = os.path.join(zonegdb, 'ParkingZone')
ParkingZone_copy = os.path.join(gdb, 'ParkingZoneCopy')
arcpy.Copy_management(ParkingZone, ParkingZone_copy)

arcpy.AddField_management(ParkingZone_copy, "OFFSET", "LONG", 9, "", "", "", "NULLABLE", "NON_REQUIRED")

SignTableDetail = os.path.join(gdb, 'SignTableDetail')
Relation_ZoneSign = os.path.join(gdb, 'ZoneSignRelation')
arcpy.CreateRelationshipClass_management(ParkingZone_copy, SignTableDetail, Relation_ZoneSign,'SIMPLE','has','compose', '', 'ONE_TO_MANY', '', 'ZoneID', 'ZoneID')

#Create relationship class (link to point feature class)
signsCopy = os.path.join(gdb,'signsCopy')
arcpy.CopyFeatures_management(signs_P, signsCopy)
Relation_SignLocation = os.path.join(gdb, 'Relation_SignLocation')
arcpy.CreateRelationshipClass_management(SignTableDetail,signsCopy,Relation_SignLocation, "SIMPLE", "is located at","SignTableDetail","NONE","ONE_TO_MANY","NONE","SupportID","SUPPORTID")




