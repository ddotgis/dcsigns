import arcpy
import sys, os
from openpyxl import load_workbook
import json

from linear_checks.SegmentUtilities import GetOnSegPointInfo
from linear_checks.TableTools import Table2Dict
from linear_checks.SnappingTools import RouteSupportsToDict, SnapSigns

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]

blockID = arcpy.GetParameterAsText(0)

gdb = os.path.join(scriptloc, "basetables.gdb")
error_gdb = os.path.join(scriptloc, "errorchecks.gdb")

if not arcpy.Exists(error_gdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(error_gdb))

SignTableDetail = os.path.join(gdb,'SignTableDetail')
ErrorCheckTable = os.path.join(error_gdb,'ErrorCheckTable')

if not arcpy.Exists(ErrorCheckTable):
    arcpy.CreateTable_management(error_gdb,os.path.basename(ErrorCheckTable))
    arcpy.AddField_management(ErrorCheckTable, "SupportIDs", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "SignIDs", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "RestrictionID", "TEXT", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "MUTCD", "TEXT","", "", 25, "MUTCD Code", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "SIGNTEXT", "TEXT","", "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "SIGNNUMBER", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "SignArrowDirection", "TEXT","", "", 2, "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ROUTEID", "LONG", 10, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "MEASURE", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "MEASURE_TO", "FLOAT", 8, 3, "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "SIDE", "LONG", 8, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ZoneID", "LONG", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "STARTDAY", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ENDDAY", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "STARTTIME", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ENDTIME", "LONG", 2, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "TIMEID", "TEXT", 25, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "EXCEPTION", "TEXT", 9, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "X", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "Y", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "LAT", "FLOAT", '12', "8", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "LON", "FLOAT", '12', "8", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "CheckName", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ErrorLevel", "TEXT", 20, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ErrorType", "TEXT", 20, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ErrorCheckTable, "ErrorID", "LONG", 8, "", "", "", "NULLABLE", "NON_REQUIRED")
    
else:
    arcpy.TruncateTable_management(ErrorCheckTable)

wb = load_workbook(filename=os.path.join(scriptloc,'Book1.xlsx'), read_only=True)
ws = wb['pointerrorchecks'] # ws is now an IterableWorksheet
errorid = 0

for row in ws.rows:
    checkname = row[0].value
    if checkname == 'CheckName':
        continue
    print "Running..." + checkname
    errorquery = row[1].value
    errorlevel = row[2].value     

    with arcpy.da.SearchCursor(SignTableDetail,['SignID',
                                                'RestrictionID',
                                                'STARTDAY',
                                                'ENDDAY',
                                                'STARTTIME',
                                                'ENDTIME',
                                                'TIMEID',
                                                'EXCEPTION',
                                                'SIDE',
                                                'MUTCD',
                                                'SIGNTEXT',
                                                'SignArrowDirection',
                                                'SIGNNUMBER',
                                                'X',
                                                'Y',
                                                'SupportID'],
                                               where_clause=errorquery) as cursor:
        
        with arcpy.da.InsertCursor(ErrorCheckTable,['SignIDs',
                                                    'RestrictionID',
                                                    'STARTDAY',
                                                    'ENDDAY',
                                                    'STARTTIME',
                                                    'ENDTIME',
                                                    'TIMEID',
                                                    'EXCEPTION',
                                                    'SIDE',
                                                    'MUTCD',
                                                    'SIGNTEXT',
                                                    'SignArrowDirection',
                                                    'SIGNNUMBER',
                                                    'X',
                                                    'Y',
                                                    'SupportIDs',
                                                    'CheckName',
                                                    'ErrorType',
                                                    'ErrorLevel',
                                                    'ErrorID']) as rows:

            for a in cursor:
                errorid +=1
                rows.insertRow([a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],checkname,"PointErrors",errorlevel, errorid])

signerrors = Table2Dict(ErrorCheckTable, "ErrorID", ["SupportIDs","SignIDs","MUTCD","X","Y","CheckName","ErrorLevel","ErrorType"])
with open(os.path.join(scriptloc, 'result.json'), 'w') as fp:
    json.dump(signerrors, fp)
