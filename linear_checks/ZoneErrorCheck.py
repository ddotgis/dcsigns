import sys, os
import arcpy
from TableTools import Table2Dict
from numpy import arange

        
def getOverlap(starta, enda, startb, endb, step):
    x = arange(starta, enda, step)
    y = arange(startb, endb, step)
    return list(set(x) & set(y))

def unique_values(table, field):
    with arcpy.da.SearchCursor(table, [field]) as cursor:
        return sorted({row[0] for row in cursor})

def WriteToTable(dict, CheckTable, field):
    '''
    dict: the returned dictionary of errors from previous searchcursor
    CheckTable: the error table to be populated
    ErrorCode: the errorcode for the specific error
    Description: description for the error
    field: populated field names
    '''
    import arcpy
    
    cursor = arcpy.da.InsertCursor(CheckTable, field)
    for key, value in dict.iteritems():
        cursor.insertRow(tuple([key]+ [value[a] for a in field if a != 'ZoneID']))
    del cursor

arcpy.env.overwriteOutput = True
scriptloc = sys.path[0]
checkzone = os.path.join(scriptloc, 'checkzones.gdb')
ZoneError = os.path.join(checkzone,'ZoneError')
SignAndSupportErrors = os.path.join(checkzone,'SignAndSupportErrors')

parking_zones_gdb = os.path.join(scriptloc, "parking_zones.gdb")
gdb = os.path.join(scriptloc, "scratch.gdb")
ParkingZone = os.path.join(parking_zones_gdb,'ParkingZone')
parking_gdb = os.path.join(scriptloc, "parking_signs_reference.gdb")
conflict_signs_reference = os.path.join(parking_gdb, 'conflict_signs_reference')

ParkingZoneCopy = os.path.join(gdb, 'ParkingZoneCopy')
arcpy.Copy_management(ParkingZone, ParkingZoneCopy)

    
#build zone error table  
if not arcpy.Exists(ZoneError):
    arcpy.CreateTable_management(checkzone, os.path.basename(ZoneError))
    arcpy.AddField_management(ZoneError, "ErrorCode", "TEXT", 25, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "Description", "TEXT", 255, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "ZoneID", "LONG", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ZoneError, "ZoneID_2", "LONG", 9, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(ZoneError, "ROUTEID", "LONG", 10, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "SIDE", "TEXT", 8, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "MUTCD", "TEXT", 25, "", "", "SignType", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "MEAS_FROM", "FLOAT", 8, 3, "","Linear Start (overlapping)", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "MEAS_TO", "FLOAT", 8, 3, "", "Linear End (overlapping)", "NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "STARTDAY", "LONG", 2, "", "", "STARTDAY (conflict)","NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "ENDDAY", "LONG", 2, "", "", "ENDDAY (conflict)", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "STARTTIME", "LONG", 2, "", "", "STARTTIME (conflict)", "NULLABLE","NON_REQUIRED")
    arcpy.AddField_management(ZoneError, "ENDTIME", "LONG", 2, "", "", "ENDTIME (conflict)", "NULLABLE","NON_REQUIRED")
else:
    arcpy.TruncateTable_management(ZoneError)
    
print 'Empty error table is ready to be populated'

if not arcpy.Exists(SignAndSupportErrors):
    
# check TimeDayIncomplete


def TimeDayIncomplete(inputValue, dict, fields):

    '''
    if 8 is in either STARTDAY or ENDDAY OR 49 (any time) is in either STARTTIME or ENDTIME, all values are considered ANYTIME.
    if 8 (Any time) is in STARTDAY or ENDDAY, then STARTTIME and ENDTIME are updated to 49
    inputValue: eachrow of the zone table
    dict: output dictionary with error zones
    fields: fields from inputTable that you want to keep and store into the dictionary
    
    '''
    if inputValue[fields.index('STARTDAY')] is None and inputValue[fields.index('ENDDAY')] is None:
        inputValue[fields.index('STARTDAY')] = 8
        inputValue[fields.index('ENDDAY')] = 8
        print 'Both nulls are replaced by 8'
    elif inputValue[fields.index('STARTDAY')] is None or inputValue[fields.index('ENDDAY')] is None:
        if inputValue[fields.index('STARTDAY')] == 8 or inputValue[fields.index('ENDDAY')] == 8:
            print 'Filled in 8'
            inputValue[fields.index('STARTDAY')] = 8
            inputValue[fields.index('ENDDAY')] = 8
        elif inputValue[fields.index('ENDDAY')] is not None:
            print 'Value misplaced in ENDDAY at zone ' + str(inputValue[fields.index('ZoneID')])
            dict[inputValue[fields.index('ZoneID')]] = {'MUTCD':inputValue[fields.index('MUTCD')], 
                                                    'ROUTEID':inputValue[fields.index('ROUTEID')], 
                                                    'SIDE':inputValue[fields.index('SIDE')], 
                                                    'MEAS_FROM':inputValue[fields.index('MEAS_FROM')], 
                                                    'MEAS_TO':inputValue[fields.index('MEAS_TO')], 
                                                    'STARTDAY':inputValue[fields.index('STARTDAY')], 
                                                    'ENDDAY':inputValue[fields.index('ENDDAY')], 
                                                    'STARTTIME':inputValue[fields.index('STARTTIME')], 
                                                    'ENDTIME':inputValue[fields.index('ENDTIME')],
                                                    'ErrorCode': 'MD',
                                                    'Description': 'Misplaced DAY information'
                                                    }
    elif int(inputValue[fields.index('STARTDAY')]) > int(inputValue[fields.index('ENDDAY')]):
        #some are valid and some can be expressed in other ways. for example, if STARTDAY = 6 and ENDDAY = 5, it is the same as STARTDAY = 1 and ENDDAY = 7
        #However, if it is STARTDAY = 7 and ENDDAY = 5 ??????????????
        print 'Startday is larger than endday at Zone ' + str(inputValue[fields.index('ZoneID')])
        dict[inputValue[fields.index('ZoneID')]] = {'MUTCD':inputValue[fields.index('MUTCD')], 
                                                    'ROUTEID':inputValue[fields.index('ROUTEID')], 
                                                    'SIDE':inputValue[fields.index('SIDE')], 
                                                    'MEAS_FROM':inputValue[fields.index('MEAS_FROM')], 
                                                    'MEAS_TO':inputValue[fields.index('MEAS_TO')], 
                                                    'STARTDAY':inputValue[fields.index('STARTDAY')], 
                                                    'ENDDAY':inputValue[fields.index('ENDDAY')], 
                                                    'STARTTIME':inputValue[fields.index('STARTTIME')], 
                                                    'ENDTIME':inputValue[fields.index('ENDTIME')],
                                                    'ErrorCode': 'RE',
                                                    'Description': 'Day or time restriction error'
                                                    }
    else:
        #DAY information are fine
        if inputValue[fields.index('STARTTIME')] is None and inputValue[fields.index('ENDTIME')] is None:
            inputValue[fields.index('STARTTIME')] = 49
            inputValue[fields.index('ENDTIME')] = 49
            # both are blank
            print 'Both nulls are replaced by 49'
        elif inputValue[fields.index('STARTTIME')] is None or inputValue[fields.index('ENDTIME')] is None:
            #only one is blank
            if inputValue[fields.index('STARTTIME')] == 49 or inputValue[fields.index('ENDTIME')] == 49:
                #the other is 49
                inputValue[fields.index('STARTTIME')] = 49
                inputValue[fields.index('ENDTIME')] = 49
                print 'Filled in 49'
            else:
                #the other is not 49
                print 'issues with time at zone ' + str(inputValue[fields.index('ZoneID')])
                dict[inputValue[fields.index('ZoneID')]] = {'MUTCD':inputValue[fields.index('MUTCD')], 
                                                            'ROUTEID':inputValue[fields.index('ROUTEID')], 
                                                            'SIDE':inputValue[fields.index('SIDE')], 
                                                            'MEAS_FROM':inputValue[fields.index('MEAS_FROM')], 
                                                            'MEAS_TO':inputValue[fields.index('MEAS_TO')], 
                                                            'STARTDAY':inputValue[fields.index('STARTDAY')], 
                                                            'ENDDAY':inputValue[fields.index('ENDDAY')], 
                                                            'STARTTIME':inputValue[fields.index('STARTTIME')], 
                                                            'ENDTIME':inputValue[fields.index('ENDTIME')],
                                                            'ErrorCode': 'MT',
                                                            'Description': 'Incompelete Time Restriction'
                                                            }
        elif inputValue[fields.index('STARTTIME')] > inputValue[fields.index('ENDTIME')]:
            print 'issues with time at zone ' + str(inputValue[fields.index('ZoneID')])
            dict[inputValue[fields.index('ZoneID')]] = {'MUTCD':inputValue[fields.index('MUTCD')], 
                                                        'ROUTEID':inputValue[fields.index('ROUTEID')], 
                                                        'SIDE':inputValue[fields.index('SIDE')], 
                                                        'MEAS_FROM':inputValue[fields.index('MEAS_FROM')], 
                                                        'MEAS_TO':inputValue[fields.index('MEAS_TO')], 
                                                        'STARTDAY':inputValue[fields.index('STARTDAY')], 
                                                        'ENDDAY':inputValue[fields.index('ENDDAY')], 
                                                        'STARTTIME':inputValue[fields.index('STARTTIME')], 
                                                        'ENDTIME':inputValue[fields.index('ENDTIME')],
                                                        'ErrorCode': 'RE',
                                                        'Description': 'Day or Time Restriction'
                                                        }
        else:
            print 'Complete Day and Time info for Zone ' + str(inputValue[fields.index('ZoneID')])
        
    return dict, inputValue[fields.index('STARTTIME')], inputValue[fields.index('ENDTIME')], inputValue[fields.index('STARTDAY')], inputValue[fields.index('ENDDAY')]


TimeDayErrorDict = {}
fields = ['ZoneID','MUTCD','ROUTEID','SIDE','MEAS_FROM','MEAS_TO','STARTDAY','ENDDAY','STARTTIME','ENDTIME']
with arcpy.da.UpdateCursor(ParkingZoneCopy, fields) as cursor:
    for row in cursor:
        output = TimeDayIncomplete(row, TimeDayErrorDict, fields)
        row[fields.index('STARTTIME')] = output[1]
        row[fields.index('ENDTIME')] = output[2]
        row[fields.index('STARTDAY')] = output[3]
        row[fields.index('ENDDAY')] = output[4]
        cursor.updateRow(row)

WriteToTable(TimeDayErrorDict, ZoneError, fields + ['ErrorCode', 'Description'])

print 'Zone error check is completed and table is populated'



#CheckZoneConflict
conflict_signs_ref = Table2Dict(conflict_signs_reference,"MUTCD",["TYPE"])
RouteID_list = unique_values(ParkingZoneCopy, "RouteID")
arcpy.AddFieldDelimiters(ParkingZoneCopy, fields)

def CreateZoneCompareDict(RouteID, side, dict, ZoneErrorDict, fields):
    '''
    RouteID: the block we want to look at
    side: the side we want to look at
    dict: the dict where all the zone info get populated in
    ZoneErrorDict: if zone is in this dict, that means there is issue with time and day. need to correct that first before proceeding
    fields: fields from zone table that need to be kept and stored into output dictionary
    '''
    
    with arcpy.da.SearchCursor(ParkingZoneCopy, fields, where_clause = """ "ROUTEID" = %s AND "SIDE" = '%s' """ %(RouteID, side)) as cursor:
        for row in cursor:
            if row[fields.index('ZoneID')] in ZoneErrorDict:
                print "Resolve zone error for Zone " + str(row[fields.index('ZoneID')]) + " first and then proceed"
            else:
                dict[row[fields.index('ZoneID')]] = {'STARTDAY': row[fields.index('STARTDAY')],
                                                     'ENDDAY': row[fields.index('ENDDAY')],
                                                     'STARTTIME': row[fields.index('STARTTIME')],
                                                     'ENDTIME': row[fields.index('ENDTIME')],
                                                     'MEAS_FROM': row[fields.index('MEAS_FROM')],
                                                     'MEAS_TO': row[fields.index('MEAS_TO')],
                                                     'MUTCD':row[fields.index('MUTCD')],
                                                     'ROUTEID': row[fields.index('ROUTEID')],
                                                     'SIDE': row[fields.index('SIDE')]
                                                     }
    return dict

def CheckZoneConflict(InputDict, outputDict):
    '''
    InputDict comes from CreateZoneCompareDict()
    outputDict stores all the information of zones that have conflicts
    '''
    for key, value in InputDict.iteritems():
        check_zone_MUTCD = value['MUTCD']
        if check_zone_MUTCD not in conflict_signs_ref:
            print 'Signtype is neither permissive or restrictive for Zone ' + str(key)
        else:
            check_zone_signtype = conflict_signs_ref[check_zone_MUTCD]['TYPE'] #returns whether sign is permissive or restrictive.
            check_zone_begin = int(value['MEAS_FROM'])
            check_zone_end = int(value['MEAS_TO'])
            check_zone_daystart = value['STARTDAY']
            check_zone_dayend = value['ENDDAY']
            check_zone_hourstart = value['STARTTIME']
            check_zone_hourend = value['ENDTIME']

            against_dict = {against_key: against_value for against_key, against_value in InputDict.iteritems() if against_key != key}
            for against_key, against_value in against_dict.iteritems():
                currentcheckmsg = "Checking " + str(key) + " against " + str(against_key)
                if currentcheckmsg == 'Checking 17 against 19':
                    print 'whoa'
                print "Checking " + str(key) + " against " + str(against_key)
                against_zone_MUTCD = against_value['MUTCD']
                if against_zone_MUTCD not in conflict_signs_ref:
                    print 'Signtype is neither permissive or restrictive for Zone ' + str(against_key) + ' ;therefore PASS'
                else:
                    against_zone_signtype = conflict_signs_ref[against_zone_MUTCD]['TYPE'] #returns whether sign is permissive or restrictive.
                    against_zone_begin = int(against_value['MEAS_FROM'])
                    against_zone_end = int(against_value['MEAS_TO'])
                    against_zone_daystart = against_value['STARTDAY']
                    against_zone_dayend = against_value['ENDDAY']
                    against_zone_hourstart = against_value['STARTTIME']
                    against_zone_hourend = against_value['ENDTIME']

                #check linear overlap
                    linearoverlap = sorted(getOverlap(check_zone_begin, check_zone_end, against_zone_begin, against_zone_end, 1))
                    if linearoverlap:
                        if not check_zone_dayend or not against_zone_dayend: #check_zone_daystart is None (and the other is not) will be in the error dict already
                            check_zone_dayend = check_zone_daystart
                            against_zone_dayend = against_zone_daystart
                        dayoverlap = getOverlap(check_zone_daystart, check_zone_dayend + 1, against_zone_daystart, against_zone_dayend + 1, 1)
                        if dayoverlap:
                            #finally, check if times overlap
                            timeoverlap = getOverlap(check_zone_hourstart, check_zone_hourend, against_zone_hourstart, against_zone_hourend, 1)
                            if timeoverlap:
                                #check if checking permissive vs. restrictive/ restrictive vs. permissive.
                                if check_zone_signtype != against_zone_signtype:
                                    if against_key in outputDict and outputDict[against_key]['ZoneID_2'] == key:
                                        print 'Error has been recorded'
                                    else:
                                        outputDict[key] = {'ErrorCode': 'CTDT',
                                                           'Description': 'Conflicting time between two zones of different types',
                                                           'MEAS_FROM': min(linearoverlap),
                                                           'MEAS_TO': max(linearoverlap)+1,
                                                           'STARTDAY': min(dayoverlap),
                                                           'ENDDAY': max(dayoverlap)+1,
                                                           'STARTTIME': min(timeoverlap),
                                                           'ENDTIME': max(timeoverlap)+1,
                                                           'ZoneID_2':against_key,
                                                           'ROUTEID': against_value['ROUTEID'],
                                                           'SIDE': against_value['SIDE']
                                                            }
                                        print 'Conflicting time between two zones of different types between ' + str(key) + ' and ' + str(against_key)
                                else:
                                    if against_key in outputDict and outputDict[against_key]['ZoneID_2'] == key:
                                        print 'Error has been recorded'
                                    else:
                                        outputDict[key] = {'ErrorCode': 'OTST',
                                                           'Description': 'Overlapping time between two zones of same types',
                                                           'MEAS_FROM': min(linearoverlap),
                                                           'MEAS_TO': max(linearoverlap),
                                                           'STARTDAY': min(dayoverlap),
                                                           'ENDDAY': max(dayoverlap),
                                                           'STARTTIME': min(timeoverlap),
                                                           'ENDTIME': max(timeoverlap),
                                                           'ZoneID_2':against_key,
                                                           'ROUTEID': against_value['ROUTEID'],
                                                           'SIDE': against_value['SIDE']
                                                            }
                                        print 'Overlapping time between two zones of same types between ' + str(key) + ' and ' + str(against_key)
    return outputDict          


ZoneConflictDict = {}
for value in RouteID_list:
    for side in ['left','right']:
        compare = {}
        CreateZoneCompareDict(value, side, compare, TimeDayErrorDict, fields)
        CheckZoneConflict(compare, ZoneConflictDict)

WriteToTable(ZoneConflictDict, ZoneError, [field for field in fields if field != 'MUTCD'] + ['ErrorCode', 'Description','ZoneID_2'])
        
print 'DONE'

     
        















